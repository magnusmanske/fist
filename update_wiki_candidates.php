#!/usr/bin/php
<?PHP
error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','1800M');

include_once ( '/data/project/fist/public_html/php/ToolforgeCommon.php' ) ;

$tf = new ToolforgeCommon ;

function updateGeneric ( $group , $sql_part , $comment ) {
	global $tf ;
	print "Group: $group\n" ;
	$sql = "/* $comment */
	SELECT page_title,gil_to,count(*) AS cnt FROM wb_items_per_site ips1,page,commonswiki_p.globalimagelinks
	WHERE substr(page_title,2,255)=ips_item_id /* hack around failure by WMF to provide non-numeric field in wb_items_per_site */
	AND page_namespace=0 /* only Wikidata items */
	$sql_part
	AND NOT EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=0 AND pl_title IN ('Q13406463','Q11266439','Q4167836','Q4167410')) /* and are not a list article, template, category, disambig */
	AND NOT EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=120 AND pl_title='P18') /* but have no image */
	AND gil_wiki=ips_site_id /* with a wiki sitelink to a page with a Commons file */
	AND REPLACE(ips_site_page,' ','_')=gil_page_title /* where one database stores pages with spaces and the other stores them with underscores :-( */
	AND gil_page_namespace_id=0 /* and the linked page is in article namespace */
	AND gil_to NOT LIKE '%.svg' AND gil_to NOT LIKE '%.SVG' /* computer drawings are not usually humans on Commons */
	AND gil_to NOT LIKE '%.png' AND gil_to NOT LIKE '%.PNG' /* neither are PNGs */
	GROUP BY page_title,gil_to
	" ;
print "$sql\n" ;
	$db = $tf->openDBwiki ( 'wikidatawiki' , true ) ;
	$result = $tf->getSQL ( $db , $sql ) ;

	$dbt = $tf->openDBtool ( 'wdfist_p' ) ;
	$sql = "UPDATE wiki_candidates SET tag=0 WHERE `group`='$group'" ;
	$tf->getSQL ( $dbt , $sql ) ;

	while($o = $result->fetch_object()){
		$file = $dbt->real_escape_string ( $o->gil_to ) ;
		$q = preg_replace ( '/\D/' , '' , $o->page_title ) ;
		$sql = "INSERT INTO wiki_candidates (file,q,status,wiki_count,`group`,`tag`) VALUES ('$file',$q,'OPEN',{$o->cnt},'$group',1)" ;
		$sql .= " ON DUPLICATE KEY UPDATE wiki_count={$o->cnt},`tag`=1" ;
		$tf->getSQL ( $dbt , $sql , 2 ) ;
	}
	print "Stage 1 complete\n" ;

	// Cleanup files that are not there anymore
	$sql = "DELETE FROM wiki_candidates WHERE `group`='$group' AND `tag`=0" ;
	$result = $tf->getSQL ( $db , $sql , 2 ) ;
	print "Stage 2 complete\n" ;

	// Cleanup files that were already marked as "ignore"
	$sql = "DELETE FROM wiki_candidates WHERE EXISTS (SELECT * FROM ignore_files WHERE ignore_files.q=wiki_candidates.q AND ignore_files.file=wiki_candidates.file) AND `group`='$group'" ;
	$result = $tf->getSQL ( $db , $sql , 2 ) ;
	print "Stage 3 complete\n" ;

	// Cleanup multi-occurrence files (>3)
	$files = [] ;
	$sql = "SELECT DISTINCT `file` FROM wiki_candidates WHERE `group`='$group' GROUP BY `file` HAVING count(*)>3" ;
	$result = $tf->getSQL ( $dbt , $sql , 2 ) ;
	while($o = $result->fetch_object()) $files[] = $dbt->real_escape_string ( $o->file ) ;
	print "Stage 4 complete\n" ;
	if ( count($files) > 0 ) {
		$sql = "DELETE FROM wiki_candidates WHERE `file` IN ('" . implode("','",$files) . "') AND `group`='$group'" ;
		$tf->getSQL ( $dbt , $sql ) ;
	}
	print "Stage 5 complete\n" ;
}

function updateLinksToQ ( $group ) {
	$sql = "AND EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=0 AND pl_title='$group') /* that link to $group */" ;
	$comment = "Get image candidates for items that link to $group without image on Wikidata; part of the FIST tool, script update_wiki_candidates.php" ;
	updateGeneric ( $group , $sql , $comment ) ;
}

function updateWiki ( $group ) {
	$sql = "AND ips_item_id IN ( SELECT ips2.ips_item_id FROM wb_items_per_site ips2 WHERE ips2.ips_site_id='$group') /* sitelink to $group */" ;
	$comment = "Get image candidates for items with a $group sitelink without image on Wikidata; part of the FIST tool, script update_wiki_candidates.php" ;
	updateGeneric ( $group , $sql , $comment ) ;
}


//updateLinksToQ ( 'Q5' ) ;
updateWiki ( 'nlwiki' ) ;

?>