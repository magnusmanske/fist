#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);
ini_set('display_errors', 'On');

include_once ( '/data/project/fist/FileCandidates.php' ) ;

# FIXME this only does 500
function scanCommons ( $query , $group , &$fc ) {
	$files = $fc->searchCommons ( $query , 6 , 500 ) ;
	foreach ( $files AS $file ) {
		$fc->addFile ( [
			'q' => 0 ,
			'json' => $file ,
			'group' => '#'.strtoupper($group) ,
			'file_type' => 'IMAGE' ,
			'proposed_item_label' => preg_replace ( '/^File:(.+)\.[^.]+$/' , '$1' , $file->title ) ,
			'source' => 'COMMONS' ,
			'file_id' => $file->pageid
		] ) ;
	}
}

function scanFlickr ( $query , $group , &$fc ) {
	$page = 1 ;

	do {
		$files = $fc->searchFlickrAll ( $query , $page ) ;
		foreach ( $files AS $file ) {
			$file = json_decode ( json_encode($file) ) ;
			if ( !isset($file->{'@attributes'}) ) continue ;
			$a = $file->{'@attributes'} ;
			$fc->addFile ( [
				'q' => 0 ,
				'json' => $file ,
				'group' => '#'.strtoupper($group) ,
				'file_type' => 'IMAGE' ,
				'proposed_item_label' => $a->title ,
				'source' => 'FLICKR' ,
				'file_id' => $a->id
			] ) ;
		}
		$page++ ;
	} while ( $page <= $fc->flickr_total_pages ) ;
}

if ( !isset($argv[2]) ) die ( "USAGE: item_candidates.php GROUP_NAME QUERY_TEXT" ) ;
$group = $argv[1] ;
$query = $argv[2] ;

$fc = new FileCandidates ;
scanCommons ( $query , $group , $fc ) ;
scanFlickr ( $query , $group , $fc ) ;

# jsub -mem 4g -cwd ./item_candidates.php SUBSPECIES 'subspecies'
# jsub -mem 4g -cwd ./item_candidates.php SCIENTIST 'scientist or biologist or chemist or physicist or mathematician'
# jsub -mem 4g -cwd ./item_candidates.php FAMOUS 'famous'
# jsub -mem 4g -cwd ./item_candidates.php PUBLIC_ART 'public art'
# jsub -mem 4g -cwd ./item_candidates.php MICROSCOPY 'microscopy'
# jsub -mem 4g -cwd ./item_candidates.php SAO_PAULO 'Sao Paulo'
# jsub -mem 4g -cwd ./item_candidates.php MOSAIC 'mosaic'

?>