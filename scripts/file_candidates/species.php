#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);
ini_set('display_errors', 'On');

include_once ( '/data/project/fist/FileCandidates.php' ) ;
$fc = new FileCandidates ;
$fc->connectCommonsDB() ;

function newSpeciesBatchGenerator ( $max_batch_size=5000 ) {
	global $fc ;
	$limit = 1200000 ;
	$offset = 0 ;
	while ( 1 ) {
		$sparql = 'SELECT ?q ?name { ?q wdt:P31 wd:Q16521; wdt:P225 ?name ; wdt:P105 wd:Q7432 . OPTIONAL { ?q wdt:P18 ?img } FILTER(!bound(?img)) }' ;
		if ( isset($limit) ) $sparql .= " LIMIT $limit" ;
		if ( $offset > 0 ) $sparql .= " OFFSET $offset" ;

		$q2name = [] ;
		$cnt = 0 ;
		foreach ( $fc->tfc->getSPARQL_TSV($sparql) AS $b ) {
			$cnt++ ;
			$q = preg_replace ( '/^.+Q/' , 'Q' , $b['q'] ) ;
			$name = $b['name'] ;
			$q2name[$q] = $name ;
			if ( count($q2name) < $max_batch_size ) continue ;

			# Remove items that already exist
			$existing_q = array_keys($fc->getFileCandidatesForItems(array_keys($q2name))) ;
			foreach ( $existing_q AS $q ) unset($q2name[$q]) ;
			if ( count($q2name)>0 ) yield $q2name ;
			$q2name = [] ;
		}

		# Flush
		$existing_q = array_keys($fc->getFileCandidatesForItems(array_keys($q2name))) ;
		foreach ( $existing_q AS $q ) unset($q2name[$q]) ;
		if ( count($q2name)>0 ) yield $q2name ;

		if ( $cnt == 0 ) break ; // All done
		$offset += $limit ;
	}
	yield from [] ;
}

foreach ( newSpeciesBatchGenerator() AS $q2name ) {
	foreach ( $q2name AS $q => $name ) {
		$query = '"' . $name . '"' ;

		// Flickr
		$files = [];#$fc->searchFlickr ( $query ) ; # DEACTIVATED API KEY ISSUE
		foreach ( $files AS $file ) {
			print_r ( $file ) ;
			$fc->addFile ( [
				'q' => $q ,
				'json' => $file ,
				'group' => 'SPECIES' ,
				'source' => 'FLICKR' ,
				'file_id' => $file['@attributes']['id']
			] ) ;
		}

		// Categories on Commons
		$sql = "SELECT page_title FROM page,categorylinks WHERE cl_from=page_id AND page_namespace=6 AND cl_to='" . $fc->escape(str_replace(' ','_',$name)) . "' LIMIT 10" ;
		$result = getSQL ( $fc->dbc , $sql ) ;
		while($o = $result->fetch_object()){
			$file = $fc->getCommonsImageInfo ( $o->page_title ) ;
			if ( !isset($file) ) continue ;
			if ( $fc->getFileTypeByExtension($file->title) == 'DOCUMENT' ) continue ;
			$fc->addFile ( [
				'q' => $q ,
				'json' => $file ,
				'group' => 'SPECIES' ,
				'source' => 'COMMONS' ,
				'file_id' => $file->pageid
			] ) ;
		}

		// Fulltext search Commons
		$files = $fc->searchCommons ( $query ) ;
		foreach ( $files AS $file ) {
			if ( $fc->getFileTypeByExtension($file->title) == 'DOCUMENT' ) continue ;
			$fc->addFile ( [
				'q' => $q ,
				'json' => $file ,
				'group' => 'SPECIES' ,
				'source' => 'COMMONS' ,
				'file_id' => $file->pageid
			] ) ;
		}
	}
}

?>