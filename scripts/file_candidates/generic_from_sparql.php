#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);
ini_set('display_errors', 'On');

require_once ( '/data/project/fist/scripts/file_candidates/FileCandidatesMaintenance.php' ) ;

$fcm = new FileCandidatesMaintenance ;
$fcm->genericFromSparql($argv[1]);

$fcm->fc->tfc->showProcInfo();

?>