#!/usr/bin/php
<?php

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( '/data/project/fist/FileCandidates.php') ;

$max_files_per_search_result = 20 ;

$fc = new FileCandidates ;

$sparql = 'SELECT ?q ?cat { ?q wdt:P31/wdt:P279* wd:Q16970 ; wdt:P373 ?cat MINUS { ?q wdt:P18 [] } }' ;

$j = getSPARQL ( $sparql ) ;

$dbc = openDB ( 'commons' , 'wikimedia' ) ;
foreach ( $j->results->bindings AS $b ) {
	$q = preg_replace ( '/^.+\/Q/' , 'Q' , $b->q->value ) ;
	$commonscat = preg_replace('| |','_',ucfirst(trim($b->cat->value))) ;
	$commonscat_safe = $fc->escape($commonscat) ;
	$sql = "SELECT DISTINCT `page_title` FROM `page`,`categorylinks` WHERE `cl_to`='{$commonscat_safe}' AND `cl_from`=`page_id` AND `page_namespace`=6 ORDER BY page_len DESC LIMIT 10" ;
	$result = $fc->tfc->getSQL ( $dbc , $sql ) ;
	while ( $o = $result->fetch_object() ) {
		if ( !preg_match ( '/\.(jpg|jpeg)$/i' , $o->page_title ) ) continue ;
		$file = $fc->getCommonsImageInfo ( $o->page_title ) ;
		if ( !isset($file) ) continue ;
		$fc->addFile ( [
			'q' => $q ,
			'group' => 'CHURCH2' ,
			'source' => 'COMMONS' ,
			'file_type' => 'IMAGE' ,
			'file_id' => $file->pageid ,
			'json' => $file
		] ) ;
	}
}

?>