#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);
ini_set('display_errors', 'On');

include_once ( '/data/project/fist/FileCandidates.php' ) ;
$fc = new FileCandidates ;

function bioacoustica_get_spec2files() {
	global $fc ;
	$spec2files = [] ;
	$url = 'https://petscan.wmflabs.org/?psid=2922680&format=csv' ; // BioAcoustica files
	foreach ( $fc->tfc->csvUrlGenerator($url) AS $entry ) {
		$file = $entry['title'] ;
		$file2 = preg_replace ( '/_/' , ' ' , $file ) ;
		$file2 = preg_replace ( '/ MHV /' , ' ' , $file2 ) ;
		if ( !preg_match ( '/^BioAcoustica [0-9 \-]+ (.+?)[0-9.-]/' , $file2 , $m ) ) continue ;
		$species = trim(preg_replace('/\s*\(.+?\)\s*/',' ',ucfirst(trim($m[1])))) ;
		$spec2files[$species][] = $file ;
	}
	return $spec2files ;
}

function bioacoustica() {
	global $fc ;
	foreach ( bioacoustica_get_spec2files() AS $species => $files ) {
		$query = "haswbstatement:P31=Q16521 haswbstatement:\"P225={$species}\" -haswbstatement:P51" ;
		$items = $fc->getCachedWikidataSearch($query);
		if ( count($items) != 1 ) continue ;
		$q = $items[0] ;
		foreach ( $files AS $file ) {
			$j = $fc->getCommonsImageInfo ( $file ) ;
			if ( $j === null ) continue ; // Paranoia
			$fc->addFile ( [
				'q' => $q ,
				'json' => $j ,
				'group' => 'TAXON AUDIO' ,
				'source' => 'COMMONS' ,
				'comment' => 'BioAcoustica file' ,
				'file_id' => $j->pageid ,
				'file_type' => 'AUDIO'
			] ) ;
		}
		
	}
}

# Audio
$petscan_url = 'https://petscan.wmflabs.org/?psid=11247114&format=csv' ;
$group_name = 'TAXON AUDIO' ;
$field_type = 'AUDIO' ;
$file_type_pattern = '/\.(ogg|mp3|wav|oga|flac|webm|mid)$/i' ;
$fc->addTaxonMedia ( $petscan_url , $group_name , $field_type , $file_type_pattern ) ;
bioacoustica();

# Video
$petscan_url = 'https://petscan.wmflabs.org/?psid=11242068&format=csv' ;
$group_name = 'TAXON_VIDEO' ;
$field_type = 'VIDEO' ;
$file_type_pattern = '' ;
$fc->addTaxonMedia ( $petscan_url , $group_name , $field_type , $file_type_pattern ) ;

?>