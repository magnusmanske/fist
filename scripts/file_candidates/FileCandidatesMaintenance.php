<?PHP

require_once ( '/data/project/fist/FileCandidates.php' ) ;

class FileCandidatesMaintenance {
	public $fc ;
	public $crd_max_files_per_item = 4 ;
	protected $crd_bad_qs = [] ;
	protected $crd_item_link_counterindications = [
		'Q4167836' , # category
		'Q11266439' , # template
		'Q202444' , # given name
		'Q12308941' , # male given name
		'Q11879590' , # female given name
		'Q202444' , # unisex name
		'Q4167410' , # disambiguation page
		'Q3624078' , # sovereign state
		'Q13406463' , # Wikimedia list article
		'Q14204246' , # Wikimedia project page
		'Q101352' , # family name
		'Q13442814' , # scholarly article
		'Q235729' # common year
	] ;
	protected $template2sparql = [ # Template names need to be DB safe!
		'Listed_building_England' => 'select ?q { ?q wdt:P1216 "$ID" }' ,
		'Listed_building_Wales' => 'select ?q { ?q wdt:P1459 "$ID" }' ,
		'Listed_building_Northern_Ireland' => 'select ?q { ?q wdt:P1460 "$ID" }' ,
		'Listed_building_Scotland' => 'select ?q { ?q wdt:P709 "$ID" }'
	] ;
	protected $on_wikidata_config_file = '/data/project/fist/scripts/file_candidates/on_wikidata.json' ;
	protected $gfs_config_file = '/data/project/fist/scripts/file_candidates/config.json' ;

	function __construct ( $fc = '' ) {
		$this->fc = is_object($fc) ? $fc : new FileCandidates ;
	}

	public function wdDepicts ( $max_files_per_search_result = 20 ) {
		$sparql = 'SELECT ?q ?file { ?img wdt:P180 ?q ; wdt:P18 ?file . MINUS { ?q wdt:P18 [] } }' ;
		foreach ( $this->fc->tfc->getSPARQL_TSV($sparql) AS $b ) {
			$q = preg_replace ( '/^.+\/Q/' , 'Q' , $b['q'] ) ;
			$filename = preg_replace ( '/^.+?Special:FilePath\//' , '' , $b['file'] ) ;
			$filename = urldecode ( $filename ) ;
			if ( preg_match ( '/\.(pdf|svg|og.|mp.)$/i' , $filename ) ) continue ;

			$file = $this->fc->getCommonsImageInfo ( $filename ) ;
			if ( !isset($file) ) continue ;
			
			$this->fc->addFile ( [
				'q' => $q ,
				'json' => $file ,
				'group' => 'WD_DEPICTS' ,
				'source' => 'COMMONS' ,
				'file_id' => $file->pageid
			] ) ;
		}
	}

	public function genericFromSparql ( $set_name ) {
		$sets = json_decode ( file_get_contents ( $this->gfs_config_file ) ) ;
		foreach ( $sets AS $set ) {
			if ( strtoupper($set_name) != $set->group ) continue ; # Note that this allows for multiple configurations for the same group!
			$this->genericFromSparqlRunSet ( $set ) ;
		}
	}

	public function commonsRandomDepicts ( $iterations = 500 ) {
		$this->get_crd_bad_qs() ;
		$this->reset_crd_bad_qs() ;
		for ( $cnt = 0 ; $cnt < $this->iterations ; $cnt++ ) $this->process_batch() ;
		$this->get_crd_bad_qs() ;
		$this->reset_crd_bad_qs() ;
	}

	public function maintenance() {
		$this->fc->connectCommonsDB() ;

		# Try to find Flickr files on Commons, and replace them in file_candidates
		$attr = '@attributes' ;
		$sql = 'SELECT id,json,file_id FROM file_candidates WHERE source="FLICKR" AND status="WAIT"' ;
		$result = getSQL ( $this->fc->dbt , $sql ) ;
		while($o = $result->fetch_object()) {
			$j = json_decode ( $o->json ) ;

			if ( !isset($j->$attr) ) continue ; # TODO error

			$urls = [] ;
			$urls[] = "http://flickr.com/photos/" . $j->$attr->owner . '/' . $o->file_id ;
			$urls[] = "https://flickr.com/photos/" . $j->$attr->owner . '/' . $o->file_id ;
			$urls[] = "http://www.flickr.com/photos/" . $j->$attr->owner . '/' . $o->file_id ;
			$urls[] = "https://www.flickr.com/photos/" . $j->$attr->owner . '/' . $o->file_id ;
			foreach ( $urls AS $k => $v ) {
				if ( preg_match ( '/\/$/',$v) ) break ;
				$urls[] = "$v/" ;
			}

			$sql = "SELECT * FROM page,externallinks WHERE page_id=el_from AND page_namespace=6 AND el_to IN ('" . implode ( "','",$urls) . "')" ;
			$commons_files = [] ;
			$result2 = getSQL ( $this->fc->dbc , $sql ) ;
			while($o2 = $result2->fetch_object()) $commons_files[] = $o2 ;
			if ( count($commons_files) != 1 ) continue ;

			$o2 = $commons_files[0] ;
			$file = $this->fc->getCommonsImageInfo ( $o2->page_title ) ;
			if ( !isset($file) ) continue ; # Paranoia
			$j = $this->fc->escape ( json_encode ( $file ) ) ;
			if ( $j == '' ) continue ;
			$sql = "UPDATE IGNORE file_candidates SET `file_id`='{$file->pageid}',`source`='COMMONS',`json`='$j' WHERE id={$o->id} AND `source`='FLICKR' AND `file_id`='{$o->file_id}'" ;
		#	print "$sql\n" ;
			getSQL ( $this->fc->dbt , $sql ) ;
		}
	}

	public function onWikidata() {
		$this->fc->connectCommonsDB() ;
		$config_json = json_decode ( file_get_contents ( $this->on_wikidata_config_file ) ) ;

		$sql = "SELECT DISTINCT page_title FROM templatelinks,linktarget,page
			WHERE page_id=tl_from
			AND linktarget.lt_id=tl_target_id
			AND lt_namespace=10
			AND tl_from_namespace=6
			AND `page_touched`>='{$config_json->last_page_touched}'
			AND lt_title IN ('On_Wikidata','" . str_replace(' ','_',implode("','",array_keys($this->template2sparql))) . "')
			AND NOT EXISTS (SELECT * FROM imagelinks WHERE il_to=page_title AND il_from_namespace=0 LIMIT 1)
			ORDER BY `page_touched`
			LIMIT 2500
			" ; # ORDER BY rand()

		$config_json->last_page_touched = date ( 'YmdHis' , time() - 60*10 ) ; # 10 min ago

		$files_processed = 0 ;
		$result = $this->fc->tfc->getSQL ( $this->fc->dbc , $sql ) ;
		while ($o = $result->fetch_object()) {
			$files_processed++ ;
			$q = '' ;
			$file = $o->page_title ;
		#print "Checking {$file}...\n" ;

			$q = $this->guessItemForFile ( $file ) ;
			if ( $q == '' ) continue ;


			# Follow redirect, if necessary
			$sql = "SELECT * FROM page,pagelinks,linktarget WHERE pl_target_id=lt_id AND page_namespace=0 AND page_title='{$q}' AND page_is_redirect=1 AND pl_from=page_id AND lt_namespace=0" ;
			$result2 = $this->fc->tfc->getSQL ( $this->fc->dbw , $sql ) ;
			if ($o2 = $result2->fetch_object()) $q = $o2->lt_title ;

			# Check for image
			if ( $this->fc->doesItemHaveImage ( $q ) ) continue ;

			# Add image candidate
			$j = $this->fc->getCommonsImageInfo ( $file ) ;
			if ( $j === null ) continue ; // Paranoia
		#	if ( $this->fc->doesFileCandidateExists ( 'COMMONS' , $j->pageid ) ) continue ;
			$this->fc->addFile ( [
				'q' => $q ,
				'json' => $j ,
				'group' => 'ON WIKIDATA' ,
				'source' => 'COMMONS' ,
				'comment' => 'Via {{On Wikidata}}' ,
				'file_id' => $j->pageid ,
				'file_type' => 'IMAGE'
			] ) ;
		}

		print "Files processed: {$files_processed}\n" ;
		file_put_contents ( $this->on_wikidata_config_file , json_encode ( $config_json ) ) ;
	}


	protected function genericFromSparqlRunSet ( $set ) {
		print "{$set->sparql}\n" ;
		$j = getSPARQL ( $set->sparql ) ;
		$last_query = '' ;

		if ( !isset($j) or !isset($j->results) ) return ;

		foreach ( $j->results->bindings AS $b ) {
			$q = preg_replace ( '/^.+Q/' , 'Q' , $b->q->value ) ;

			// Construct query and skip if necessary
			$skip_item = false ;
			$query = [] ;
			foreach ( $set->parts AS $part ) {
				$var = $part->var ;
				$value = trim ( $b->$var->value ) ; # TODO ensure string
				if ( preg_match ( '/^Q\d+$/' , $value ) ) $value = '' ; # If value is a Wikidata item, pretend it's empty
				if ( $value == '' and $part->required ) $skip_item = true ;
				if ( $value == '' ) continue ;
				if ( $part->quote ) $value = "'" . $value . "'" ;
				$query[] = $value ;
			}
			if ( $skip_item ) continue ;
			$query = trim ( implode ( ' ' , $query ) ) ;
			if ( $query == '' or "$q$query" == $last_query ) continue ; // Paranoia
			$last_query = "$q$query" ;

			// Check if this item already has candidates; search only for ones that don't
			$existing_candidates = $this->fc->getFileCandidatesForItems ( [ $q ] ) ;
			if ( count($existing_candidates) > 0 ) continue ;

			if ( isset($set->commons) and $set->commons ) { // Fulltext search Commons
				try {
					$files = $this->fc->searchCommons ( $query ) ;
					foreach ( $files AS $file ) {
						if ( isset($set->exclude_files) ) {
							if ( preg_match ( $set->exclude_files , $file->title ) ) continue ;
						}
						$this->fc->addFile ( [
							'q' => $q ,
							'json' => $file ,
							'group' => $set->group ,
							'source' => 'COMMONS' ,
							'file_id' => $file->pageid
						] ) ;
					}
				} catch(Exception $e) {
					# TODO
				}
			}

			if ( isset($set->flickr) and $set->flickr ) { // Flickr search
				try {
					$files = $this->fc->searchFlickr ( $query ) ;
					foreach ( $files AS $file ) {
						if ( !isset($file['@attributes']) ) continue ; // Paranoia
						$this->fc->addFile ( [
							'q' => $q ,
							'json' => $file ,
							'group' => $set->group ,
							'source' => 'FLICKR' ,
							'file_id' => $file['@attributes']['id']
						] ) ;
					}
				} catch(Exception $e) {
					# TODO
				}
			}

		}
	}



	protected function guessItemForFile ( $file ) {
		$q = '' ;
		$wikitext = $this->fc->tfc->getWikiPageText ( 'commonswiki' , 'File:'.$file ) ;
		$wikitext = preg_replace ( '/\s+/' , ' ' , $wikitext ) ;
		$r = 0 ;
		if ( $r == 0 ) $r = preg_match ( '/\{\{ *[oO]n[ _]Wikidata *\| *([Qq]\d+)/' , $wikitext , $m ) ;
		if ( $r == 0 ) $r = preg_match ( '/\{\{ *Wikidata *\| *([Qq]\d+)/' , $wikitext , $m ) ;
		if ( $r == 0 ) {
			unset ( $m ) ;
			foreach ( $this->template2sparql AS $template => $sparql ) {
				$template = str_replace ( ' ' , '_' , $template ) ;
				$pattern = str_replace ( '_' , '[ _]' , $template ) ;
				$pattern = '/\{\{ *' . $pattern . ' *\| *([^ \|\}]+)/i' ;
				$r = preg_match ( $pattern , $wikitext , $m2 ) ;
				if ( $r == 0 ) continue ;
				$id = $m2[1] ;
				$id = preg_replace ( '/^.+=/' , '' , $id ) ; # Remove template parameter, if any
				$sparql = str_replace ( '$ID' , $id , $sparql ) ;
				$items = $this->fc->tfc->getSPARQLitems ( $sparql ) ;
				if ( count($items) != 1 ) continue ;
				$q = $items[0] ;
				return $q ;
			}
		}
		if ( $q == '' and $r == 0 ) return '' ;
		if ( $q == '' and isset($m) ) $q = $m[1] ;
		$q = preg_replace ( '/^.+=/' , '' , strtoupper ( $q ) ) ; # Remove template parameter, if any
		$q = trim ( $q ) ;
		if ( !preg_match ( '/^Q\d+$/' , $q ) ) return '' ; # Paranoia
		return $q ;
	}

	protected function get_crd_bad_qs() {
		$this->crd_bad_qs = [] ;
		$sql = "SELECT q,count(*) AS cnt FROM file_candidates WHERE `group`='COMMONS_DEPICTS' GROUP BY q HAVING cnt>$this->crd_max_files_per_item" ;
		$result = $this->fc->tfc->getSQL ( $this->fc->dbt , $sql ) ;
		while($o = $result->fetch_object()) $this->crd_bad_qs["Q{$o->q}"] = "Q{$o->q}" ;
	}

	protected function reset_crd_bad_qs() {
		if ( count($this->crd_bad_qs) == 0 ) return ;
		$qs = implode ( ',' , $this->crd_bad_qs ) ;
		$qs = preg_replace ( '|Q|' , '' , $qs ) ;
		$sql = "UPDATE file_candidates SET `status`='SKIPPED' WHERE `status`='WAIT' AND q IN ({$qs}) AND `group`='COMMONS_DEPICTS'" ;
		$this->fc->tfc->getSQL ( $this->fc->dbt , $sql ) ;
	}

	protected function process_batch () {
		global $wikidata_api_url  ;
		$files = $this->fc->searchCommons ( 'haswbstatement:P180' , 6 , 50 , 'random') ;

		# Get image names to query in Wikidata, and Media IDs
		$image_names = [] ;
		$media_items = [] ;
		foreach ( $files as $page_id => $file ) {
			if ( !isset($file->imageinfo) ) continue ; # Paranoia to code around https://phabricator.wikimedia.org/T253373
			$filename = $this->fc->normalizeCommonsFilename ( $file->title ) ;
			$mid = "M{$page_id}" ;
			$image_names[] = $this->fc->escape ( $filename ) ;
			$media_items[] = $mid ;
			$mid2filename[$mid][$filename] = $filename ;
		}

		# Get MediaInfo for these files
		$wikidata_api_url_old = $wikidata_api_url ;
		$wikidata_api_url = "https://commons.wikimedia.org/w/api.php" ;
		$mid2wd = [] ;
		$wd2mid = [] ;
		$cowil = new WikidataItemList ;
		$cowil->loadItems ( $media_items ) ;
		foreach ( $media_items AS $mid ) {
			$i = $cowil->getItem ( $mid ) ;
			if ( !isset($i) ) {
				print "Could not get {$mid}\n" ;
				continue ;
			}
			$p180s = $i->getClaims ( 'P180' ) ;
			foreach ( $p180s AS $claims ) {
				foreach ( $claims AS $claim ) {
					if ( !isset($claim->datavalue) ) continue ;
					if ( !isset($claim->datavalue->value) ) continue ;
					if ( !isset($claim->datavalue->value->id) ) continue ;
					$q = $claim->datavalue->value->id ;
					$wd2mid[$q][$mid] = $mid ;
					$mid2wd[$mid][$q] = $q ;
				}
			}
		}
		$wikidata_api_url = $wikidata_api_url_old ;

		# Get the potential items without images
		$item_link_counterindications2 = implode ( "','" , $this->crd_item_link_counterindications ) ;
		$items_without_p18 = [] ;
		$sql = implode("','", array_keys($wd2mid) ) ;
		$sql = "SELECT DISTINCT page_title FROM page WHERE page_namespace=0 AND page_title IN ('{$sql}')"  ;
		$sql .= " AND NOT EXISTS (SELECT * FROM pagelinks,linktarget WHERE pl_target_id=lt_id AND lt_namespace=120 AND lt_title='P18' AND pl_from=page_id)" ; # No image
		$sql .= " AND NOT EXISTS (SELECT * FROM pagelinks,linktarget WHERE pl_target_id=lt_id AND lt_namespace=0 AND lt_title IN ('{$item_link_counterindications2}') AND pl_from=page_id)" ; # No category etc
		$sql .= " AND NOT EXISTS (SELECT * FROM pagelinks,linktarget WHERE pl_target_id=lt_id AND lt_namespace=120 AND lt_title='P279' AND pl_from=page_id)" ; # No subclass
		$sql .= " AND EXISTS (SELECT * FROM pagelinks,linktarget WHERE pl_target_id=lt_id AND lt_namespace=120 AND lt_title='P31' AND pl_from=page_id)" ; # But P31
		$result = $this->fc->tfc->getSQL ( $this->fc->dbw , $sql ) ;
		while($o = $result->fetch_object()) {
			$q = $o->page_title ;
			if ( !isset($wd2mid[$q]) ) continue ;
			$items_without_p18[] = $q ;
		}

		# Get existing image links for the ones without image, to exclude logos etc
		$item_using_file = [] ;
		$sql = implode ( "','" , $items_without_p18 ) ;
		$sql = "SELECT page_title,il_to FROM page,imagelinks WHERE il_from=page_id AND page_namespace=0 AND page_title IN ('{$sql}')" ;
		$result = $this->fc->tfc->getSQL ( $this->fc->dbw , $sql ) ;
		while($o = $result->fetch_object()) {
			$filename = $this->fc->normalizeCommonsFilename ( $o->il_to ) ;
			$q = $o->page_title ;
			$item_using_file["{$q}:{$filename}"] = 1 ;
		}

		$item_using_file = [] ;
		foreach ( $items_without_p18 AS $q ) {
			if ( in_array ( $q , $this->crd_bad_qs ) ) continue ;
			foreach ( $wd2mid[$q] as $mid ) {
				foreach ( $mid2filename[$mid] as $filename ) {
					$k = "{$q}:{$filename}" ;
					if ( isset($item_using_file[$k]) ) continue ;
					$item_using_file[$k] = 1 ;
					$page_id = substr($mid,1) * 1 ;
					#print "Potential image for page {$page_id} ({$q}): $filename\n" ;
					$file = $files->$page_id ;
					if ( !isset($file) ) {
						print "No file for page {$page_id}\n" ;
						continue ;
					}
						$this->fc->addFile ( [
							'q' => $q ,
							'json' => $file ,
							'group' => 'COMMONS_DEPICTS' ,
							'source' => 'COMMONS' ,
							'file_id' => $file->pageid
						] ) ;

				}
			}
		}
	}
}

?>