#!/usr/bin/php
<?PHP

require_once ( "/data/project/fist/public_html/php/ToolforgeCommon.php" ) ;

class WD4WP {
	public $langs = [ 'de' , 'es' , 'it' , 'ru' , 'nl' , 'ca' , 'en' , 'cy' , 'pt' , 'fr' , 'zh' , 'yo' , 'ha', 'or' ] ;
	public $tfc ;
	protected $dbt ;

	function __construct() {
		$this->tfc = new ToolforgeCommon ( 'wd4wp' ) ;
		$this->dbt = $this->tfc->openDBtool ( 'wd4wp_p' ) ;
		$this->tfc->use_db_cache = false ;
	}

	protected function normalize_title ( $title ) {
		return str_replace ( ' ' , '_' , ucfirst ( trim ( str_replace ( '_' , ' ' , $title ) ) ) ) ;
	}

	protected function escape ( $s ) {
		return $this->dbt->real_escape_string ( $s ) ;
	}

	public function get_all_wikis () {
		$ret = [] ;
		$sql = "SELECT `id`,`name` FROM `wiki`" ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
		while($o = $result->fetch_object()) $ret[$o->name] = $o->id ;
		return $ret ;
	}

	protected function get_or_create_wiki_id ( $wiki ) {
		$wiki = $this->escape ( $wiki ) ;
		$sql = "SELECT `id` FROM `wiki` WHERE `name`='{$wiki}'" ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
		while($o = $result->fetch_object()) return $o->id ;
		$sql = "INSERT IGNORE INTO `wiki` (`name`) VALUES ('{$wiki}')" ;
		$this->tfc->getSQL ( $this->dbt , $sql ) ;
		return $this->dbt->insert_id ;
	}

	protected function is_valid_image ( $image ) {
		if ( preg_match ( '|\.svg$|i' , $image ) ) return false ; # Ignore SVG
		# TODO more?
		return true ;
	}

	protected function add_articles ( $wiki_id , $q2article ) {
		$items = array_keys ( $q2article ) ;
		$sparql = "SELECT ?q ?image ?instance_of { VALUES ?q { wd:" . implode ( ' wd:' , $items ) . " } ?q wdt:P18 ?image OPTIONAL { ?q wdt:P31 ?instance_of } }" ;
		$j = $this->tfc->getSPARQL ( $sparql ) ;
		if ( !isset($j) or !isset($j->results) or !isset($j->results->bindings) ) return ;
		$data = [] ;
		foreach ( $j->results->bindings AS $v ) {
			$q = preg_replace ( '/^.+\//' , '' , $v->q->value ) ;
			$img = $this->normalize_title ( urldecode ( preg_replace ( '/^.+\//' , '' , $v->image->value ) ) ) ;
			if ( !$this->is_valid_image($img) ) continue ; # Paranoia
			if ( !isset($data[$q]) ) $data[$q] = [ 'q' => $q , 'image' => $img , 'title' => $q2article[$q] , 'io' => [] ] ;
			if ( isset($v->instance_of) ) $data[$q]['io'][] = preg_replace ( '/^.+\//' , '' , $v->instance_of->value ) ;
		}

		foreach ( $data AS $q => $page ) {
			$title = $this->escape ( $page['title'] ) ;
			$image = $this->escape ( $page['image'] ) ;
			$q = preg_replace ( '|\D|' , '' , $page['q'] ) * 1 ;
			$sql = "INSERT IGNORE INTO `page` (`wiki`,`title`,`q`,`image`) VALUES ({$wiki_id},'{$title}',{$q},'{$image}')" ;
			$this->tfc->getSQL ( $this->dbt , $sql ) ;
			$page_id = $this->dbt->insert_id ;
			if ( count($page['io']) == 0 ) continue ;
			$page_instance = [] ;
			foreach ( $page['io'] AS $p31 ) {
				$p31 = preg_replace ( '|\D|' , '' , $p31 ) * 1 ;
				$page_instance[] = "({$page_id},{$p31})" ;
			}
			$sql = "INSERT IGNORE INTO `page_instance` (`page_id`,`p31`) VALUES " . implode ( ',' , $page_instance ) ;
			$this->tfc->getSQL ( $this->dbt , $sql ) ;
		}
	}

	public function update_all () {
		foreach ( $this->langs AS $lang ) {
			$this->update_wiki ( "{$lang}wiki" ) ;
		}
	}

	protected function candidate_articles_generator ( $wiki , &$existing_articles ) {
		$sql_base = 'SELECT page_id,page_title AS lp_title,pp1.pp_value AS q FROM page local_page,page_props pp1
		WHERE local_page.page_is_redirect=0 AND local_page.page_namespace=0 AND NOT EXISTS (SELECT * FROM page_props pp2 WHERE local_page.page_id=pp2.pp_page AND pp2.pp_propname IN ("page_image","page_image_free") ) 
		AND local_page.page_id=pp1.pp_page AND pp1.pp_propname="wikibase_item"' ;
		$limit = 50000 ;
		$last_page_id = 0 ;
		$this->dbt->close() ; # Might lose connectivity during this long query
		$dbw = $this->tfc->openDBwiki ( $wiki ) ;
		$dbwd = $this->tfc->openDBwiki ( 'wikidatawiki' ) ;

		$has_result = true ;
		while ( $has_result ) {
			$has_result = false ;
			$sql = "{$sql_base} AND page_id>{$last_page_id} ORDER BY page_id LIMIT {$limit}" ;
			$result = $this->tfc->getSQL ( $dbw , $sql ) ;
			$tmp = [] ;
			while($o = $result->fetch_object()) {
				$last_page_id = $o->page_id ;
				$has_result = true ;
				$title = $this->normalize_title ( $o->lp_title ) ;
				if ( isset($existing_articles[$title]) ) {
					$existing_articles[$title] = 'FOUND' ;
					continue ;
				}
				$q = $o->q ;
				//if ( isset ( $q2article[$q] ) ) continue ;
				//$q2article[$q] = $this->normalize_title ( $title ) ;
				$tmp[$q] = $this->normalize_title($title) ;
			}
			if ( count($tmp) > 0 ) {
				$qs = "'".implode("','",array_keys($tmp))."'" ;
				$sql = "SELECT page_title FROM page,pagelinks,linktarget WHERE pl_target_id=lt_id AND page_namespace=0 AND page_title IN ({$qs}) AND pl_from=page_id AND lt_namespace=120 AND lt_title='P18'";
				$result = $this->tfc->getSQL ( $dbwd , $sql ) ;
				while($o = $result->fetch_object()) {
					$q = $o->page_title ;
					yield ['q'=>$q,'title'=>$tmp[$q]];
				}
			}
		}
		
		$dbw->close() ;
		$dbwd->close() ;
		$this->dbt = $this->tfc->openDBtool ( 'wd4wp_p' ) ;
	}

	protected function get_candidate_articles ( $wiki , &$existing_articles ) {
		$q2article = [] ;
		foreach ( $this->candidate_articles_generator($wiki,$existing_articles) AS $arr ) {
			$q2article[$arr['q']] = $arr['title'];
		}
		return $q2article ;
	}

	protected function remove_non_candidates ( $wiki_id , $existing_articles ) {
		$titles_to_remove = [] ;
		foreach ( $existing_articles AS $title => $status ) {
			if ( $status == 'FOUND' ) continue ;
			$titles_to_remove[] = $this->escape($title) ;
		}
		if ( count($titles_to_remove) == 0 ) return ;

		$sql = "UPDATE `page` SET `status`='NO_LONGER_VALID' WHERE `wiki`={$wiki_id} AND `title` IN ('".implode("','",$titles_to_remove)."') AND `status`='TODO'" ;
		$this->tfc->getSQL ( $this->dbt , $sql ) ; # TODO
	}

	public function update_wiki ( $wiki ) {
		$wiki_id = $this->get_or_create_wiki_id ( $wiki ) ;
		$this->set_last_update_now ( $wiki_id ) ;

		$this->check_if_image_links_still_exist ( $wiki ) ;

		$existing_articles = [] ;
		$sql = "SELECT `title`,`status` FROM `page` WHERE `wiki`={$wiki_id}" ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
		while($o = $result->fetch_object()) $existing_articles[$o->title] = $o->status ;

		$q2article = $this->get_candidate_articles ( $wiki , $existing_articles ) ;
		$this->remove_non_candidates ( $wiki_id , $existing_articles ) ;
		unset ( $existing_articles ) ;

		$chunks = array_chunk ( $q2article , 100 , true ) ;
		unset ( $q2article ) ;
		foreach ( $chunks AS $q2article ) $this->add_articles ( $wiki_id , $q2article ) ;
		$this->mark_pages_using_candidate_images_as_done ( $wiki ) ;
	}

	protected function set_last_update_now ( $wiki_id ) {
		$sql = "UPDATE `wiki` SET `last_update`=now() WHERE id={$wiki_id}" ;
		$this->tfc->getSQL ( $this->dbt , $sql ) ;
	}

	public function update_next_wiki() {
		$sql = "SELECT `name` FROM `wiki` ORDER BY `last_update` LIMIT 1" ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
		$o = $result->fetch_object() ;
		$this->update_wiki ( $o->name ) ;
	}

	public function mark_pages_using_candidate_images_as_done ( $wiki ) {
		$wiki_id = $this->get_or_create_wiki_id ( $wiki ) ;
		$dbwp = $this->tfc->openDBwiki ( $wiki ) ;
		$limit = 1000 ;
		$start_id = 0 ;
		while ( true ) {
			$sql = "SELECT `id`,`title`,`image` FROM `page` WHERE `id`>{$start_id} AND `wiki`={$wiki_id} AND `status`='TODO' ORDER BY `id` LIMIT {$limit}" ;
			$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
			$sql = [] ;
			$pages = [] ;
			while($o = $result->fetch_object()) {
				$pages[] = $this->escape($o->title) ;
				$sql[] = "`page_title`='".$this->escape($o->title)."' AND il_to='".$this->escape($o->image)."'" ;
				$start_id = $o->id ;
			}
			if ( count($sql) == 0 ) break ; # Done
			$sql = "SELECT `page_title` FROM `page`,`imagelinks` WHERE `page_namespace`=0 AND `page_id`=`il_from` AND ((" . implode(') OR (',$sql) . '))' ;
			#$sql .= " AND `page_title` IN ('".implode("','",$pages)."')";
			$result = $this->tfc->getSQL ( $dbwp , $sql ) ;
			$escaped_pages = [] ;
			while($o = $result->fetch_object()) $escaped_pages[] = $this->escape($o->page_title) ;
			if ( count($escaped_pages) == 0 ) continue ;
			$sql = "UPDATE `page` SET `status`='ALREADY_LINKS' WHERE `wiki`={$wiki_id} AND `title` IN ('".implode("','",$escaped_pages)."') AND `status`='TODO'";
			$this->tfc->getSQL ( $this->dbt , $sql ) ;
		}
	}

	public function check_if_image_links_still_exist ( $wiki ) {
		$wiki_id = $this->get_or_create_wiki_id ( $wiki ) ;
		
		$dbwd = $this->tfc->openDBwiki ( 'wikidatawiki' ) ;
		$limit = 10000 ;
		$start_id = 0 ;
		while ( true ) {
			$sql = "SELECT `id`,`q`,`image` FROM `page` WHERE `id`>{$start_id} AND `wiki`={$wiki_id} and `status`='TODO' ORDER BY `id` LIMIT {$limit}" ;
			$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
			$sql = [] ;
			$expected = [] ;
			while($o = $result->fetch_object()) {
				$sql[] = "page_title='Q{$o->q}' AND il_to='".$this->escape($o->image)."'" ;
				$expected["Q{$o->q}|{$o->image}"] = $o->id ;
				$start_id = $o->id ;
			}
			if ( count($sql) == 0 ) break ;
			$sql = "SELECT `page_title`,`il_to` FROM `page`,`imagelinks` WHERE `page_namespace`=0 AND `page_id`=`il_from` AND ((" . implode(') OR (',$sql) . '))' ;
			$result = $this->tfc->getSQL ( $dbwd , $sql ) ;
			while($o = $result->fetch_object()) {
				$key = "{$o->page_title}|{$o->il_to}" ;
				unset($expected[$key]);
			}
			if ( count($expected) == 0 ) continue ;

			$sql = "UPDATE `page` SET `status`='NO_LONGER_VALID' WHERE `id` IN ('".implode("','",$expected)."') AND `status`='TODO'" ;
			$this->tfc->getSQL ( $this->dbt , $sql ) ;
		}
	}
}


$wd4wp = new WD4WP ;
$command = $argv[2]??'run' ;
if ( $command == 'run' ) {
	if ( isset($argv[1]) ) $wd4wp->update_wiki ( $argv[1] ) ;
	else $wd4wp->update_next_wiki() ;
} else if ( $command == 'mark' ) {
	$wd4wp->mark_pages_using_candidate_images_as_done ( $argv[1] ) ;
} else if ( $command == 'test' ) {
	$wikis = $wd4wp->get_all_wikis() ;
	foreach ( $wikis AS $wiki => $wiki_id ) {
		print "{$wiki}\n" ;
		$wd4wp->check_if_image_links_still_exist ( $wiki ) ;
	}
} else {
	print "Unknown command '{$command}'\n" ;
	exit(1);
}

?>
