<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); # E_ALL|
ini_set('display_errors', 'On');
@set_time_limit ( 15*60 ) ; # Time limit 45min

include_once ( "php/common.php" ) ;
include_once ( "php/wikidata.php" ) ;

function get_flickr_key () { return trim ( file_get_contents ( '../flickr_key.txt' ) ) ; }

function flickr2item ( $p , $ip ) {

	$lat1 = $p->latitude ;
	$lon1 = $p->longitude ;
	
	$lat2 = $ip->lat ;
	$lon2 = $ip->lon ;

	$theta = $lon1 - $lon2;
	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;

	return ($miles * 1.609344) * 1000 ; // meters
}


$lat = get_request ( 'lat' , '' ) ;
$lon = get_request ( 'lon' , '' ) ;
$radius = get_request ( 'radius' , '' ) ;
$nearby = get_request ( 'nearby' , '50' ) ;
$max_results = get_request ( 'max_results' , '100' ) * 1 ;

print get_common_header ( '' , 'Check Flickr geo' ) ;
print "<div class='lead'>This tool finds Wikidata items without image around a location, then finds free images on Flickr taken nearby.</div>
<form method='get' action='?' class='form form-inline' >
<div>Coordinates: 
<input name='lat' value='$lat' type='text' placeholder='Latitude' /> 
<input name='lon' value='$lon' type='text' placeholder='Longitude' />
</div>
<div>Radius:
<input name='radius' value='$radius' type='text' placeholder='Around center' /> [km] around center; 
<input name='nearby' value='$nearby' type='text' placeholder='Nearby' /> [m] around each item
</div><div>
<input type='text' value='$max_results' name='max_results' /> Maximum numer of total candidate images
</div><div>
<input type='submit' name='doit' value='Do it!' class='btn btn-primary' />
</div>
</form>" ;

if ( !isset($_REQUEST['doit']) ) {
	print get_common_footer() ;
	exit ( 0 ) ;
}


$url = "$wdq_internal_url?q=around[625,$lat,$lon,$radius]%20and%20noclaim[18]&props=625" ;
$j = json_decode ( file_get_contents ( $url ) ) ;
$p625 = 625 ;
$wd = array() ;
foreach ( $j->props->$p625 AS $x ) {
	if ( $x[1] != 'coord' ) continue ;
	$y = explode ( '|' , $x[2] ) ;
	$wd['Q'.$x[0]] = (object) array ( 'lat' => $y[0] , 'lon' => $y[1] , 'flickr' => array() ) ;
}
unset ( $j ) ;


// Check https://www.flickr.com/services/api/explore/flickr.photos.search
$flickr_key = get_flickr_key() ;

$page = 0 ;
$found = 0 ;
$max_results = 100 ;
while ( 1 ) {
	$url = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=$flickr_key&license=4,5,7,8,9,10&sort=interestingness-desc&page=$page&has_geo=1&lat=$lat&lon=$lon&radius=$radius&radius_units=km&extras=geo%2Curl_sq&per_page=500&format=json&nojsoncallback=1" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	$page = $j->photos->page ;
	$pages = $j->photos->pages ;
	if ( count ( $j->photos->photo ) == 0 ) break ;

	foreach ( $j->photos->photo AS $p ) {
		foreach ( $wd AS $item => $itempos ) {
			$dist = flickr2item ( $p , $itempos ) ;
			if ( $dist > $nearby ) continue ;
			$p2 = clone $p ;
			$p2->distance = $dist ;
			$wd[$item]->flickr[] = $p2 ;
			$found++ ;
		}
	}
	unset ( $j ) ;
	print "<div>Loaded Flickr result page #$page : $found total images found, stopping after $max_results</div>" ; myflush();
	if ( $page >= $pages ) break ;
	$page++ ;
	if ( $found >= $max_results ) break ;
}

$qs = array() ;
foreach ( $wd AS $q => $i ) {
	if ( count ( $i->flickr ) == 0 ) continue ;
	$qs[] = $q ;
}

$wil = new WikidataItemList ;
$wil->loadItems ( $qs ) ;


print "<table class='table table-striped table-condensed'>" ;
print "<tbody>" ;
foreach ( $wd AS $q => $i ) {
	if ( count ( $i->flickr ) == 0 ) continue ;
	print "<tr>" ;
	print "<td valign='top' style='width:33%'><a href='//www.wikidata.org/wiki/$q' target='_blank'>" . $wil->items[$q]->getLabel() . "</a><br/><small>$q</small></td>" ;
	print "<td>" ;
	foreach ( $i->flickr AS $f ) {
		$url = "https://www.flickr.com/photos/{$f->owner}/{$f->id}" ;
		print "<div style='display:table;margin-bottom:2px;margin-top:2px'>" ;
		print "<div style='display:table-cell;vertical-align:top'>" ;
		print "<a href='$url' target='_blank'><img src='{$f->url_sq}' border=0 /></a>" ;
		print "</div>" ;
		print "<div style='display:table-cell;padding-left:5px;vertical-align:top'>" ;
		print "<div><b>{$f->title}</b></div>" ;
		if ( isset($f->_content) ) print "<div style='font-size:9pt'>" . $f->_content . "</div>" ;
		print "<div>Distance : " . floor($f->distance) . "m</div>" ;
		print "<div><a href='http://tools.wmflabs.org/flickr2commons/?photoid={$f->id}' target='_blank'>Upload to Commons</a></div>";
		print "</div>" ;
		print "</div>" ;
	}
	print "</td>" ;
	print "</tr>" ;
}
print "</tbody></table>" ;

//print "<pre>" ; print_r ( $wd ) ; print "</pre>" ;

print get_common_footer() ;

?>