// ENFORCE HTTPS
if (location.protocol != 'https:') location.href = 'https:' + window.location.href.substring(window.location.protocol.length);

var widar ;
var wd = new WikiData ;
var media_props = { 'photo':[] , 'map':[] , 'diagram':[] , 'audio':[] , 'video':[] , 'other':[] } ;
var media_prop2label = {} ;
var metadata = {} ;


var FileCandidatesMixin = {
	data : function () { return { file_candidates_api:'https://fist.toolforge.org/file_candidates/api.php' } } ,
	methods : {
		setFileCandidateStatus : function ( candidate_id , status , prop , callback ) {
			var me = this ;
			$.get(me.file_candidates_api,{
				action:'set_candidate_status',
				id:candidate_id,
				prop:prop,
				user:widar.getUserName(),
				status:status
			},function ( d ) {
				if ( typeof callback != 'undefined' ) callback ( d ) ;
			} ) . fail ( function ( jqXHR, textStatus, errorThrown ) {
				if ( typeof callback != 'undefined' ) callback ( textStatus ) ;
			} ) ;
		} ,
		addCommas : function (nStr) {
			nStr += '';
			x = nStr.split('.');
			x1 = x[0];
			x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + ',' + '$2');
			}
			return x1 + x2;
		} ,
		logEvent : function ( method ) {
			$.getJSON ( 'https://tools.wmflabs.org/magnustools/logger.php?tool=file_candidates&method='+method+'&callback=?' , function(j){} ) ;
		}
	}
} ;

class pageParams {

	constructor ( main_candidates_page ) {
		this.base_path = 'candidates' ;
		this.main_tt = 'main_candidates_lead' ;
		this.main_candidates_page = main_candidates_page ;
		this.csv_params = {
			sources:'source',
			groups:'group',
			types:'type'
		} ;
		this.string_params = {
			size:'size',
			sparql:'sparql',
			commonscat:'commonscat'
		} ;
		this.default_values = {
			size:10
		} ;
		this.init() ;
	}

	parseParameterList  ( param ) {
		var list = [] ;
		$.each ( (param||'').split(/,/) , function ( dummy , part ) {
			part = $.trim(part.toUpperCase()) ;
			if ( part == '' || part == 'ANY' ) return ;
			list.push ( part ) ;
		} )
		return list ;
	}
	
	init () {
		var me = this ;
		me.data = {} ;
		$.each ( me.default_values , function ( k , v ) {
			me.data[k] = v ;
		} ) ;
		$.each ( me.csv_params , function ( k , v ) {
			if ( typeof me.data[k] == 'undefined' ) me.data[k] = [] ;
			if ( typeof me.main_candidates_page[v] == 'undefined' ) return ;
			me.data[k] = me.parseParameterList(me.main_candidates_page[v]) ;
		} ) ;
		$.each ( me.string_params , function ( k , v ) {
			if ( typeof me.data[k] == 'undefined' ) me.data[k] = '' ;
			if ( typeof me.main_candidates_page[v] == 'undefined' ) return ;
			me.data[k] = me.main_candidates_page[v] ;
		} ) ;
	}

	getHashURL () {
		var me = this ;
		var ret = [] ;
		$.each ( me.csv_params , function ( param_name_in_class , param_name_in_url ) {
			var value = (me.data[param_name_in_class]||[]).join(',') ;
			if(value=='' || value==(me.default_values[param_name_in_class]||'') ) return ;
			ret.push ( param_name_in_url+'='+encodeURIComponent(value) )
		} ) ;
		$.each ( me.string_params , function ( param_name_in_class , param_name_in_url ) {
			var value = me.data[param_name_in_class]||'' ;
			if(value=='' || value==(me.default_values[param_name_in_class]||'') ) return ;
			ret.push ( param_name_in_url+'='+encodeURIComponent(value) )
		} ) ;
		ret = ret.join('&') ;
		if ( ret != '' ) ret = '?' + ret ;
		return '/' + me.base_path + '/' + ret ;
	}
} ;


var autodesc_cache = {} ;

Vue.component ( 'autodesc' , {
	props : [ 'q' , 'description_mode' ] ,
	data : function () { return { loaded:false , manual:'' , auto:'' , loading:false } } ,
	created : function () {
		this.updateDescription() ;
	} ,
	methods : {
		getLanguage : function () {
			return tt.language ;
		} ,
		sanitizeQ : function ( q ) {
			return (''+q).replace(/\D/g,'') ;
		} ,
		updateLabelDescription : function ( q ) {
			var me = this ;
			var sq = me.sanitizeQ(me.q) ;
			var language = me.getLanguage() ;
			var d = autodesc_cache[language][sq] ;
			$('.wikidata_item.not_loaded[q="'+sq+'"]').removeClass('notLoaded').removeClass('not_loaded').text(d.label) ;
			me.manual = d.manual_description ;
			me.auto = d.result ;
			me.auto = me.auto.replace ( /<.+?>/g , '' ) ;
			me.loading = false ;
			me.loaded = true ;
		} ,
		updateDescription : function () {
			var me = this ;
			var language = me.getLanguage() ;
			if ( typeof autodesc_cache[language] != 'undefined' && typeof autodesc_cache[language][me.sanitizeQ(me.q)] != 'undefined' ) {
				me.updateLabelDescription ( me.q ) ;
				return ;
			}
			if ( me.loading ) return ;
			me.loading = true ;
			me.loaded = false ;
			me.manual = '' ;
			me.auto = '' ;
			var params = {
				q:me.q,
				lang:language,
				mode:me.description_mode=='short'?'short':'long',
				links:'text',
				format:'json'
			} ;
			$.getJSON ( 'https://autodesc.toolforge.org/?callback=?' , params , function ( d ) {
				if ( typeof autodesc_cache[language] == 'undefined' ) autodesc_cache[language] = {} ;
				autodesc_cache[language][me.sanitizeQ(me.q)] = d ;
				me.updateLabelDescription ( me.q ) ;
			} , 'json' ) .fail(function() {
				me.manual = '' ;
				me.auto = "Automatic description not available" ;
				me.loaded = true ;
			} ) .always ( function () { me.loading = false } ) ;
		}
	} ,
	created : function () { this.updateDescription() } ,
	watch : {
		'q' : function () { this.updateDescription() } ,
	} ,
	template : '#autodesc-template'
} ) ;

Vue.component ( 'main-candidates-parameters' , {
	props : [ 'p' , 'hashgroups' ] ,
	data : function () { return { is_expanded:false , cb_state:{} , did_window_scroll:false } } ,
	created : function () {
		var me = this ;
		me.init() ;
		$(window).scroll ( function ( event ) { // To turn off: .off("scroll")
			me.did_window_scroll = true ;
		} ) ;
		setInterval(function() {
			if ( !me.did_window_scroll ) return ;
			me.did_window_scroll = false ;
			var page_top = window.pageYOffset ;
			if ( page_top < $('#main-candidates-parameters').offset().top+$('#main-candidates-parameters').height() ) {
				$('#side_button_bar').removeClass('side_button_bar_fixed') ;
			} else {
				$('#side_button_bar').addClass('side_button_bar_fixed') ;
			}
		} , 50 ) ;
	} ,
	updated : function () {
		var me = this ;
		tt.updateInterface(me.$el) ; 
	} ,
	methods : {
		init : function () {
			var me = this ;
			me.cb_state = {} ;
			$.each ( me.p.csv_params , function ( param_object , param_url ) {
				if ( typeof metadata.field_values[param_url] == 'undefined' ) return ;
				me.cb_state[param_url] = {}
				$.each ( metadata.field_values[param_url].values , function ( k , pv ) {
					me.cb_state[param_url][pv] = me.p.data[param_object].indexOf(pv) !== -1 ;
				} )
			} )
		} ,
		isValidOption : function ( param , name ) {
			var me = this ;
			if ( param == 'group' ) {
				if ( me.hashgroups && name.substr(0,1)=="#" ) return true ;
				if ( !me.hashgroups && name.substr(0,1)!="#" ) return true ;
				return false ;
			}
			return true ;
		} ,
		getParamLabel : function ( param , name ) {
			var me = this ;
			if ( param == 'group' ) {
				if ( me.hashgroups ) name = name.substr(1) ;
			}
			return name ;
		} ,
		reloadHandler : function () {
			var me = this ;
			router.push(me.p.getHashURL()) ;
			me.$emit ( 'update_route' , me.p ) ;
		} ,
		expandHandler : function () {
			window.scrollTo(0,0);
			this.is_expanded = !this.is_expanded ;
		} ,
		updateParameterCheckbox : function ( param_object , value ) {
			var me = this ;
			var position_in_array = me.p.data[param_object].indexOf(value) ;
			if ( position_in_array !== -1 ) me.p.data[param_object].splice ( position_in_array , 1 ) ;
			else me.p.data[param_object].push ( value ) ;
		}
	} ,
	template : '#main-candidates-parameters-template'
} ) ;


var MainPage = Vue.extend ( {
	mounted : function () {
		var me = this ;
		tt.updateInterface(me.$el) ; 
	} ,
	template : '#main-page-template'
} ) ;


var ProposedItemsPage = Vue.extend ( {
	mixins : [ FileCandidatesMixin ] ,
	props : [ "group" , "size" ] ,
	data : function () { return { p:{} , last_params:'' , loading:false , data:{} , error:'' } } ,
	created : function () {
		this.p = new pageParams ( this ) ;
		this.p.base_path = 'proposed_items' ;
		this.p.main_tt = 'proposed_items_lead' ;
	} ,
	updated : function () {
		var me = this ;
		tt.updateInterface(me.$el) ; 
	} ,
	mounted : function () {
		this.init() ;
	} ,
	methods : {
		init : function ( force = false , np = {} ) {
			var me = this ;
			if ( force ) me.p = np ;
			else me.p.init () ;
			me.loadCandidates () ;
		} ,
		loadCandidates : function () {
			var me = this ;
			if ( me.loading ) return ;
			me.loading = true ;
			me.error = '' ;

			var params = {
				action:'get_proposed_items',
				size:(me.p.data.size||10)*1,
				group:me.p.data.groups.join(','),
				source:me.p.data.sources.join(','),
				sparql:me.p.data.sparql
			} ;
			if ( typeof me.items != 'undefined' ) params.items = me.items ;

			$.post ( me.file_candidates_api , params , function ( d ) {
				if ( d.status !== 'OK' ) {
					me.error = d.status ;
					me.loading = false ;
					return ;
				}
				me.data = d.data ;

				me.loading = false ;
			} , 'json' ) . fail ( function ( jqXHR, textStatus, errorThrown ) {
				me.error = 'ERROR: ' + textStatus ;
				me.loading = false ;
			} ) ;
		}
	} ,
	template : '#proposed-items-template'
} ) ;


var StatsPage = Vue.extend ( {
	mixins : [ FileCandidatesMixin ] ,
	props : [] ,
	data : function () { return { data:{} , loaded:false , statuses:{} , sources:{} , totals:{} , all_total:{} , total_overall:{} , group_names:[] } } ,
	created : function () {
		this.init();
	} ,
	updated : function () {
		var me = this ;
		tt.updateInterface(me.$el) ; 
	} ,
	mounted : function () {
	} ,
	methods : {
		init : function () {
			var me = this ;
			$.get ( FileCandidatesMixin.data().file_candidates_api , {
				action:'stats'
			} , function ( d ) {
				me.total_overall['total'] = 0 ;
				$.each ( d.data , function ( group , v1 ) {
					me.group_names.push(group);
					me.totals[group] = {};
					me.all_total[group] = 0;
					$.each ( v1 , function ( status , v2 ) {
						if ( typeof me.total_overall[status] == 'undefined' ) me.total_overall[status] = 0 ;
						me.totals[group][status] = 0 ;
						me.statuses[status] = status ;
						$.each ( v2 , function ( source , count ) {
							me.sources[source] = source ;
							me.totals[group][status] += count*1 ;
							me.all_total[group] += count*1 ;
							me.total_overall[status] += count*1 ;
							me.total_overall['total'] += count*1 ;
						}) ;
					}) ;
				}) ;
				me.data = d.data
				me.group_names.sort();
				me.loaded = true ;
			} , 'json' ) ;
		} ,
	} ,
	template : '#stats-template'
} ) ;

var MainCandidatesPage = Vue.extend ( {
	mixins : [ FileCandidatesMixin ] ,
	props : [ "source" , "group" , "size" , "items" , "sparql" , "commonscat" ] ,
	data : function () { return { p:{} , last_params:'' , loading:false , data:{} , error:'' , warning:'' } } ,
	created : function () {
		this.p = new pageParams ( this ) ;
	} ,
	updated : function () {
		var me = this ;
		tt.updateInterface(me.$el) ; 
	} ,
	mounted : function () {
		this.init() ;
	} ,
	methods : {
		init : function ( force = false , np = {} ) {
			var me = this ;
			if ( force ) me.p = np ;
			else me.p.init () ;
			me.loadCandidates () ;
		} ,
		loadCandidates : function () {
			var me = this ;
			if ( me.loading ) return ;
			me.loading = true ;
			me.error = '' ;
			me.warning = '' ;

			var params = {
				action:'get_candidates',
				size:(me.p.data.size||10)*1,
				group:me.p.data.groups.join(','),
				source:me.p.data.sources.join(','),
				commonscat:me.p.data.commonscat.replace(/ /g,'_'),
				sparql:me.p.data.sparql
			} ;
			if ( typeof me.items != 'undefined' ) params.items = me.items ;

			$.post ( me.file_candidates_api , params , function ( d ) {
				if ( d.status !== 'OK' ) {
					me.error = d.status ;
					me.loading = false ;
					return ;
				}
				me.data = d.data ;
				if ( typeof d.warning != 'undefined' && d.warning != '' ) me.warning = d.warning ;

				var items = [] ;
				$.each ( me.data , function ( q , v ) { items.push(q) } ) ;
				wd.getItemBatch ( items , function () {
					me.loading = false ;
				} ) ;

			} , 'json' ) . fail ( function ( jqXHR, textStatus, errorThrown ) {
				me.error = 'ERROR: ' + textStatus ;
				me.loading = false ;
			} ) ;
		}
	} ,
	template : '#main-candidates-template'
} ) ;

Vue.component ( 'wd-item-box' , {
	props : [ 'q' , 'use_item_link' ] ,
	data : function () { return { i:undefined , img:'' , img_url:'' } } ,
	created : function () {
		var me = this ;
		if ( typeof me.q == 'undefined' ) return ;
		wd.getItemBatch ( [ me.q ] , function () {
			me.i = wd.getItem ( me.q ) ;
			if ( typeof me.i == 'undefined' ) return ;
			me.img = me.i.getFirstStringForProperty ( 'P18' ) ;
			if ( me.img == '' ) return ;
			me.img_url = 'https://commons.wikimedia.org/wiki/Special:Redirect/file/' + encodeURIComponent(me.img) + '?width=100px' ;
		} ) ;
	} ,
	methods : {
		useItem : function ( q ) {
			var me = this ;
			me.$emit('useq',{q:q}) ;
		}
	} ,
	template : '#wd-item-box-template'
} ) ;


Vue.component ( 'new-item' , {
	mixins : [ FileCandidatesMixin ] ,
	props : [ 'q' ] ,
	data : function () { return { query:'' , results:[] , searching:0 , search_cache:{'':[]} } } ,
	updated : function () {
		var me = this ;
		tt.updateInterface(me.$el) ; 
	} ,
	methods : {
		handleChange : function () {
			var me = this ;
			if ( typeof me.search_cache[me.query] != 'undefined' ) {
				me.results = me.search_cache[me.query] ;
				return ;
			}
			me.searching++ ;
			me.search_cache[me.query] = [] ; // Placeholder, to avoid sending this out several times
			$.getJSON ( 'https://www.wikidata.org/w/api.php?callback=?' , {
				action:'wbsearchentities',
				search:me.query ,
				limit:10,
				type:'item',
				format:'json',
				language:tt.language
			} , function ( d ) {
				if ( d.searchinfo.search != me.query ) return ; // Search has been updated, don't display results

				var to_load = [] ;
				$.each ( d.search , function ( k , v ) {
					to_load.push ( v.id ) ;
				} ) ;
				me.searching++ ;
				wd.getItemBatch ( to_load , function () {
					me.searching-- ;
					me.results = d.search ;
					me.search_cache[me.query] = me.results ;
				} ) ;

			} ) . always ( function () {
				me.searching-- ;
			} );
		} ,
		createNewItem : function () {
			var me = this ;
			console.log ( 'creating new item' ) ;
			var data = {} ;
			if ( me.query != '' ) {
				data.labels = {} ;
				data.labels[tt.language] = { language:tt.language , value:me.query } ;
			}
			var json = {
				action:'wbeditentity',
				new:'item',
				data:JSON.stringify ( data )
			} ;
			var params = { botmode:1 , action:'generic' , summary:'via #file_candidates' , json:JSON.stringify(json) } ; // create_blank_item
			widar.run ( params , function ( d ) {
				if ( d.error != 'OK' ) {
					alert ( d.error ) ;
					return ;
				}
				var q = 'Q' + d.res.entity.id.replace ( /\D/g , '' ) ;
				me.useQ ( {q:q} ) ;
				me.logEvent ( 'create new item' ) ;
			} ) ;				
		} ,
		useQ : function ( event ) {
			var me = this ;
			me.$emit('useq',event) ;
		}
	} ,
	template : '#new-item-template'
} ) ;

Vue.component ( 'file-candidate' , {
	mixins : [ FileCandidatesMixin ] ,
	props : [ 'q' , 'file' ] ,
	data : function () { return { page_url:'' , thumbnail_url:'' , file_description_cell_class:'file_description_cell_generic' , ii:{} , hide:false , 
		flickr : { show_components:false , new_filename_on_commons:'' , property:'P18' , error:'' , status:''  } , description:'' , is_busy:false
	} } ,
	created : function () {
		var me = this ;
		if ( me.file.source == 'COMMONS' ) {
			me.ii = me.file.json.imageinfo[0] ;
			me.page_url = me.ii.descriptionurl ;
			me.thumbnail_url = me.ii.thumburl ;
			me.file_description_cell_class = 'file_description_cell_commons' ;
		} else if ( me.file.source == 'FLICKR' ) {
			me.ii = me.file.json['@attributes'] ;
			if ( typeof me.file.json != 'undefined' && typeof me.file.json.description != 'undefined' ) {
				me.description = (''+me.file.json.description).replace(/<.+?>/g,' ') ;
				me.description = $('<textarea />').html(me.description).text() ;
			}
			me.page_url = 'https://flickr.com/photos/' + me.ii.owner + "/" + me.ii.id ;
			me.thumbnail_url = me.ii.url_t ;
			me.file_description_cell_class = 'file_description_cell_flickr' ;
		} else {
			// TODO
		}
	} ,
	updated : function () { tt.updateInterface(this.$el) } ,
	mounted : function () { tt.updateInterface(this.$el) } ,
	methods : {
		skipCandidate : function () {
			var me = this ;
			me.setFileCandidateStatus ( me.file.id , 'SKIPPED' , 0 , function () {
				me.hide = true ;
			} ) ;
		} ,
		showFlickrComponents : function () {
			var me = this ;
			me.flickr.error = '' ;
			me.flickr.status = 'Checking authentication...' ;
			me.is_busy = true ;
			flickr2commons.checkAuth ( function ( error ) {
				if ( error != 'OK' ) {
					me.flickr.error = error.replace(/, then reload this page.*$/,', then try again.') ;
					me.flickr.status = '' ;
					me.is_busy = false ;
					return ;
				}
				me.flickr.status = 'Checking if file exists on Commons...' ;
				var photo_object = { photo_id:me.ii.id , error:'' } ;
				flickr2commons.getExistingCommonsFile ( photo_object , function ( o ) {
					if ( o.error != '' ) {
						me.flickr.error = o.error ;
						me.flickr.status = '' ;
						me.is_busy = false ;
						return ;
					}
					if ( typeof o.filename_on_commons != 'undefined' ) {
						me.flickr.status = '' ;
						me.flickr.new_filename_on_commons = o.filename_on_commons ;
						me.flickr.show_components = true ;
						me.is_busy = false ;
						return ;
					}

					me.flickr.status = 'Generating new Commons file name...' ;
					flickr2commons.uploadUnderAnyName ( o , function ( o ) {
						if ( o.error != '' ) { me.flickr.error = o.error ; me.flickr.status = '' ; return }
						if ( typeof o.filename_on_commons == 'undefined' ) {
							me.flickr.status = '' ;
							me.flickr.error = o.error ;
							me.is_busy = false ;
							return ;
						}

						me.flickr.status = 'Generating file description...' ;
						flickr2commons.generateInformationTemplate ( o , function ( o ) {
							if ( o.error != '' ) { me.flickr.error = o.error ; me.flickr.status = '' ; return }
							me.flickr.status = 'Uploading file to Commons...' ;
							flickr2commons.uploadFileToCommons ( o , function ( o ) {
								if ( o.error != '' ) { me.flickr.error = o.error ; me.flickr.status = '' ; return }
								me.flickr.status = '' ;
								me.flickr.new_filename_on_commons = o.filename_on_commons ;
								me.flickr.show_components = true ;
								me.is_busy = false ;
								me.logEvent ( 'transferred from flickr to commons' ) ;
							} ) ;
						} ) ;

					}) ;
				}) ;

			} ) ;
		} ,
		setFileForProperty : function () {
			var me = this ;
			//console.log ( me.flickr.new_filename_on_commons , me.flickr.property ) ;
			var fn = $.trim(me.flickr.new_filename_on_commons.replace(/_/g,' ')) ;
			fn = fn.replace ( /^[Ff]ile:/ , '' ) ;
			me.is_busy = true ;
			$.getJSON ( 'https://commons.wikimedia.org/w/api.php?callback=?' , {
				action:'query',
				titles:'File:'+fn,
				prop:'imageinfo',
				format:'json'
			} , function ( d ) {
				if ( typeof d.query.pages['-1'] != 'undefined' ) {
					alert ( "No file with that name on Commons" ) ;
					me.is_busy = false ;
					return ;
				}

				me.flickr.show_components = false ;
				var params = { botmode:1 , action:'set_string' , id:me.q , prop:me.flickr.property , text:fn } ;
				widar.run ( params , function ( d ) {
					me.is_busy = false ;
					if ( d.error != 'OK' ) {
						me.flickr.show_components = true ;
						alert ( d.error ) ;
						return ;
					}
					me.setFileCandidateStatus ( me.file.id , 'DONE' , me.flickr.property ) ;
					me.hide = true ;
					me.logEvent ( 'add to wikidata' ) ;
				} ) ;				
				
			} ) ;
		} ,
		hideCommonsFile : function ( e ) {
			var me = this ;
			if ( me.file.source != 'COMMONS' ) return ;
			if ( me.file.json.title != e.file ) return ;
			me.hide = true ;
		} ,
	} ,
	template : '#file-candidate-template'
} ) ;


Vue.component ( 'set-commons-file-on-wikidata' , {
	mixins : [ FileCandidatesMixin ] ,
	props : [ 'q' , 'file' ] ,
	data : function () { return { hide:false } } ,
	updated : function () { tt.updateInterface(this.$el) } ,
	mounted : function () { tt.updateInterface(this.$el) } ,
	methods : {
		addFileToItem : function ( prop ) {
			var me = this ;
			var title = me.file.json.title.replace(/^File:/,'') ;
			var params = { botmode:1 , action:'set_string' , id:me.q , prop:prop , text:title } ;
			me.hide = true ;
			widar.run ( params , function ( d ) {
				if ( d.error != 'OK' ) {
					alert ( d.error ) ;
					me.hide = false ;
					return ;
				}
				me.setFileCandidateStatus ( me.file.id , 'DONE' , prop ) ;
				me.$emit('hidecommonsfile',{file:me.file.json.title,prop:prop}) ;
				me.logEvent ( 'add to wikidata' ) ;
			} ) ;
		} ,
		checkWidar : function () {
			widar.checkLogin() ;
		}
	} ,
	template : '#set-commons-file-on-wikidata-template'
} ) ;


Vue.component ( 'candidate-row' , {
	props : [ 'q' , 'files' ] ,
	data : function () { return { hide:false } } ,
	template : '#candidate-row-template'
} ) ;


Vue.component ( 'candidate-list' , {
	props : [ 'data' ] ,
	template : '#candidate-list-template'
} ) ;



Vue.component ( 'candidate-item-row' , {
	props : [ 'file' ] ,
	data : function () { return { q:0 , hide:false } } ,
	methods : {
		useQ : function ( e ) {
			var me = this ;
			me.q = e.q ;
		}
	} ,
	template : '#candidate-item-row-template'
} ) ;

Vue.component ( 'candidate-item-list' , {
	props : [ 'data' ] ,
	template : '#candidate-item-list-template'
} ) ;


Vue.component ( 'candidate-item' , {
	mixins : [ FileCandidatesMixin ] ,
	props : [ 'q' ] ,
	data : function () { return { label:'' , desc:'' , taxon_name:'' , address:'' , has_files:{} , i:{} } } ,
	created : function () {
		var me = this ;
		me.i = wd.getItem ( me.q ) ;
		if ( typeof me.i == 'undefined' ) return ;
		me.label = me.i.getLabel ( tt.language ) ;
		me.desc = me.i.getDesc() ;
		me.taxon_name = me.i.getFirstStringForProperty ( 'P225' ) ;
		me.address = me.i.getFirstStringForProperty ( 'P969' ) ;

		var loc = me.i.getClaimItemsForProperty('P131',true) ;
		if ( loc.length > 0 ) {
			wd.getItemBatch ( [loc[0]] , function () {
				var i = wd.getItem(loc[0]) ;
				if ( typeof i == 'undefined' ) return ;
				var loc_label = i.getLabel ( tt.language ) ;
//				if ( /^Q\d+$/.test(loc_label) ) return ;
				if ( me.address != '' ) me.address += '; ' ;
				me.address += loc_label ;
			} ) ;
		}

		me.has_files = {} ;
		me.checkFileProp ( 'P18' ) ;
		$.each ( media_props , function ( group , props ) {
			$.each ( props , function ( dummy , prop ) {
				me.checkFileProp ( prop.p )
			})
		})
	} ,
	methods : {
		skipAllFiles : function () {
			var me = this ;
			var tr = $($(me.$el).parents('tr').get(0)) ;
			$(tr.find('a[tt_title="skip_file_item"]')).each ( function () {
				var a = $(this) ;
				var file_id = a.attr('file_id') ;
				me.setFileCandidateStatus ( file_id , 'SKIPPED' , 0 ) ;
			} ) ;
			me.$emit ( 'hide_item' , {} ) ;
		} ,
		checkFileProp : function ( prop ) {
			var me = this ;
			var files = me.i.getMultimediaFilesForProperty ( prop ) ;
			if ( files.length == 0 ) return ;
			$.each ( files , function ( dummy , file ) {
				if ( typeof me.has_files[prop] == 'undefined' ) me.has_files[prop] = [] ;
				me.has_files[prop].push ( file ) ;
			} )
		}
	} ,
	template : '#candidate-item-template'
} ) ;


Vue.component ( 'userlink' , {
	props : [ 'username' ] ,
	template : '<span>'+
	'<a :href="\'https://www.wikidata.org/wiki/User:\'+encodeURIComponent(username.replace(/ /g,\'_\'))" target="_blank" class="wikidata">{{username}}</a>'+
	'</span>'
} ) ;

Vue.component ( 'wdlink' , {
	props : [ 'q' ] ,
	data : function () { return { qnum:0 , label:'' } } ,
	created : function () { this.init() } ,
	methods : {
		init : function () {
			var me = this ;
			if ( typeof me.q == 'undefined' ) return ;
			me.qnum = (''+me.q).replace(/\D/g,'') * 1 ;
			if ( me.qnum == 0 ) return ;
			me.label = 'Q'+me.qnum ;
			wd.getItemBatch ( ['Q'+me.qnum] , function () {
				var i = wd.getItem('Q'+me.qnum) ;
				if ( typeof i == 'undefined' ) return ;
				me.label = i.getLabel(tt.language) ;
				if ( /^Q\d+$/.test(me.label) ) me.label = i.getLabel() ;
			} ) ;
		}
	} ,
	template : '#wdlink-template'
} ) ;

Vue.component ( 'sdc-link' , {
	mixins : [ FileCandidatesMixin ] ,
	props : [ 'filename' , 'q' ] ,
	data : function () { return { error:'' , depicts_is_set:false } } ,
	methods : {
		add_prominent : function () {
			this.add_sdc ( 'preferred' ) ;
		} ,
		add_default : function () {
			this.add_sdc ( 'normal' ) ;
		} ,
		add_sdc : function ( rank ) {
			let me = this ;
			flickr2commons.get_sdc_id_from_name ( me.filename , function ( file_id ) {
				if ( typeof file_id == 'undefined' ) {
					me.error = "Cannot get file ID" ;
					return ;
				}
				flickr2commons.check_statement ( file_id , 'P180' , me.q , function ( has_statement ) {
					if ( has_statement ) me.depicts_is_set = true ;
					else flickr2commons.set_sdc_prop_q_rank ( file_id , 'P180' , me.q , rank , function ( d ) {
						if ( d.error == 'OK' ) {
							me.depicts_is_set = true ;
							me.logEvent ( 'structured data on commons' ) ;
						} else {
							me.error = d.error ;
						}
					} ) ;
				} )
			} )
		}
	} ,
	template : '#sdc-link-template'
} ) ;

Vue.component ( 'widar' , {
	data : function () { return { is_logged_in:false , userinfo:{} , widar_api:'api.php' , loaded:false , toolname:'file_candidates' } } ,
	created : function () {
		widar = this ;
		this.checkLogin()
	} ,
	updated : function () { tt.updateInterface(this.$el) } ,
	mounted : function () { tt.updateInterface(this.$el) } ,
	methods : {
		checkLogin : function () {
			var me = this ;
			$.get ( me.widar_api , {
				action:'get_rights',
				botmode:1
			} , function ( d ) {
				me.loaded = true ;
				if ( typeof d.result.query != 'undefined' && typeof d.result.query.userinfo != 'undefined' ) {
					me.is_logged_in = true ;
					me.userinfo = d.result.query.userinfo ;
				} else {
				}
			} , 'json' ) ;
		} ,
		run : function ( params , callback ) {
			var me = this ;
			params.tool_hashtag = me.toolname ;
			params.botmode = 1 ;
			$.get ( me.widar_api , params , function ( d ) {
				if ( d.error != 'OK' ) {
					console.log ( params ) ;
					if ( null != d.error.match(/Invalid token/) || null != d.error.match(/happen/) || null != d.error.match(/Problem creating item/) || ( params.action!='create_redirect' && null != d.error.match(/failed/) ) ) {
						console.log ( "ERROR (re-trying)" , params , d ) ;
						setTimeout ( function () { me.run ( params , callback ) } , 500 ) ; // Again
					} else {
						console.log ( "ERROR (aborting)" , params , d ) ;
//						var h = "<li style='color:red'>ERROR (" + params.action + ") : " + d.error + "</li>" ;
//						$('#out ol').append(h) ;
						callback ( d ) ; // Continue anyway
					}
				} else {
					callback ( d ) ;
				}
			} , 'json' ) . fail(function() {
				console.log ( "Again" , params ) ;
				me.run ( params , callback ) ;
			}) ;
		} ,
		getUserName : function () { return this.userinfo.name }
	} ,
	template : '#widar-template'
} ) ;

const routes = [
  { path: '/', component: MainPage },
  { path: '/candidates', component: MainCandidatesPage , props: (route) => (route.query) },
  { path: '/candidates/', component: MainCandidatesPage , props: (route) => (route.query) },
  { path: '/proposed_items', component: ProposedItemsPage , props: (route) => (route.query) },
  { path: '/proposed_items/', component: ProposedItemsPage , props: (route) => (route.query) },
  { path: '/stats', component: StatsPage , props: (route) => (route.query) },
] ;

var router ;
var app ;
var tt ;


$(document).ready ( function () {
	flickr2commons.oauth_uploader_base = 'api.php' ;
	flickr2commons.oauth_uploader_api = 'api.php?botmode=1' ;

	var cnt = 3 ;
	function fin () {
		cnt-- ;
		if ( cnt > 0 ) return ;
		router = new VueRouter({routes}) ;
/*		router.beforeEach(function (transition) {
			console.log ( "TRANSITION",transition) ;
			transition.next();
		} ) ;*/
		app = new Vue ( { router } ) .$mount('#app') ;
	}

	// Load interface translations
	tt = new ToolTranslation ( {
		tool : 'file_candidates' ,
		fallback : 'en' ,
		highlight_missing : true ,
		onLanguageChange : function ( new_lang ) {
		} ,
		onUpdateInterface : function () {
		} ,
		callback : function () {
			tt.addILdropdown ( '#tooltranslate_wrapper' ) ;
			fin() ;
		}
	} ) ;



	// Load metadata
	$.get ( FileCandidatesMixin.data().file_candidates_api , {
		meta:'all',
		action:'get_flickr_key'
	} , function ( d ) {
		metadata = d.meta ;
		flickr2commons.flickr_api_key = d.data ;
		fin() ;
	} , 'json' ) ;

	// Load media properties
	var sparql = 'SELECT ?property ?propertyLabel WHERE { ?property wikibase:propertyType wikibase:CommonsMedia . SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } }' ;
	wd.loadSPARQL ( sparql , function ( json ) {
		var group2hint = {
			'diagram' : /\b(logo|seal|flag|structure|symbol|icon|diagram|plan|bathymetry)\b/ ,
			'map' : /\bmap\b/ ,
			'photo' : /\b(image|banner|view)\b/ ,
			'video' : /\bvideo\b/ ,
			'audio' : /\baudio\b/ ,
		}
		$.each ( json.results.bindings , function ( dummy , b ) {
			var label = b.propertyLabel.value ;
			var p = wd.itemFromBinding ( b.property ) ;
			p = p.replace(/\D/g,'') ;
			media_prop2label['P'+p] = label ;
			if ( p == 18 || p == 368 || p == 369 ) return ; // Special cases
			var to_group = 'other' ;
			$.each ( group2hint , function ( group , hint ) {
				if ( !hint.test(label) ) return ;
				to_group = group ;
				return false ;
			} ) ;
			media_props[to_group].push ( { p:'P'+p.replace(/\D/g,'') , label:label } ) ;
		} ) ;
		$.each ( media_props , function ( group , props ) {
			props.sort ( function ( a , b ) {
				return (a.label.toLowerCase()>b.label.toLowerCase())?1:-1 ;
			} ) ;
		} ) ;
		fin() ;
	} ) ;
	
	$('#navbar_search_form').submit ( function ( ev ) {
		ev.preventDefault();
		var query = $('#navbar_search_form input[type="text"]').get(0).val() ;
		console.log("1",query);
		return false ;
	} ) ;

} ) ;
