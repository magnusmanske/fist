<?PHP

$max_objects = 25000 ;

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); # E_ALL|
ini_set('display_errors', 'On');
@set_time_limit ( 15*60 ) ; # Time limit 45min

#@apache_setenv('no-gzip', 1);
@ini_set('zlib.output_compression', 0);
@ini_set('implicit_flush', 1);

include_once ( "php/common.php") ;
include_once ( "php/wikiquery.php") ;
include_once ( "common_data.php" ) ;
include_once ( "common_images.php" ) ;
include_once ( "/data/project/pagepile/public_html/pagepile.php" ) ;

$mt = microtime(true);

#if ( !isset ( $_REQUEST['lowmem'] ) ) high_mem ( 64 ) ;
ini_set('memory_limit','1G');

ini_set('implicit_flush', 1);

$major_languages = array (
	'en', 'de', 'fr', 'pl', 'ja', 'nl', 'it', 'pt', 'es', 'sv', 
) ;

$evil_templates_commons = array (
	'pd-russia' ,
	'pd-soviet',
	'no source' ,
	'no source since' ,
	'pd-italy',
) ;

$evil_templates[] = 'nocommons' ;

load_hard_ignore_images () ;

$geograph_sites = array () ;
$geograph_sites['uk'] = 'www.geograph.org.uk' ;
$geograph_sites['de'] = 'geo.hlipp.de' ;
$geograph_sites['channel-islands'] = 'channel-islands.geographs.org' ;

#____________________________________________________________________________________________________________

function get_flickr_key () { return trim ( file_get_contents ( '../flickr_key.txt' ) ) ; }

function load_hard_ignore_images () {
	global $hard_ignore_images ;
	$ig = file_get_contents ( 'http://meta.wikimedia.org/w/index.php?title=FIST/Ignored_images&action=raw' ) ;
	$ig = explode ( "\n" , $ig ) ;
	foreach ( $ig AS $i ) {
		if ( '*' != substr ( $i , 0 , 1 ) ) continue ;
		$i = ucfirst ( trim ( substr ( $i , 1 ) ) ) ;
		$i = str_replace ( ' ' , '_' , $i ) ;
		$hard_ignore_images['all'][] = $i ;
	}
}


function print_form () {
  global $language , $project , $data , $datatype ;
  global $sources , $params , $geograph_sites ;
  global $tusc_user , $tusc_password ;
  global $pagepile_enabeled ;
  

#  $used = db_get_usage_counter ( 'fist' ) . '.' ;
  print get_common_header ( "fist.php" , "F<img style='display:inline' src='//upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Fist.svg/16px-Fist.svg.png' />ST &ndash; Free Image Search Tool" , array (
		'style' => '
    .form_table { border:1px solid black;background-color:#CCCCCC }
    .result_header { border-bottom:2px solid grey;background-color:#EEEEEE }
    .main_div { margin-left:5px;margin-right:5px;margin-bottom:15px;margin-top:0px }
    .main_th { border-bottom:2px solid black;background-color:#CCCCCC }
    .ll_unknown { border-bottom:1px solid grey;background-color:#FFFFCC }
    .ll_commons { border-bottom:1px solid grey;background-color:#CCCCFF }
    .ll_good { border-bottom:1px solid grey;background-color:#CCFFCC }
    .ll_bad { border-bottom:1px solid grey;background-color:#FFCCCC }
    .flickr { border-bottom:1px solid grey;background-color:#DDCCDD }
    .gimp { border-bottom:1px solid grey;background-color:#DDDDCC }
    .no_language_links { color:white; background-color:red; border:1px solid black; padding-left:5px; padding-right:5px }
    .black_bottom { border-bottom:1px solid black }
    .black_top { border-top:1px solid black }
    .gray_bottom { border-bottom:1px solid gray }
    input.span1 { width:50px !important; display: inline }
    input.span2 { width:50px !important; display: inline }
    input.span3 { width:250px !important; display: inline }
'
  ) ) ;
  print "<div class='main_div'>" ;
  print "<div>Search for free images to add to Wikipedia articles, or replace non-free images and placeholders.</div>" ;
  
  $is_categories = $datatype == 'categories' ? 'checked' : '' ;
  $is_articles = $datatype == 'articles' ? 'checked' : '' ;
  $is_replaceimages = $datatype == 'replaceimages' ? 'checked' : '' ;
  $is_ricategories = $datatype == 'ricategories' ? 'checked' : '' ;
  $is_random = $datatype == 'random' ? 'checked' : '' ;
  $is_pagepile = $datatype == 'pagepile' ? 'checked' : '' ;
  
  $is_source_freemages = isset ( $sources['freemages'] ) ? 'checked' : '' ;
  $is_source_ipernity = isset ( $sources['ipernity'] ) ? 'checked' : '' ;
  $is_source_flickr = isset ( $sources['flickr'] ) ? 'checked' : '' ;
  $is_source_languagelinks = isset ( $sources['languagelinks'] ) ? 'checked' : '' ;
  $is_source_gimp = isset ( $sources['gimp'] ) ? 'checked' : '' ;
  $is_source_commons = isset ( $sources['commons'] ) ? 'checked' : '' ;
  $is_source_wts = isset ( $sources['wts'] ) ? 'checked' : '' ;
  $is_source_everystockphoto = isset ( $sources['everystockphoto'] ) ? 'checked' : '' ;
  $is_source_wikidata = isset ( $sources['wikidata'] ) ? 'checked' : '' ;
//  $is_source_geograph = isset ( $sources['geograph'] ) ? 'checked' : '' ;
//  $is_source_geograph_de = isset ( $sources['geograph_de'] ) ? 'checked' : '' ;
  $is_source_agenciabrasil = isset ( $sources['agenciabrasil'] ) ? 'checked' : '' ;
  $is_source_picasa = isset ( $sources['picasa'] ) ? 'checked' : '' ;
  
  
  $is_forarticles_all = $params['forarticles'] == 'all' ? 'checked' : '' ;
  $is_forarticles_noimage = $params['forarticles'] == 'noimage' ? 'checked' : '' ;
  $is_forarticles_lessthan = $params['forarticles'] == 'lessthan' ? 'checked' : '' ;
  $has_no_candidate = isset ( $params['skip_no_candidate'] ) ? 'checked' : '' ;
  $free_only = isset ( $params['free_only'] ) ? 'checked' : '' ;
  $commons_only = isset ( $params['commons_only'] ) ? 'checked' : '' ;
  $include_flickr_id = isset ( $params['include_flickr_id'] ) ? 'checked' : '' ;
  $flickr_new_name_from_article = isset ( $params['flickr_new_name_from_article'] ) ? 'checked' : '' ;
  $flickr_other_languages = isset ( $params['flickr_other_languages'] ) ? 'checked' : '' ;
  $esp_skip_flickr = isset ( $params['esp_skip_flickr'] ) ? 'checked' : '' ;
  $show_existing_images = isset ( $params['show_existing_images'] ) ? 'checked' : '' ;
  $commonsense = isset ( $params['commonsense'] ) ? 'checked' : '' ;
  $skip_articles_in_categories = isset ( $params['skip_articles_in_categories'] ) ? 'checked' : '' ;
  $use_article_as_file_name = isset ( $params['use_article_as_file_name'] ) ? 'checked' : '' ;
  $use_article_as_description = isset ( $params['use_article_as_description'] ) ? 'checked' : '' ;
  
  $is_out_html = $params['output_format'] == 'out_html' ? 'checked' : '' ;
  $is_out_xml = $params['output_format'] == 'out_xml' ? 'checked' : '' ;
  $is_out_json = $params['output_format'] == 'out_json' ? 'checked' : '' ;
  $is_out_wiki = $params['output_format'] == 'out_wiki' ? 'checked' : '' ;

  $jpeg = isset ( $params['jpeg'] ) ? 'checked' : '' ;
  $png = isset ( $params['png'] ) ? 'checked' : '' ;
  $gif = isset ( $params['gif'] ) ? 'checked' : '' ;
  $tiff = isset ( $params['tiff'] ) ? 'checked' : '' ;
  $svg = isset ( $params['svg'] ) ? 'checked' : '' ;
  $ogg = isset ( $params['ogg'] ) ? 'checked' : '' ;
  $checked_count_only_jpg = isset($_REQUEST['count_only_jpg']) ? 'checked' : '' ;
  
  $geographs = '' ;
  foreach ( $geograph_sites AS $site => $server ) {
  	$gs = '' ;
  	if ( $site != 'uk' ) $gs = "_$site" ;
  	$us = strlen($site)<5 ? strtoupper ( $site ) : ucfirst($site);
  	$isg = isset ( $sources['geograph'.$gs] ) ? 'checked' : '' ;
	$geographs .= "  <!--GEOGRAPH.$us-->
  <tr><td valign='top' class='gray_bottom'>
  <label class='checkbox'><input type='checkbox' name='sources[geograph$gs]' id='geograph$gs' value='1' $isg/>
 <a target='_blank' href='//$server'>Geograph $us</a> <small>(CC-BY-SA-2.0)</small></label>
  </td><td class='gray_bottom'>
   <label class='checkbox'><input type='text' class='span2' id='geographmax$gs' name='params[geograph_max$gs]' value='" . $params['geograph_max'.$gs] . "' size='2' />
  results per article (max)</label>
  </td></tr>" ;
  }
  
  $nors = 11 + count ( $geograph_sites ) - 1 ; // - Agencia Brasil

  print "<form class='form' method='post' action='?'><table class='form_table' cellpadding='3px' cellspacing='0px'>
<!--
  <tr><th class='black_bottom'>Project</th><td colspan='2'>
  <input type='text' class='span2' name='language' value='$language' size='2' /> .
  <input type='text' class='span2' name='project' value='$project' size='10' /> .org
  </td></tr>
-->

  <!--Scan-->
  <tr><th class='black_bottom' valign='top' rowspan='2'>Scan</th>
  
  <td nowrap><input type='text' class='span1' name='language' value='$language' /> .
  <input type='text' class='span3' name='project' value='$project' /> .org
  </td></tr><tr>
  
  <td class='black_bottom' style='vertical-align:top'>
  <textarea name='data' cols='40' rows='6'>$data</textarea></td>
  <td class='black_bottom' valign='top'>To the left are (one line each):<br/>
   <label class='checkbox'><input type='radio' name='datatype' value='categories' id='categories' $is_categories/> Categories of articles</label>
  (<input type='text' class='span2' name='params[catdepth]' value='{$params['catdepth']}' size='3' /> subcategories deep)<br/>
   <label class='checkbox'><input type='radio' name='datatype' value='articles' id='articles' $is_articles/> Articles</label><br/>
   <label class='checkbox'><input type='radio' name='datatype' value='replaceimages' id='replaceimages' $is_replaceimages/> Images to be replaced</label><br/>
   <label class='checkbox'><input type='radio' name='datatype' value='ricategories' id='ricategories' $is_ricategories/> Categories of images to be replaced</label> [<i>slooow</i>]<br/>
   <label class='checkbox'><input type='radio' name='datatype' value='random' id='random' $is_random/> Random pages</label>
  (<input type='text' class='span2' name='params[random]' value='{$params['random']}' size='3' /> &times;)  [<i>slooow</i>]<br/>
" ;

	if ( $pagepile_enabeled ) print "<label class='checkbox'><input type='radio' name='datatype' value='pagepile' id='pagepile' $is_pagepile/> PagePile ID</label><br/>" ;

print "Start at
  <input type='text' class='span2' name='params[startat]' value='{$params['startat']}' />
  <br/><label><input type='checkbox' name='count_only_jpg' value='1' {$checked_count_only_jpg} /> Count only JPEGs as images</label>
  
  </td></tr>
  
  
  
  <!--Image sources-->
  <tr><th valign='top' rowspan='$nors'>Image<br/>sources</th>
  
  <!--Language links-->
  <td valign='top' class='gray_bottom'>
   <label class='checkbox'><input type='checkbox' name='sources[languagelinks]' id='languagelinks' value='1' $is_source_languagelinks/>
   Other languages & Commons<br/><dd/><small>Follows language links and looks<br/>for images there</small></label>
  </td><td class='gray_bottom'>
   <label class='checkbox'><input type='text' class='span2' id='llmax' name='params[ll_max]' value='{$params['ll_max']}' size='2' />
  results per article (max)</label><br/>
   <label class='checkbox'><input type='checkbox' name='params[free_only]' id='free_only' value='1' $free_only/>
  Show only images with free licenses</label><br/>
   <label class='checkbox'><input type='checkbox' name='params[commons_only]' id='commons_only' value='1' $commons_only/>
  Show images from commons only</label>  
  </td></tr>
  
  <!--Commons-->
  <tr><td valign='top' class='gray_bottom'>
   <label class='checkbox'><input type='checkbox' name='sources[commons]' id='commons' value='1' $is_source_commons/>
  <a target='_blank' href='//commons.wikimedia.org'>Wikimedia Commons</a> search<br/>
  <dd/><small>Results directly from the internal<br/>Commons search</small></label>
  </td><td valign='top' class='gray_bottom'>
   <label class='checkbox'><input type='text' class='span2' id='commonsmax' name='params[commons_max]' value='{$params['commons_max']}' size='2' />
   results per article (max)</label><br/>
  </td></tr>

  <!--Wikidata-->
  <tr><td valign='top' class='gray_bottom'>
   <label class='checkbox'><input type='checkbox' name='sources[wikidata]' id='commons' value='1' $is_source_wikidata/>
  <a target='_blank' href='//commons.wikimedia.org'>Wikidata</a><br/>
  <dd/><small>Images in items corresponding to articles</small></label>
  </td><td valign='top' class='gray_bottom'>
<!--   <label class='checkbox'><input type='text' class='span2' id='commonsmax' name='params[commons_max]' value='{$params['commons_max']}' size='2' />
   results per article (max)</label><br/>-->
  </td></tr>
  
  <!--Flickr-->
  <tr><td valign='top' class='gray_bottom'>
   <label class='checkbox'><input type='checkbox' name='sources[flickr]' id='flickr' value='1' $is_source_flickr/>
  <a target='_blank' href='//www.flickr.com'>Flickr</a><br/>
  <dd/><small>Results from Flickr for CC-BY and<br/>CC-BY-SA images</small></label>
  </td><td class='gray_bottom'>
   <label class='checkbox'><input type='text' class='span2' id='flickrmax' name='params[flickr_max]' value='{$params['flickr_max']}' size='2' />
   results per article (max)</label><br/>
   <label class='checkbox'><input type='checkbox' name='params[include_flickr_id]' id='include_flickr_id' value='1' $include_flickr_id/>
  Include the Flickr image ID in the filename suggestion</label><br/>
   <label class='checkbox'><input type='checkbox' name='params[flickr_new_name_from_article]' id='flickr_new_name_from_article' value='1' $flickr_new_name_from_article/>
  Base the filename suggestion on the article name</label><br/>
   <label class='checkbox'><input type='checkbox' name='params[flickr_other_languages]' id='flickr_other_languages' value='1' $flickr_other_languages/>
  Try language link titles on flickr if neccessary</label>
  </td></tr>


  <!--Ipernity-->
  <tr><td valign='top' class='gray_bottom'>
   <label class='checkbox'><input type='checkbox' name='sources[ipernity]' id='ipernity' value='1' $is_source_ipernity/>
  <a target='_blank' href='//www.ipernity.com'>Ipernity</a><br/>
  <dd/><small>Results from Ipernity for CC-BY and<br/>CC-BY-SA images</small></label>
  </td><td class='gray_bottom'>
<!--  EXPERIMENTAL, DO NOT USE!-->
<!--  <input type='text' class='span2' id='flickrmax' name='params[flickr_max]' value='{$params['flickr_max']}' size='2' />
  <label for='flickrmax'> results per article (max)</label><br/>
  <input type='checkbox' name='params[include_flickr_id]' id='include_flickr_id' value='1' $include_flickr_id/>
  <label for='include_flickr_id'>Include the Flickr image ID in the filename suggestion</label><br/>
  <input type='checkbox' name='params[flickr_new_name_from_article]' id='flickr_new_name_from_article' value='1' $flickr_new_name_from_article/>
  <label for='flickr_new_name_from_article'>Base the filename suggestion on the article name</label><br/>
  <input type='checkbox' name='params[flickr_other_languages]' id='flickr_other_languages' value='1' $flickr_other_languages/>
  <label for='flickr_other_languages'>Try language link titles on flickr if neccessary</label>-->
  </td></tr>


  <!--Google Picasa-->
  <tr><td valign='top' class='gray_bottom'>
   <label class='checkbox'><input type='checkbox' name='sources[picasa]' id='picasa' value='1' $is_source_picasa/>
  <a target='_blank' href='//picasaweb.google.com/home'>Picasa (Google)</a> search<br/>
  <dd/><small>Searches for \"Commercial use allowed\" CC images<br/>on Picasa Web Albums<br/>CAUTION: Might contain non-remix ones!</small></label>
  </td><td valign='top' class='gray_bottom'>
   <label class='checkbox'><input type='text' class='span2' id='picasamax' name='params[picasa_max]' value='{$params['picasa_max']}' size='2' />
   results per article (max)</label>
  </td></tr>
  
  <!--WikiTravel Shared-->
  <tr><td valign='top' class='gray_bottom'>
   <label class='checkbox'><input type='checkbox' name='sources[wts]' id='wts' value='1' $is_source_wts/>
  <a target='_blank' href='//wikitravel.org/shared'>WikiTravel Shared</a> search<br/>
  <dd/><small>Results directly from the internal<br/>WikiTravel search</small></label>
  </td><td valign='top' class='gray_bottom'>
   <label class='checkbox'><input type='text' class='span2' id='wtsmax' name='params[wts_max]' value='{$params['wts_max']}' size='2' />
  results per article (max)</label><br/>
  <i>Note : No thumbnails available; click on the icons</i>
<!--  <input type='checkbox' id='commonsense' name='params[commonsense]' $commonsense size='2' />
  <label for='commonsense'> Use CommonSense to suggest categories and galleries</label><br/>-->
  </td></tr>
  
  <!--GIMP-SAVY-->
  <tr><td valign='top' class='gray_bottom'>
   <label class='checkbox'><input type='checkbox' name='sources[gimp]' id='gimp' value='1' $is_source_gimp/>
  <a target='_blank' href='//gimp-savvy.com/PHOTO-ARCHIVE/index.html'>GIMP-SAVVY</a><br/>
  <dd/><small>Searches a collection of PD images,<br/>mostly NASA and NOAA</small></label>
  </td><td class='gray_bottom'>
   <label class='checkbox'><input type='text' class='span2' id='gimpmax' name='params[gimp_max]' value='{$params['gimp_max']}' size='2' />
  results per article (max)</label><br/>
  <i>Note : GIMP-SAVVY blocks most thumbnail requests.</i>
  </td></tr>
  
  <!--EVERYSTOCKPHOTO-->
  <tr><td valign='top' class='gray_bottom'>
   <label class='checkbox'><input type='checkbox' name='sources[everystockphoto]' id='everystockphoto' value='1' $is_source_everystockphoto/>
  <a target='_blank' href='//everystockphoto.com/index.html'>everystockphoto</a><br/>
  <dd/><small>Stock photos under various licenses</small></label>
  </td><td class='gray_bottom'>
   <label class='checkbox'><input type='text' class='span2' id='espmax' name='params[esp_max]' value='{$params['esp_max']}' size='2' />
  results per article (max)</label><br/>
<!--  <input type='checkbox' name='params[esp_skip_flickr]' id='esp_skip_flickr' value='1' $esp_skip_flickr/>
  <label for='esp_skip_flickr'>Do no show results from flickr</label> -->
  </td></tr>
  
  <!--agenciabrasil-->
<!--  <tr><td valign='top' class='gray_bottom'>
   <label class='checkbox'><input type='checkbox' name='sources[agenciabrasil]' id='agenciabrasil' value='1' $is_source_agenciabrasil/>
  <a target='_blank' href='//www.agenciabrasil.gov.br'>Agencia Brasil</a><br/>
  <dd/><small>Government photos under CC-BY 2.5</small></label>
  </td><td class='gray_bottom'>
   <label class='checkbox'><input type='text' class='span2' id='abmax' name='params[ab_max]' value='{$params['ab_max']}' size='2' />
  results per article (max)</label>
  </td></tr>
-->
  
  $geographs
  
  <!--freemages-->
  <tr><td valign='top'>
   <label class='checkbox'><input type='checkbox' name='sources[freemages]' id='freemages' value='1' $is_source_freemages/>
  <a target='_blank' href='//www.freemages.co.uk'>Freemages</a><br/>
  <dd/><small>All pictures under the Free Art License</small></label>
  </td><td>
   <label class='checkbox'><input type='text' class='span2' id='freemages_max' name='params[freemages_max]' value='{$params['freemages_max']}' size='2' />
  results per article (max)</label>
  </td></tr>
  
  
  <!--List-->
  <tr><th valign='top' class='black_top'>List</th>
  
  <td class='black_top' valign='top' nowrap>
  <label class='checkbox'><input type='radio' name='params[forarticles]' id='forarticles_all' value='all' $is_forarticles_all/> All articles</label><br/>
  <label class='checkbox'><input type='radio' name='params[forarticles]' id='forarticles_noimage' value='noimage' $is_forarticles_noimage/> Articles that have no image</label><br/>
  <label class='checkbox'><input type='radio' name='params[forarticles]' id='forarticles_lessthan' value='lessthan' $is_forarticles_lessthan/> Articles that have less than</label>
  <input type='text' class='span1' name='params[lessthan_images]' value='{$params['lessthan_images']}' /> images<br/>

   <label class='checkbox'><input type='checkbox' name='params[skip_articles_in_categories]' id='skip_articles_in_categories' value='1' $skip_articles_in_categories/>
   Skip articles in </label><a target='_blank' href=\"//meta.wikipedia.org/wiki/FIST/Ignored_categories\">these categories</a><br/>

   <label class='checkbox'><input type='checkbox' name='params[show_existing_images]' id='show_existing_images' value='1' $show_existing_images/> Show images already in the article</label><br/>

   <label class='checkbox'><input type='checkbox' name='params[skip_no_candidate]' id='skip_no_candidate' value='1' $has_no_candidate/> List only articles with potential images</label><br/>

   <label class='checkbox'><input type='checkbox' name='params[use_article_as_file_name]' id='use_article_as_file_name' value='1' $use_article_as_file_name/> Use the article name as default filename</label><br/>

   <label class='checkbox'><input type='checkbox' name='params[use_article_as_description]' id='use_article_as_description' value='1' $use_article_as_description/> Use the article name as description</label><br/>
  
  Default thumbnail size is 
  <input type='text' class='span1' name='params[default_thumbnail_size]' value='{$params['default_thumbnail_size']}' /> px<br/>

  <table border='0'>
  <tr><td valign='top' rowspan='2'>Find</td>
  <td><label class='checkbox'><input type='checkbox' name='params[jpeg]' id='jpeg' value='1' $jpeg/> JPEG</label></td>
  <td><label class='checkbox'><input type='checkbox' name='params[png]' id='png' value='1' $png/> PNG</label></td>
  <td><label class='checkbox'><input type='checkbox' name='params[gif]' id='gif' value='1' $gif/> GIF</label></td>
  </tr><tr>
  <td><label class='checkbox'><input type='checkbox' name='params[svg]' id='svg' value='1' $svg/> SVG</label></td>
  <td><label class='checkbox'><input type='checkbox' name='params[ogg]' id='ogg' value='1' $ogg/> OGG</label></td>
  </tr></table>
  
  <td class='black_top' valign='top'>
  	Results as 
    <label class='checkbox'><input type='radio' name='params[output_format]' value='out_html' id='out_html' $is_out_html/> Web page</label>
    <label class='checkbox'><input type='radio' name='params[output_format]' value='out_xml' id='out_xml' $is_out_xml/> XML</label>
    <label class='checkbox'><input type='radio' name='params[output_format]' value='out_json' id='out_json' $is_out_json/> JSON</label>
    <label class='checkbox'><input type='radio' name='params[output_format]' value='out_wiki' id='out_wiki' $is_out_wiki/> Wiki</label>

	<br/>
	  Potential images have to be at least<dd/>
	  <input type='text' class='span2' name='params[min_width]' value='{$params['min_width']}' size='3' />
	  &times
	  <input type='text' class='span2' name='params[min_height]' value='{$params['min_height']}' size='3' />
	  pixels in size

	<div>Color codes :</div>
  	<div class='ll_commons'>Image from commons (free license)</div>
  	<div class='ll_good'>Free license or public domain</div>
  	<div class='ll_unknown'>Unknown copyright status</div>
  	<div class='ll_bad'>Non-free image (copyvio, fair use, etc.)</div>
  	<div class='flickr'>Image from flickr (CC-BY/-SA license)</div>
  	<div class='gimp'>Image from GIMP-SAVVY (public domain)</div>
	  </td>
  	
  </td>

  </tr>
  
  <!-- TUSC -->
  <tr>
<!--  <th valign='top' style='border-top:2px solid black' rowspan='2'>TUSC<br/>(optional)</th>
  <th align='right' style='border-top:2px solid black'>Commons user name</th>
  <td style='border-top:2px solid black;width:100%' colspan='1'><input name='tusc_user' value='$tusc_user' type='text' class='span2' size='50'/></td>
  </tr>
  <tr>
  <th align='right'>TUSC password</th>
  <td colspan='1'><input name='tusc_password' value='$tusc_password' type='password' size='50'/>-->
  <td colspan=2 /><td>
  <input class='btn btn-primary' type='submit' name='doit' value='Do it!' /></td>
  </tr>

  </table></form>" ;
  myflush() ;
}

#______________________________________________________
# Get the list of articles, according to mode
function get_article_list () {
	global $language , $project , $data , $datatype , $params ;
	global $wq , $articles , $db ;
	$articles = array () ;
	$lines = explode ( "\n" , $data ) ;

	foreach ( $lines AS $k => $v ) {
		remove_namespace_prefix ( $lines[$k] , 6 ) ; # Image
		remove_namespace_prefix ( $lines[$k] , 14 ) ; # Category
	}
	
	# Random
	if ( $datatype == 'random' ) {
		$at = db_get_random_pages ( $language , $project , 0 , $params['random'] ) ;
		foreach ( $at AS $a ) $articles[$a] = $a ;
		return ;
	}
	
	foreach ( $lines AS $l ) {
		$l = ucfirst ( trim ( $l ) ) ;
		if ( $l == '' ) continue ;
		if ( $datatype == 'categories' ) {
			$dummyarray = array() ;
			$at = getPagesInCategory ( $db , $l , $params['catdepth'] , 0 , true ) ;
//			$at = db_get_articles_in_category ( $language , $l , $params['catdepth'] , 0 , $dummyarray , true , $max_objects ) ;
			if ( count ( $at ) == 0 ) { # None found, try talk pages...
				$dummyarray = array() ;
				$at = getPagesInCategory ( $db , $l , $params['catdepth'] , 1 , true ) ;
//				$at = db_get_articles_in_category ( $language , $l , $params['catdepth'] , 1 , $dummyarray , true , $max_objects ) ;
			}
			foreach ( $at AS $a ) $articles[$a] = $a ;
		} else if ( $datatype == 'articles' ) {
			$articles[$l] = $l ;
		} else if ( $datatype == 'replaceimages' ) {
      $l = "Image:$l" ;
			$at = $wq->get_used_image ( $l ) ;
			foreach ( $at AS $a ) $articles[$a] = $a ;
		} else if ( $datatype == 'ricategories' ) {
			$at2 = db_get_images_in_category ( $language , $l , $params['catdepth'] ) ;
			foreach ( $at2 AS $b ) {
//        print "!$b! " ;
        $b = "Image:$b" ;
				$at = $wq->get_used_image ( $b ) ;
				foreach ( $at AS $a ) $articles[$a] = $a ;
			}

		} else if ( $datatype == 'pagepile' ) {
		
			$pp = new PagePile ( trim($l)*1 ) ;
			$language = $pp->getLanguage() ;
			$project = $pp->getProject() ;
			$params['language'] = $language ;
			$params['project'] = $project ;
			$pp->each ( function ( $row , $pp ) {
				global $articles ;
				$full_title = $pp->getFullPageTitle ( $row->page , $row->ns ) ;
				$articles[$full_title] = $full_title ;
			} ) ;
//	print "<pre>" ; print_r ( $articles ) ; print "</pre>" ;

		}
	}
	if ( $params['startat'] != '' ) {
		$p = ucfirst ( trim ( str_replace ( '_' , ' ' , $params['startat'] ) ) ) ;
		$art = array () ;
		foreach ( $articles AS $k => $a ) {
			$a = ucfirst ( trim ( str_replace ( '_' , ' ' , $a ) ) ) ;
			if ( $a >= $p ) $art[$k] = $k ;
		}
		$articles = $art ;
	}
	asort ( $articles ) ;
}

#______________________________________________________
# Determine article "type" (license status as good, bad, commons)
function get_image_type ( $language , $title , $templates ) {
	global $good_templates , $evil_templates , $evil_templates_commons ;
	
	$status = 'unknown' ;
	foreach ( $templates AS $t ) {
		$t = strtolower ( $t ) ;
		$t2 = str_replace ( '_' , ' ' , $t ) ;
		if ( $language == 'commons' ) {
			if ( in_array ( $t , $evil_templates_commons ) ) return 'bad' ;
			if ( in_array ( $t2 , $evil_templates_commons ) ) return 'bad' ;
			continue ;
		}
		if ( in_array ( $t , $evil_templates ) ) return 'bad' ;
		if ( in_array ( $t2 , $evil_templates ) ) return 'bad' ;
		if ( in_array ( $t , $good_templates ) ) $status = 'good' ;
		if ( in_array ( $t2 , $good_templates ) ) $status = 'good' ;
	}
	if ( $language == "commons" ) return "commons" ;
	
	return $status ;
}

#______________________________________________________
# Generate results wrapper table for a single source
function results_section ( $text , $title , $colspan , $append = '' ) {
  if ( $text == '' ) return '' ;
  $ret = '<table width="100%" cellspacing="1px" cellpadding="2px">' ;
  $ret .= '<tr><td colspan="' . 
          $colspan . 
          '" class="result_header"><b>' . 
          $title . 
          '</b>' . 
          $append . 
          '</td></tr>' ;
  $ret .= $text ;
	$ret .= "</table>" ;
  return $ret ;
}

function remove_namespace_prefix ( &$article , $namespace ) {
	global $namespaces ;
	$article = ucfirst ( trim ( $article ) ) ;
	$l = strlen ( $namespaces[$namespace] ) + 1 ;
	if ( substr ( $article , 0 , $l ) == $namespaces[$namespace] . ':' ) {
		$article = substr ( $article , $l ) ;
	}
}


function cleanup_article_name ( &$article ) {
  $article = str_replace ( '_' , ' ' , $article ) ;
  $article = str_replace ( '-' , ' ' , $article ) ;
  $article = str_replace ( '(' , ' ' , $article ) ;
  $article = str_replace ( ')' , ' ' , $article ) ;
}


function get_text_between ( $text , $start , $end ) {
	$x = array_pop ( explode ( $start , $text , 2 ) ) ;
	return array_shift ( explode ( $end , $x , 2 ) ) ;
}

function suggest_image_name ( $fn , $article ) {
	global $params ;
	if ( !isset ( $params['use_article_as_file_name'] ) ) return $fn ;
	$x = explode ( '.' , $fn ) ;
	if ( count ( $x ) < 2 ) return $article ;
	return $article . '.' . array_pop ( $x ) ;
}





#_____________________________________________________________________________________________________________
# PICASA

function find_images_picasa ( $article ) {
	global $params , $api_output ;
	$ret = '' ;
	if ( $api_output ) $ret = array ( 'n' => 'set' , 'a' => array ( 'type' => 'picasa' ) , '*' => array() ) ;
	$url = urlencode ( str_replace ( '_' , ' ' , $article ) ) ;
	$max = $params['picasa_max'] ;
	$url = "http://picasaweb.google.com/data/feed/api/all/?q=$url&imglic=commercial&kind=photo&max-results=$max&thumbsize=144" ; // remix
	$text = file_get_contents ( $url ) ;
	$parts = explode ( '</entry>' , $text ) ;
	array_pop ( $parts ) ;
	foreach ( $parts AS $p ) {
		$p = array_pop ( explode ( '<entry' , $p , 2 ) ) ;
		$p = array_pop ( explode ( '>' , $p , 2 ) ) ;
		
		//$thumbnail = get_text_between ( $p , '<gphoto:thumbnail>' , '</gphoto:thumbnail>' ) ;
		
		$thumbnail = array_pop ( explode ( '<media:thumbnail' , $p ) ) ;
		$thumbnail = get_text_between ( $thumbnail , "url='" , "'" ) ;
		
		
		$title = get_text_between ( $p , "<title type='text' class='span2'>" , "</title>" ) ;
		
		$author = get_text_between ( $p , '<author>' , '</author>' ) ;
		$a_name = get_text_between ( $author , '<name>' , '</name>' ) ;
		$a_url = get_text_between ( $author , '<uri>' , '</uri>' ) ;
		
		$width = get_text_between ( $p , "<gphoto:width>" , "</gphoto:width>" ) ;
		$height = get_text_between ( $p , "<gphoto:height>" , "</gphoto:height>" ) ;
		
		$summary = get_text_between ( $p , "<summary" , "</summary>" ) ;
		$summary = trim ( array_pop ( explode ( '>' , $summary , 2 ) ) ) ;
		
		$i_url = get_text_between ( $p , "<media:content " , "/>" ) ;
		$i_url = get_text_between ( $i_url , "url='" , "'" ) ;

		$desc_url = get_text_between ( $p , "rel='alternate'" , "/>" ) ;
		$desc_url = get_text_between ( $desc_url , " href='" , "'" ) ;
		
		if ( $summary != '' ) $title = $summary ;

		
		
		if ( $api_output ) {
			$n = array (
				'n' => 'image' ,
				'a' => array (
					'name' => apitext ( $title ) ,
					'freeness' => 'Creative Commons' ,
					'thumb_url' => apiurl ( $thumbnail ) ,
					'file_url' => apiurl ( $i_url ) ,
					'desc_url' => apiurl ( $desc_url ) ,
//					'width' => $r->img_wh[0] ,
//					'height' => $r->img_wh[1] ,
					'text' => apitext ( $r->tags ) ,
				)
			) ;
			$ret['*'][] = $n ;
		} else {
			$ret .= "<tr><td class='picasa' valign='top'>" ;
			$ret .= '<a target="_blank" href="' . $desc_url . '">' ;
			$ret .= "<img src=\"" . $thumbnail . "\" width='120px' border=0 />" ;
			$ret .= "</a>" ;
			$ret .= "</td><td class='ll_unknown' width='100%' valign='top'>" ;
//			$ret .= "<a target='_blank' href='$i_url'><b>$title</b></a>" ;
			$ret .= "<b>$title</b>" ;
			$ret .= "<br/>Author : <a target='_blank' href='$a_url'>$a_name</a>" ;
			$ret .= "<br/>Image size : $width&times;$height px" ;
			if ( $summary != $title ) $ret .= "<br/>Summary : $summary" ;

//			$ret .= " (<a target='_blank' href='" . $server . $r->img_url . "'>description page</a>)" ;
//			$ret .= "<br/>Tags : <i>" . $r->tags . "</i>" ;
			$ret .= "</td></tr>" ;
		}
	}
	
	if ( $api_output ) return $ret ;
	return results_section ( $ret , "Google Picasa" , 2 ) ;
}

#_____________________________________________________________________________________________________________
# freemages

function find_images_freemages ( $article ) {
	global $params , $language_links , $language , $project , $major_languages , $api_output ;
	$apiret = array () ;
	$ret = '' ;
	$max = $params['freemages_max'] ;

	$server = "http://www.freemages.co.uk" ;
	$url = "$server/find/view.php?q=" . urlencode ( $article ) ;
	$html = utf8_encode ( file_get_contents ( $url ) ) ;
	$h = explode ( ' class="oneimage"' , $html ) ;
	array_shift ( $h ) ;
	while ( count ( $h ) > $max ) array_pop ( $h ) ;
	foreach ( $h AS $part ) {
		$part = array_shift ( explode ( "</div>" , $part , 2 ) ) ;
//		print "<pre>" . htmlspecialchars ( $part ) . "</pre>" ;
		$part = explode ( '<br/>' , $part , 2 ) ;
		
		$page = array_shift ( explode ( '<img' , $part[0] , 2 ) ) ;
		$page = array_pop ( explode ( ' href="' , $page , 2 ) ) ;
		$page = array_shift ( explode ( '"' , $page , 2 ) ) ;
		$page = $server . $page ;
		
		$thumb = array_pop ( explode ( ' src="' , $part[0] , 2 ) ) ;
		$thumb = array_shift ( explode ( '"' , $thumb , 2 ) ) ;
		$thumb = $server . $thumb ;

		$name = array_pop ( explode ( ' alt="' , $part[0] , 2 ) ) ;
		$name = array_shift ( explode ( '"' , $name , 2 ) ) ;
		
		$author = array_pop ( explode ( '</a> Picture by ' , $part[1] , 2 ) ) ;
		$author = array_shift ( explode ( ', ' , $author , 2 ) ) ;
		
		
		if ( $api_output ) {
			$apiret[] = array (
				'n' => 'image' ,
				'a' => array (
					'name' => apitext ( $name ) ,
//					'id' => apitext ( $v['doc_id'] ) ,
					'location' => 'freemages' ,
					'desc_url' => apiurl ( $page ) ,
					'thumb_url' => apiurl ( $thumb ) ,
	//				'file_url' => apiurl ( $flickr_image_url ) ,
					'text' => "Image by $author" ,
					'freeness' => 'good' 
				)
			) ;
		} else {
			$ret .= "<tr><td class='flickr' valign='top'>" ;
			$ret .= '<a target="_blank" href="' . $page . '">' ;
			$ret .= "<img src=\"" . $thumb . "\" width='120px' border=0 />" ;
			$ret .= "</a>" ;
			$ret .= "</td><td class='ll_good' width='100%' valign='top'>" ;
			$ret .= "<b>$name</b>" ;
			$ret .= "<br/>Author : $author" ;
//			$ret .= "<br/>Image size : $width&times;$height px" ;
//			if ( $summary != '' ) $ret .= "<br/>$summary" ;

			//			$ret .= " (<a target='_blank' href='" . $server . $r->img_url . "'>description page</a>)" ;
			//			$ret .= "<br/>Tags : <i>" . $r->tags . "</i>" ;
			$ret .= "</td></tr>" ;

		}
		
	}

	if ( $api_output ) {
		$apiret = array (
			'n' => 'set' ,
			'*' => $apiret ,
			'a' => array ( 'type' => 'flickr' )
		) ;
		return $apiret ;
	}

	return results_section ( $ret , "Freemages" , 2 ) ;
}

#_____________________________________________________________________________________________________________
# ipernity

function find_images_ipernity ( $article , $ll = false , $count = 0 ) {
	global $params , $language_links , $language , $project , $major_languages , $api_output ;
	global $ipernity_pass ;
	if ( !isset ( $ipernity_pass ) ) $ipernity_pass = get_ipernity_key () ;
	$apiret = array () ;
	$ret = '' ;
	$url = "http://www.ipernity.com/api/doc.search/php/?text=" . urlencode ( $article )  ;
	$url .= "&per_page=5&api_key=" . $ipernity_pass . "&license=1,9,255" ; // CC-BY, CC-BY-SA, Copyleft
	$url .= "&extra=owner,license,original" ;
	$d = unserialize ( file_get_contents ( $url ) ) ;
/*	print "<pre>" ;
	print_r ( $d ) ;
	print "</pre>" ;*/
	
	foreach ( $d['docs']['doc'] AS $v ) {
		$desc_url = "//www.ipernity.com/doc/" . urlencode ( $v['owner']['alias'] ) . "/" . $v['doc_id'] ;
		$thumbnail = $v['thumb']['url'] ;
		$title = $v['title'] ;
		$a_name = $v['owner']['username'] . " (" . $v['owner']['alias'] . ")" ;
		$a_url = "//www.ipernity.com/home/" . $v['owner']['alias'] ;
		$summary = array() ;
		if ( $v['license'] == 1 ) $summary[] = 'License : CC-BY' ;
		if ( $v['license'] == 9 ) $summary[] = 'License : CC-BY-SA' ;
		if ( $v['license'] == 255 ) $summary[] = 'License : PD ("copyleft")' ;
		$summary = implode ( '<br/>' , $summary ) ;

		if ( $api_output ) {
			$apiret[] = array (
				'n' => 'image' ,
				'a' => array (
					'name' => apitext ( $title ) ,
					'id' => apitext ( $v['doc_id'] ) ,
					'location' => 'ipernity' ,
					'desc_url' => apiurl ( $desc_url ) ,
					'thumb_url' => apiurl ( $thumbnail ) ,
	//				'file_url' => apiurl ( $flickr_image_url ) ,
				)
			) ;
		} else {
			$ret .= "<tr><td class='flickr' valign='top'>" ;
			$ret .= '<a target="_blank" href="' . $desc_url . '">' ;
			$ret .= "<img src=\"" . $thumbnail . "\" width='120px' border=0 />" ;
			$ret .= "</a>" ;
			$ret .= "</td><td class='ll_unknown' width='100%' valign='top'>" ;
			$ret .= "<b>$title</b>" ;
			$ret .= "<br/>Author : <a target='_blank' href='$a_url'>$a_name</a>" ;
//			$ret .= "<br/>Image size : $width&times;$height px" ;
			if ( $summary != '' ) $ret .= "<br/>$summary" ;

			//			$ret .= " (<a target='_blank' href='" . $server . $r->img_url . "'>description page</a>)" ;
			//			$ret .= "<br/>Tags : <i>" . $r->tags . "</i>" ;
			$ret .= "</td></tr>" ;

		}
	}


	if ( $api_output ) {
		$apiret = array (
			'n' => 'set' ,
			'*' => $apiret ,
			'a' => array ( 'type' => 'flickr' )
		) ;
		return $apiret ;
	}

	return results_section ( $ret , "Ipernity" , 2 ) ;
//	return $ret ;
}

#_____________________________________________________________________________________________________________
# FLICKR

function find_images_flickr ( $article , $ll = false , $count = 0 ) {
  global $params , $language_links , $language , $project , $major_languages , $api_output ;
  $apiret = array () ;
  $username = "" ;
  $ret = "" ;
  $ret2 = "" ;
  $include_flickr_id = isset ( $params['include_flickr_id'] ) ;
  $flickr_new_name_from_article = isset ( $params['flickr_new_name_from_article'] ) ? 'checked' : '' ;
  
  $article1 = str_replace ( ')' , ' ' , $article ) ;
  $article1 = str_replace ( '(' , ' ' , $article1 ) ;
  $article1 = str_replace ( '-' , ' ' , $article1 ) ;
  $article2 = array_shift ( explode ( '(' , $article , 2 ) ) ;
  
  $found = get_flickr_hits ( $article1 , $params['flickr_max'] ) ;
  if ( count ( $found ) == 0 and $article1 != $article2 ) $found = get_flickr_hits ( $article2 , $params['flickr_max'] ) ;

	if ( count ( $found ) < $params['flickr_max'] and !$ll and isset ( $params['flickr_other_languages'] ) ) {
		if ( !isset ( $language_links ) ) $language_links = db_get_language_links ( $article , $language , $project ) ;
		$check = array () ;
		foreach ( $language_links AS $k => $v ) {
			$v = str_replace ( '-' , ' ' , $v ) ;
			if ( $k == $language ) continue ;
			if ( $v == $article ) continue ;
			if ( !in_array ( $k , $major_languages ) ) continue ;
			if ( in_array ( $v , $check ) ) continue ;
			$check[] = $v ;
		}
		
		$count2 = count ( $found ) ;
		foreach ( $check AS $c ) {
			$subimages = find_images_flickr ( $c , true , $count2 ) ;
			if ( $api_output ) {
				foreach ( $subimages AS $s ) $apiret[] = $subimages ;
			} else $ret2 .= $subimages ;
			$count2 = count ( $found ) . substr_count ( $ret2 , '<tr>' ) ;
			if ( $count2 >= $params['flickr_max'] ) break ;
		}
	}


  if ( !$api_output and count ( $found ) == 0 and $ret2 == '' ) return '' ; # Nothing found on flickr
  if ( $api_output and count ( $found ) == 0 and count ( $ret2 ) ) return $apiret ; # Nothing found on flickr

  $missing_title = '<i>Unnamed image</i>' ;
  foreach ( $found AS $f ) {
    $count++ ;
    if ( $count > $params['flickr_max'] ) break ;
  	if ( $flickr_new_name_from_article or $f->title == $missing_title ) $f->title = $article ;

	if ( $api_output ) {
		$flickr_api_url = "https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=".get_flickr_key()."&photo_id=" . $f->id ;
		$flickr_text = file_get_contents ( $flickr_api_url ) ;
		$flickr_image_url = array_pop ( explode ( " source=\"" , $flickr_text ) ) ;
		$flickr_image_url = trim ( array_shift ( explode ( "\"" , $flickr_image_url , 2 ) ) ) ;
		
		$apiret[] = array (
			'n' => 'image' ,
			'a' => array (
				'name' => apitext ( $f->title ) ,
				'id' => apitext ( $f->id ) ,
				'location' => 'flickr' ,
				'desc_url' => apiurl ( $f->page_url ) ,
				'thumb_url' => apiurl ( $f->s_url ) ,
				'file_url' => apiurl ( $flickr_image_url ) ,
			)
		) ;
	}
  	
    $ret .= '<tr><td class="flickr">' ;
    $ret .= get_flickr_image_table ( $f , $username , $include_flickr_id ) ;
    $ret .= '</td></tr>' ;
  }

  if ( !$ll ) {
    $app = ' (<a target="_blank" href="//flickr.com/search/?ct=0&l=commderiv&q=' . urlencode ( $article1 ) . '">search flickr manually</a>)' ;
    $ret = results_section ( $ret.$ret2 , 'Flickr' , 1 , $app ) ;
  }

	if ( $api_output ) {
		if ( !$ll ) {
			$apiret = array (
				'n' => 'set' ,
				'*' => $apiret ,
				'a' => array ( 'type' => 'flickr' )
			) ;
		}
		return $apiret ;
	}

	return $ret ;
}



#_____________________________________________________________________________________________________________
# LANGUAGE LINKS

function ansi2ascii($string, $repl = '_') {
#  global $ansi2ascii ;
#  foreach ( $ansi2ascii AS $k => $v )
#    $string = str_replace ( $k , $v , $string ) ;
  for ($n = 0; $n < strlen($string); $n++) {
    if (ord($string[$n]) > 127) $string[$n] = $repl;
  }
  return $string ;
}

function db_get_language_links ( $title , $language , $project ) {
	global $db ;
#	$db = openDB ( $language , $project ) ;
	make_db_safe ( $title ) ;
	
	$ret = array () ;
	$sql = "SELECT * FROM langlinks,page WHERE ll_from=page_id AND page_title=\"{$title}\" AND page_namespace=0" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$ret[$o->ll_lang] = $o->ll_title ;
	}
	return $ret ;	
}

function db_local_images ( $language , $images ) {
	$project = 'wikipedia' ; # WTF? FIXME?
	$db = openDB ( $language , $project ) ;
#	$mysql_con = db_get_con_new($language) ;
#	$db = $language . 'wiki_p' ;
	
	$imagelist = '' ;
	foreach ( $images AS $i ) {
		if ( $imagelist != '' ) $imagelist .= ',' ;
#		$i = ucfirst ( str_replace ( ' ' , '_' , $i ) ) ;
#		$i = str_replace ( '"' , '\"' , $i ) ;
		make_db_safe ( $i ) ;
		$imagelist .= '"' . $i . '"' ;
	}
	
	$ret = array () ;
	$sql = "SELECT  img_name FROM image WHERE img_name IN ({$imagelist})" ;
#	$sql = "SELECT ".get_tool_name()." page_title FROM page WHERE page_title IN ({$imagelist}) AND page_namespace=6" ;
#	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
#	if ( mysql_errno() != 0 ) return $ret ;
#	print mysql_error() ;
#	while ( $o = mysql_fetch_object ( $res ) ) {
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$ret[] = $o->img_name ;
	}
	return $ret ;	
}

function db_get_image_list ( $title , $language , $project ) {
	$ret = array () ;
	$db = openDB ( $language , $project ) ;
//	print "<pre>" ; print_r ( gettype($db) ) ; print "</pre>" ;
	if ( !isset($db) or false === $db ) return $ret ;
#	$mysql_con = db_get_con_new($language,$project) ;
#	$db = $language . 'wiki_p' ;
	make_db_safe ( $title ) ;

	$sql = "SELECT  * FROM imagelinks,page WHERE il_from=page_id AND page_title=\"{$title}\" AND page_namespace=0" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
#	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
#	if ( mysql_errno() != 0 ) return $ret ;
#	while ( $o = mysql_fetch_object ( $res ) ) {
		if ( is_image_ignored ( $o->il_to , $language , $project ) ) continue ;
		$ret[] = $o->il_to ;
	}
	return $ret ;
}

function db_get_image_data ( $image , $language , $project ) {
	make_db_safe ( $image ) ;

	$db = openDB ( $language , $project ) ;
#	$mysql_con = db_get_con_new($language) ;
#	$db = $language . 'wiki_p' ;
	
	$sql = "SELECT * FROM image WHERE img_name=\"{$image}\"" ;
#	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
#	if ( mysql_errno() != 0 ) return false ; // Something's broken
	
#	if ( $o = mysql_fetch_object ( $res ) ) {
 	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	if($o = $result->fetch_object()){
   return $o ;
  }
	
	return false ;
}


function db_get_used_templates ( $language , $title , $ns = 0 , $project = 'wikipedia' , $test = false ) {
	$db = openDB ( $language , $project ) ;
#	$mysql_con = db_get_con_new($language,$project) ;
#	$db = get_db_name ( $language , $project ) ;
	make_db_safe ( $title ) ;
	
	$ret = array () ;
	$sql = "SELECT * FROM page,templatelinks,linktarget WHERE page_namespace={$ns} AND page_title=\"{$title}\" AND page_id=tl_from AND tl_target_id=lt_id AND lt_namespace=10" ;
#	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
#	while ( $o = mysql_fetch_object ( $res ) ) {
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$ret[] = $o->lt_title ;
	}
	if ( $test and count ( $ret ) == 0 ) print "<h3>$title $language.$project / $ns</h3>$sql<br/>" . mysql_error() . "<hr/>" ;
	return $ret ;
}


function find_images_languagelinks ( $article ) {
	global $params , $language , $project , $note , $language_links , $thumbsize ;
	global $tusc_user , $tusc_password , $image_ns_name , $api_output ;
	$langlinks = db_get_language_links ( $article , $language , $project ) ;
	$language_links = $langlinks ;
	
	$ret = array () ;
	
	if ( count ( $langlinks ) == 0 ) { # No language links, no images...
		$url = "//www.google.de/search?start=0&ie=utf-8&oe=utf-8&q=" ;
		$url .= urlencode ( "$article site:$project.org -site:$language.$project.org" ) ;
		if ( $note != '' ) $note .= '; ' ;
		$note .= '<span class="no_language_links">No language links</span>' ;
		$note .= ". <a target='_blank' href=\"$url\">Google-search other " . $project . "s</a> for this" ;
		return $ret ;
	}
  
	$used_images = db_get_image_list ( $article , $language , $project ) ; # Already used in article
	
	$images = array () ;
	$commons_usage = array () ;
	$commons_usage_data = array() ;
	$images['commons'] = array () ;
	foreach ( $langlinks AS $lang => $page ) {
    $all_images = db_get_image_list ( $page , $lang , $project ) ;
    if ( count ( $all_images ) == 0 ) continue ;
    $local_images = db_local_images ( $lang , $all_images ) ;
    if ( !isset ( $images[$lang] ) ) $images[$lang] = array () ;
    foreach ( $all_images AS $image ) {
      if ( in_array ( $image , $local_images ) ) {
        if ( !isset ( $params['commons_only'] ) ) $images[$lang][$image] = $image ;
      } else {
      	$images['commons'][$image] = $image ;
      	$thelink = get_wikipedia_url ( $lang , $page , '' , $project ) ;
      	$thelink = "<a target='_blank' href=\"$thelink\">$lang</a>" ;
      	$commons_usage_data[$image]["$lang.$project"] = $page ;
      	if ( isset ( $commons_usage[$image] ) ) $commons_usage[$image] .= ", $thelink" ;
      	else $commons_usage[$image] = $thelink ;
      }
    }
	}

#	print "TESTING:" . implode ( "|" , array_keys ( $commons_usage ) ) . "<br/>" ;

	unset ( $images[$language] ) ; # Paranoia
	$images['_commons'] = $images['commons'] ;
	unset ( $images['commons'] ) ;
	ksort ( $images ) ;
	$count = 0 ;
	foreach ( $images AS $lang => $limages ) {
    if ( $lang == '_commons' ) $lang = 'commons' ;
    $pro = $lang == 'commons' ? 'wikimedia' : $project ;
    foreach ( $limages AS $image ) {
      if ( in_array ( $image , $used_images ) ) continue ; # Already used in that article
      
      # Get image data and check size
      $data = db_get_image_data ( $image , $lang , $pro ) ;
      if ( !isset ( $data->img_width ) ) continue ;
      if ( $data->img_width == '' or $data->img_height == '' ) continue ; # Non-images
      if ( $data->img_width < $params['min_width'] ) continue ;
      if ( $data->img_height < $params['min_height'] ) continue ;

      $count++ ;
      if ( $count > $params['ll_max'] ) break ;
      
      if ( $data->img_width > $data->img_height ) {
        $nw = $thumbsize ;
      } else {
        $nw = floor ( $data->img_width * $thumbsize / $data->img_height ) ;
      }
      
      $thumburl = get_thumbnail_url ( $lang , $image , $nw , $pro ) ;
      $imageurl = get_wikipedia_url ( $lang , "Image:$image" , "" , $pro ) ;
      $image_title = str_replace ( '_' , ' ' , $image ) ;
      
      $templates = db_get_used_templates ( $lang , $image , 6 ) ;

      if ( $language == 'de' && in_array ( 'PD-US-no_notice' , $templates ) ) continue ;
      if ( $language == 'de' && in_array ( 'PD-US-not_renewed' , $templates ) ) continue ;
      
      $type = get_image_type ( $lang , $image , $templates ) ;
      if ( isset ( $params['free_only'] ) and $type != "commons" and $type != 'good' ) continue ;
      $class = 'unknown' ;
      if ( $lang == 'commons' ) $class = 'commons' ;
      if ( $type == 'good' or $type == 'bad' ) $class = $type ;
      $style = "valign='top' class='ll_$class'" ;
      
      if ( $lang != 'commons' ) $template_text = implode ( '</i>", "<i>' , $templates ) ;
      else $template_text = '' ;
      if ( $template_text != '' ) $template_text = "Uses templates \"<i>$template_text</i>\"." ;
      
      $image_noext = explode ( '.' , $image_title ) ;
      array_pop ( $image_noext ) ;
      $image_noext = implode ( '.' , $image_noext ) ;
      if ( $lang == 'commons' ) {
      	if ( !isset ( $commons_usage[$image] ) ) $thumb_text = '' ;
      	else $thumb_text = "Used on " . $commons_usage[$image] . ".<br/>" ;
      	
      	if ( isset ( $params['use_article_as_description'] ) ) $thumb_desc = "$article." ;
      	else $thumb_desc = "$image_noext." ;
        if ( $params['default_thumbnail_size'] != '' ) $thumb_desc = $params['default_thumbnail_size'] . "px|" . $thumb_desc ;
        
      	$thumb_text .= "<tt>[[$image_ns_name:$image_title|thumb|$thumb_desc]]</tt>" ;
      } else $thumb_text = '' ;
      
      if ( $lang != 'commons' and $type == 'good' ) {
      	$newname = suggest_image_name ( $image_title , $article ) ;
//        $newname = ansi2ascii ( $newname ) ;
        $image_enc = $image_title ;
        $upload = "<form method='post' action='./commonshelper.php' target='_blank'>
        <input type='text' class='span2' name='newname' size='40' value='$newname' />
        <input type='hidden' name='image' value='$image_enc' />
        <input type='hidden' name='language' value='$lang' />
        <input type='hidden' name='tusc_user' value='$tusc_user' />
        <input type='hidden' name='tusc_password' value='$tusc_password' />
        <input type='hidden' name='commonsense' value='1' />
        <input type='checkbox' id='reallydirectupload' name='reallydirectupload' value='1' checked />
        <label for='reallydirectupload'>One-click upload</label>
        <input type='submit' name='doit' value='Upload to commons' />
        </form>" ;
      } else $upload = '' ;
      
      $pr = $lang == 'commons' ? 'wikimedia' : $project ;
      
      if ( $api_output ) {
//      	$use = '' ;
//      	if ( isset ( $commons_usage[$image] ) ) $use = $commons_usage[$image] ;
      	$ret[] = array (
      		'n' => 'image' ,
      		'a' => array (
      			'name' => apitext ( $image_title ) ,
      			'location' => "$lang.$pr" ,
      			'width' => $data->img_width ,
      			'height' => $data->img_height ,
      			'bytes' => $data->img_size ,
      			'file_url' => apiurl ( get_image_url ( $lang , $image , $project ) ) ,
      			'desc_url' => apiurl ( $imageurl ) ,
      			'thumb_url' => apiurl ( $thumburl ) ,
      			'freeness' => $type ,
      			'used_by' => urlencode ( json_encode ( $commons_usage_data[$image] ) ) ,
#      			'note' => apitext ( $template_text . $thumb_text ) ,
      			
      		)
      	) ;
      } else {
// <td $style width='75px'><a target='_blank' href=\"$imageurl\"><img border='0' src='$thumburl'/></a></td>

		  $ret[] = "<tr>" . 
		  	get_image_thumb_link ( $thumburl , $imageurl , get_image_url ( $lang , $image , $project ) , "$style width='75px'" ) . 
			"<td $style>
		  <b>$image_title</b> on $lang.$pr;
		  {$data->img_width}&times;{$data->img_height}px, {$data->img_size} bytes<br/>
		  $template_text$thumb_text$upload
		  </td></tr>" ;
		}
    }
	}
	
	if ( count ( $ret ) == 0 ) { # Nothing found
	    if ( $note != '' ) $note .= '; ' ;
    
		$langlink_links = array () ;
		foreach ( $langlinks AS $k => $v ) {
			$langlink_links[] = "<a target='_blank' href=\"" . get_wikipedia_url ( $k , $v , '' , $project ) . "\">$k</a>" ;
		}
		
		$note .= "Searched for images in " . implode ( ', ' , $langlink_links ) . " - no suitable ones found" ;
	} else {
		if ( $api_output ) {
			return array (
				'n' => 'set' ,
				'a' => array ( 'type' => 'language_links' ) ,
				'*' => $ret
			) ;
		} else {
			$ret = implode ( '' , $ret ) ;
		    $ret = results_section ( $ret , 'Images used in other wikipedias (from the respective wikipedias or from Wikimedia Commons)' , 2 ) ;
		}
	}

	return $ret ;
}

function get_image_thumb_link ( $thumb_url , $page_url , $file_url , $style = '' ) {
	global $thumbsize ;
	if ( strtolower ( substr ( $page_url , -4 , 4 ) ) == '.gif' ) {
		return "<td $style><a target='_blank' href=\"$page_url\"><img border='0' width='$thumbsize"."px' src='$file_url'/></a></td>" ;
	} else {
		return "<td $style><a target='_blank' href=\"$page_url\"><img border='0' src='$thumb_url'/></a></td>" ;
	}
}

#_____________________________________________________________________________________________________________
# GIMP-SAVVY

function find_images_gimp ( $article ) {
	global $params , $api_output ;
	$server = "http://gimp-savvy.com" ;
	$url = "$server/cgi-bin/keysearch.cgi?words=" . urlencode ( $article ) ;
	$html = file_get_contents ( $url ) ;
	$html = array_pop ( explode ( '<table' , $html ) ) ;
	$html = array_shift ( explode ( '</table' , $html ) ) ;
	$trs = explode ( '<tr' , $html ) ;
	array_shift ( $trs ) ;

	$results = array () ;
	foreach ( $trs AS $tr ) {
		if ( count ( explode ( '100%' , $tr , 2 ) ) != 2 ) continue ; # No full match
		$x = explode ( " href='" , $tr , 2 ) ;
		if ( count ( $x ) < 2 ) continue ;
		$x = array_pop ( $x ) ;
		$x = explode ( "'" , $x , 2 ) ;
		$r = "" ;
		$r->img_url = array_shift ( $x ) ;
		
		$x = array_pop ( $x ) ;
		$x = explode ( '<img src=' , $x , 2 ) ;
		if ( count ( $x ) < 2 ) continue ;
		$x = array_pop ( $x ) ;
		$x = explode ( ' ' , $x , 2 ) ;
		$r->thumb_url = array_shift ( $x ) ;
		$x = explode ( '>' , array_pop ( $x ) , 2 ) ;
		$r->thumb_size = array_shift ( $x ) ;
		
		$x = explode ( '<b>' , array_pop ( $x ) , 2 ) ;
		$x = explode ( '</b>' , array_pop ( $x ) , 2 ) ;
//		$r->img_wh = explode ( 'x' , array_shift ( $x ) ) ;
		$r->img_size = str_replace ( 'x' , '&times;' , array_shift ( $x ) ) ;

		
		$y = file_get_contents ( $server . $r->img_url ) ;

		$x = array_pop ( explode ( "<img src='" , $y , 2 ) ) ;
		$x = array_shift ( explode ( "'" , $x , 2 ) ) ;
		$r->img_url_real = $x ;
		
		$x = array_pop ( explode ( "<strong>" , $y , 2 ) ) ;
		$x = array_shift ( explode ( "</strong>" , $x , 2 ) ) ;
		$r->tags = trim ( $x ) ;
		
		$results[] = $r ;
		if ( count ( $results ) >= $params['gimp_max'] ) break ;
	}
	
	if ( count ( $results ) == 0 ) return "" ;
	
	$gs_logo = '//gimp-savvy.com/images/gimp-savvy.gif' ;
	
	if ( $api_output ) $ret = array ( 'n' => 'set' , 'a' => array ( 'type' => 'gimp-savvy' ) , '*' => array() ) ;
	else $ret = "" ;
	foreach ( $results AS $r ) {
		if ( $api_output ) {
			$n = array (
				'n' => 'image' ,
				'a' => array (
					'name' => apitext ( array_pop ( explode ( '?' , $r->img_url ) ) ) ,
					'freeness' => 'PD' ,
					'thumb_url' => apiurl ( $gs_logo ) ,
					'file_url' => apiurl ( $server . $r->img_url_real ) ,
					'desc_url' => apiurl ( $server . $r->img_url ) ,
//					'width' => $r->img_wh[0] ,
//					'height' => $r->img_wh[1] ,
					'text' => apitext ( $r->tags ) ,
				)
			) ;
			$ret['*'][] = $n ;
		} else {
			$ret .= "<tr><td class='gimp' valign='top'>" ;
			$ret .= '<a target="_blank" href="' . $server . $r->img_url_real . '">' ;
//			$ret .= "<img src=\"" . $server . $r->thumb_url . "\" alt='Thumbnail access restricted by GIMP-SAVVY' border=0 " . $r->thumb_size . " />" ;
			$ret .= "<img lowsrc=\"$gs_logo\" src=\"" . $server . $r->img_url_real . "\" width='120px' border=0 alt='Thumbnail access restricted by GIMP-SAVVY' />" ;
			$ret .= "</a>" ;
			$ret .= "</td><td class='gimp' width='100%' valign='top'>" ;
			$ret .= "<b>" . array_pop ( explode ( '?' , $r->img_url ) ) . "</b>" ;
			$ret .= " (<a target='_blank' href='" . $server . $r->img_url . "'>description page</a>)" ;
//			$ret .= "<br/>" . $r->img_size . " px" ;
			$ret .= "<br/>Tags : <i>" . $r->tags . "</i>" ;
			$ret .= "</td></tr>" ;
		}
	}
	if ( $api_output ) return $ret ;
	return results_section ( $ret , "GIMP-SAVVY" , 2 ) ;
}



#_____________________________________________________________________________________________________________
# WIKIDATA

function find_images_wikidata ( $article ) {
	global $params , $api_output , $dbwd ;
	if ( $api_output ) $ret = array ( 'n' => 'set' , '*' => array() , 'a' => array ( 'type' => 'wikidata' ) ) ;
	else $ret = '' ;
	
	if ( !isset($dbwd) ) $dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
	
	$wiki = $params['language'] . $params['project'] ;
	if ( $params['project'] == 'wikipedia' ) $wiki = $params['language'] . 'wiki' ;
	
	$q = '' ;
	$t = $dbwd->real_escape_string ( str_replace ( '_' , ' ' , $article ) ) ;
	$sql = "select page_title FROM wb_items_per_site,page,pagelinks,linktarget WHERE concat('Q',ips_item_id)=page_title and page_namespace=0 and ips_site_id='enwiki' and ips_site_page='$t' and page_id=pl_from and lt_namespace=120 and lt_title='P18' limit 1" ;
	if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']'."\n\n$sql\n");
	while($o = $result->fetch_object()) $q = $o->page_title ;
	if ( $q == '' ) return ; // No image
	
	$url = "https://www.wikidata.org/w/api.php?action=wbgetentities&ids=$q&format=json" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	if ( !isset($j->entities->$q) ) return ;

	$p18 = 'P18' ;
	$claims = $j->entities->$q->claims ;
	if ( !isset($claims->$p18) ) return ;
	
	foreach ( $claims->$p18 AS $c ) {
		$name = $c->mainsnak->datavalue->value ;
		$desc_url = '//commons.wikimedia.org/wiki/File:' . myurlencode ( $name ) ;
		$file_url = get_image_url ( 'commons' , $name , 'wikipedia' ) ;
		$thumb_url = get_thumbnail_url ( 'commons' , $name , 120 , 'wikipedia' ) ;
		
		if ( $api_output ) {
			$ret['*'][] = array ( 'n' => 'image' , 'a' => array (
				'name' => apitext ( $name ) ,
				'desc_url' => apiurl ( $desc_url ) ,
				'file_url' => apiurl ( $file_url ) ,
				'thumb_url' => apiurl ( $thumb_url ) ,
				'freeness' => 'commons' ,
			) ) ;
		} else {
			$ret .= "<tr>" ;
			$ret .= "<td nowrap class='ll_commons'><a target='_blank' href='$desc_url'><img border=0 src='$thumb_url' /></a></td>" ;
			$ret .= "<td class='ll_commons' width='100%' valign='top'><b>$name</b><br/>" ;
			$ret .= "<tt>[[File:$name|thumb|" ;
			if ( $params['default_thumbnail_size'] != '' ) $ret .= $params['default_thumbnail_size'] . "px|" ;
			if ( isset ( $params['use_article_as_description'] ) ) $ret .= $article ;
			else $ret .= $name ;
			$ret .= "]]</tt></td>" ;
			$ret .= "</tr>" ;
		}
	}

	if ( $api_output ) return $ret ;
	
	$search_link = "//www.wikidata.org/wiki/$q" ;
	$search_link = " (<a href='$search_link' target='_blank'>See Wikidata item for $article</a>)" ;
	return results_section ( $ret , 'Wikidata' , 2 , $search_link ) ;
}



#_____________________________________________________________________________________________________________
# COMMONS

function find_images_commons ( $article ) {
	global $params , $api_output ;
	if ( $api_output ) $ret = array ( 'n' => 'set' , '*' => array() , 'a' => array ( 'type' => 'commons' ) ) ;
	else $ret = '' ;
	
	$result = json_decode ( file_get_contents ( "http://commons.wikimedia.org/w/api.php?action=opensearch&search=".myurlencode($article)."&format=json&namespace=6&limit=" . $params['commons_max'] ) ) ;
	$result = $result[1] ;
#	print "<pre>" ; print_r ( $result ) ; print "</pre>" ;
	
	foreach ( $result AS $img ) {
		$name = array_pop ( explode ( ':' , $img , 2 ) ) ;
		$desc_url = '//commons.wikimedia.org/wiki/File:' . myurlencode ( $name ) ;
		$file_url = get_image_url ( 'commons' , $name , 'wikipedia' ) ;
		$thumb_url = get_thumbnail_url ( 'commons' , $name , 120 , 'wikipedia' ) ;
		
		if ( $api_output ) {
			$ret['*'][] = array ( 'n' => 'image' , 'a' => array (
				'name' => apitext ( $name ) ,
				'desc_url' => apiurl ( $desc_url ) ,
				'file_url' => apiurl ( $file_url ) ,
				'thumb_url' => apiurl ( $thumb_url ) ,
				'freeness' => 'commons' ,
			) ) ;
		} else {
			$ret .= "<tr>" ;
			$ret .= "<td nowrap class='ll_commons'><a target='_blank' href='$desc_url'><img border=0 src='$thumb_url' /></a></td>" ;
			$ret .= "<td class='ll_commons' width='100%' valign='top'><b>$name</b><br/>" ;
			$ret .= "<tt>[[File:$name|thumb|" ;
			if ( $params['default_thumbnail_size'] != '' ) $ret .= $params['default_thumbnail_size'] . "px|" ;
			if ( isset ( $params['use_article_as_description'] ) ) $ret .= $article ;
			else $ret .= $name ;
			$ret .= "]]</tt></td>" ;
			$ret .= "</tr>" ;
		}
	}
	
	if ( $api_output ) return $ret ;
	
	$search_link = "//commons.wikimedia.org/w/index.php?title=Special%3ASearch&fulltext=Search&search=" . myurlencode ( $article ) ;
	$search_link = " (<a href='$search_link' target='_blank'>Search Commons for $article</a>)" ;
	return results_section ( $ret , 'Wikimedia Commons' , 2 , $search_link ) ;
}

function find_images_wikitravel ( $article ) {
  return find_images_mediawiki ( $article , 
            'http://wikitravel.org/shared/Special:Search?ns6=1&searchx=Search&search=' , 
            'WikiTravel Shared' ,
            '<ol' ,
            '</ol>' ,
            " (search 
  <a target=\"_blank\" href=\"//wikitravel.org/shared\"><img border='0' src=\"//wikitravel.org/wikitravel-shared.png\" height='14px'/></a>
  <a target=\"_blank\" href=\"//wikitravel.org/shared/Special:Search?ns6=1&searchx=Search&search=$1\">WikiTravel Shared</a>)" ,
            'll_good' ,
            0 ) ;
}

function find_images_mediawiki ( $article , $base_url , $thesource , $sep1 , $sep2 , $theheading , $class , $is_wikimedia ) {
  global $params , $imagetypes , $redirect_from , $api_output ;
  $thumbsize = 75 ;
  cleanup_article_name ( $article ) ;
  $url = $base_url . urlencode ( $article ) ;
  $html = file_get_contents ( $url ) ;
  
  $x = array_pop ( explode ( '</strong>' , $html , 2 ) ) ;
  $x = array_pop ( explode ( $sep1 , $x , 2 ) ) ;
  $x = array_shift ( explode ( $sep2 , $x , 2 ) ) ;
  
  $x = explode ( '</a>' , $x ) ;
  array_pop ( $x ) ; # Not needed
  
  $ret = array() ;
  $count = 0 ;
  foreach ( $x AS $i ) {
    $i = array_pop ( explode ( '">' , $i ) ) ;
    $i2 = array_pop ( explode ( ':' , $i , 2 ) ) ; # Removing "Image:"

    $image_noext = explode ( '.' , $i2 ) ;
    $ext = strtolower ( array_pop ( $image_noext ) ) ;
    $image_noext = implode ( '.' , $image_noext ) ;
    if ( !isset ( $imagetypes[$ext] ) ) continue ;

    # Get image data and check size
    if ( $is_wikimedia ) {
      $data = db_get_image_data ( $i2 , 'commons' , 'wikimedia' ) ;
      if ( !isset ( $data->img_width ) ) continue ;
      if ( $data->img_width == '' or $data->img_height == '' ) continue ; # Non-images
      if ( $data->img_width < $params['min_width'] ) continue ;
      if ( $data->img_height < $params['min_height'] ) continue ;
      
      $count++ ;
      if ( $count > $params['commons_max'] ) break ;

      if ( $data->img_width > $data->img_height ) {
        $nw = $thumbsize ;
      } else {
        $nw = floor ( $data->img_width * $thumbsize / $data->img_height ) ;
      }
    } else {
      $nw = $thumbsize ; # FIX THIS!!!
    }

    if ( $is_wikimedia ) {
      $thumb_url = get_thumbnail_url ( 'commons' , $i2 , $nw , 'wikimedia' ) ;
      $page_url = get_wikipedia_url ( 'commons' , $i , "" , 'wikimedia' ) ;
      $file_url = get_image_url ( 'commons' , $i2 , 'wikimedia' ) ;

      if ( isset ( $params['use_article_as_description'] ) ) $thumb_desc = "$article." ;
      else $thumb_desc = "$image_noext." ;
      if ( $params['default_thumbnail_size'] != '' ) $thumb_desc = $params['default_thumbnail_size'] . "px|" . $thumb_desc ;

      $thumb_text = "<tt>[[$i|thumb|$thumb_desc]]</tt>" ;
    } else {
    
    // http://wikitravel.org/upload/shared/thumb/c/cd/Karte_ubahn_berlin.png/746px-Karte_ubahn_berlin.png
    // http://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Dresden-Garnisionskirche-gp.jpg/796px-Dresden-Garnisionskirche-gp.jpg
    
      $thumb_url = '//wikitravel.org/favicon.ico' ; // HACK FIXME
      $page_url = '//wikitravel.org/shared/Image:' . urlencode ( $i2 ) ;
      $file_url = get_image_url ( 'commons' , $i2 , 'wikimedia' ) ;
      $file_url = str_replace ( 'upload.wikimedia.org/wikimedia/commons' , 'wikitravel.org/upload/shared' , $file_url ) ;
      $thumb_text = '' ;
    }

	if ( $api_output ) {
		$ret[] = array (
			'n' => 'image' ,
			'a' => array (
				'name' => apitext ( $i2 ) ,
				'location' => 'wikitravel' ,
				'freeness' => 'wikitravel' ,
      			'file_url' => apiurl ( $file_url ) ,
				'thumb_url' => apiurl ( $thumb_url ) ,
				'desc_url' => apiurl ( $page_url ) ,
			)
		) ;
	} else {
		$ret[] = '<tr>' ;
		//<td class="' . $class . '" valign="top">' ;
		$ret[] = get_image_thumb_link ( $thumb_url , $page_url , $file_url , 'class="' . $class . '" valign="top"' ) ;
//		$ret[] = "<a target='_blank' href=\"$page_url\"><img src=\"$thumb_url\" border='0' /></a></td>" ;
		$ret[] = '<td class="' . $class . '" valign="top" width="100%">' ;
		$ret[] = "<b>$i2</b>" ;
		if ( $thumb_text != '' ) $ret[] = "<br/>$thumb_text" ;
		$ret[] = "</td></tr>" ;
	}
  }

  if ( isset ( $params['commonsense'] ) and $is_wikimedia and !$api_output ) {
  	$keywords = trim ( str_replace('_',' ',"$article $redirect_from") ) ;
    $cs = real_common_sense ( 'en' , '' , explode ( ' ' , $keywords ) , array ( 'CATEGORIES' , 'GALLERIES' ) ) ;
    $categories = isset ( $cs['CATEGORIES'] ) ? $cs['CATEGORIES'] : array() ;
    $galleries = isset ( $cs['GALLERIES'] ) ? $cs['GALLERIES'] : array() ;
    if ( count ( $categories ) + count ( $galleries ) > 0 ) {
      $ret[] = "<tr><td colspan='2' class='$class'>" ;

      if ( count ( $categories ) > 0 ) {
        $ret[] = "Try the following categories on Commons : " ;
        $a = array () ;
        foreach ( $categories AS $c ) {
          $url = get_wikipedia_url ( 'commons' , "Category:$c" , '' , 'wikimedia' ) ;
          $a[] = "<a target='_blank' href=\"$url\">$c</a>" ;
        }
        $ret[] = implode ( ", " , $a ) ;
        $ret[] = "<br/>" ;
      }
      
      if ( count ( $galleries ) > 0 ) {
        $ret[] = "Try the following galleries on Commons : " ;
        $a = array () ;
        foreach ( $galleries AS $c ) {
          $url = get_wikipedia_url ( 'commons' , $c , '' , 'wikimedia' ) ;
          $a[] = "<a target='_blank' href=\"$url\">$c</a>" ;
        }
        $ret[] = implode ( ", " , $a ) ;
        $ret[] = "<br/>" ;
      }
      
      $ret[] = "</td></tr>" ;
    }
  }

  $urltitle = urlencode ( $article ) ;
  $search_link = str_replace ( '$1' , $urltitle , $theheading ) ;
  
  if ( $api_output ) {
  	return array (
  		'n' => 'set' ,
  		'*' => $ret ,
  		'a' => array ( 'type' => $is_wikimedia ? 'commons' : 'wikitravel' ) 
  	) ;
  }
  
	$ret = implode ( '' , $ret ) ;
	return results_section ( $ret , $thesource , 2 , $search_link ) ;
}



#_____________________________________________________________________________________________________________
# EVERYSTOCKPHOTO

function find_images_everystockphoto ( $article ) {
  global $params , $imagetypes , $api_output ;
  cleanup_article_name ( $article ) ;
  // http://www.everystockphoto.com/search.php?initialSearch=true&sField=bristol&x=0&y=0&sBool=and&sSource=&sLicense=&sPortrait=on&sLandscape=on&sSquare=on&sMinW=&sMinH=&sDispRes=on&sDispLic=on&sDispSrc=on&sDispRtg=on
//  $url = "http://everystockphoto.com/results.php?seed=" . urlencode ( $article ) ;
  $url = "http://www.everystockphoto.com/search.php?initialSearch=true&sField=" . urlencode ( $article ) ;
  $search_link = " (search <a target='_blank' href=\"$url\">everystockphoto</a> manually)" ;
  $html = file_get_contents ( $url ) ;
  $thumbs = explode ( '<div class="thumb">' , $html ) ;
  array_shift ( $thumbs ) ; # Not needed
  
  $ret = "" ;
  $apiret = array ( 'n' => 'set' , '*' => array() , 'a' => array ( 'type' => 'everystockphoto' ) ) ;
  $count = 0 ;
  foreach ( $thumbs AS $t ) {
  	$t = array_shift ( explode ( '</a>' , $t , 2 ) ) ;
  	$name = htmlspecialchars_decode ( get_text_between ( $t , ' title="' , '"' ) ) ;
  	$text = htmlspecialchars_decode ( get_text_between ( $t , ' alt="' , '"' ) ) ;
  	$thumb_url = get_text_between ( $t , ' src="' , '"' ) ;
  	$desc_url = "//everystockphoto.com/" . get_text_between ( $t , ' href="' , '"' ) ;
  	$file_url = '//www.everystockphoto.com/gotoImage.php?imageId=' . array_pop ( explode ( '?imageId=' , $desc_url , 2 ) ) ;
  	$class = 'll_unknown' ;

	if ( $api_output ) {
		$apiret['*'][] = array ( 'n' => 'image' , 'a' => array (
			'name' => apitext ( $name ) ,
			'text' => apitext ( $text ) ,
			'freeness' => 'unknown' ,
			'thumb_url' => apiurl ( $thumb_url ) ,
			'desc_url' => apiurl ( $desc_url ) ,
			'file_url' => apiurl ( $file_url ) ,
		) ) ;
	} else {
		$ret .= "<tr>" ;
		$ret .= "<td class='$class'><a href='$desc_url' target='_blank'><img src='$thumb_url' border=0 /></a></td>" ;
		$ret .= "<td valign='top' class='$class' width='100%'>$name<br/>$text</td>" ;
		$ret .= "</tr>" ;
	}

  	$count++ ;
  	if ( $count >= $params['esp_max'] ) break ;
  }  
  
  if ( $api_output ) return $apiret ;
  return results_section ( $ret , "everystockphoto.com" , 2 , $search_link ) ;
}


#_____________________________________________________________________________________________________________
# GEOGRAPH.ORG

function get_geograph_button ( $url ) {
	global $tusc_user , $tusc_password ;
	$ret = '' ;
    $ret .= "<form target='_blank' action='geograph_org2commons.php' method='post' style='display:inline;margin:0'>" ;
//    $ret .= "Single-click upload to Commons as " ;
//    $ret .= "<input type='text' class='span2' size='50' name='new_name' value=\"" . ansi2ascii ( $ft ) . ".jpg\" />" ;
    $ret .= "<input type='hidden' name='flickr_url' value='$url' />" ;
//    $ret .= "<input type='hidden' name='gotoeditpage' value='1' />" ;
    $ret .= "<input type='hidden' name='tusc_user' value='$tusc_user' />" ;
    $ret .= "<input type='hidden' name='tusc_password' value='$tusc_password' />" ;
//    $ret .= "<input type='hidden' name='server' value='$server' />" ;
    $ret .= "<input type='submit' name='doit' value='Directly upload this image to Commons using TUSC' />" ;
    $ret .= "</form>" ;
    return $ret ;
}
/*
function find_images_geograph_uk ( $article ) {
	return find_images_geograph ( $article , 'http://www.geograph.org.uk' , 'geograph.org.uk' , 'geograph_max' ) ;
}

function find_images_geograph_de ( $article ) {
	return find_images_geograph ( $article , 'http://geo.hlipp.de' , 'geo.hlipp.de' , 'geograph_max_de' ) ;
}
*/
function find_images_geograph ( $article , $base_url , $base_type , $base_max ) {
	global $tusc_user , $tusc_password ;
  global $params , $imagetypes , $api_output ;
//  $server = array_pop ( explode ( '/' , $base_url ) ) ;
  $apiret = array ( 'n' => 'set' , 'a' => array('type'=>$base_type) , '*' => array() ) ;
  $ret = '' ;
  $cnt = 0 ;
  cleanup_article_name ( $article ) ;
//  $base_query_url = 'http://www.nearby.org.uk/geograph/find-syndicator.php?format=GeoPhotoRSS&query=' ;
  $base_query_url = $base_url . '/syndicator.php?format=GeoPhotoRSS&text=' ;
  $query_url = $base_query_url ;
  $query_url .= urlencode ( $article ) ;
//  if ( $base_type == 'geograph.co.uk' ) $query_url .= '&source=fist' ;
//  print_r ( "<p>$query_url</p>" ) ;
  $rss = utf8_encode ( file_get_contents ( $query_url ) ) ;
  $items = explode ( '<item ' , $rss ) ;
  
  if ( count ( $items ) == 1 ) { # No results
	  $query_url = $base_query_url ;
	  $query_url .= urlencode ( $article ) ;
	  $rss = file_get_contents ( $query_url ) ;
	  $items = explode ( '<item ' , $rss ) ;
  }
  
  array_shift ( $items ) ;
  foreach ( $items AS $i ) {
  	$thumb = get_text_between ( $i , '<photo:thumbnail>' , '</photo:thumbnail>' ) ;
  	$url = get_text_between ( $i , '<link>' , '</link>' ) ;
  	$desc = get_text_between ( $i , '<description>' , '</description>' ) ;
  	$source = get_text_between ( $i , '<dc:source>' , '</dc:source>' ) ;
  	$author = get_text_between ( $i , '<dc:creator>' , '</dc:creator>' ) ;
  	$title = get_text_between ( $i , '<title>' , '</title>' ) ;
  	$date = get_text_between ( $i , '<dc:date>' , '</dc:date>' ) ;
  	$coord = get_text_between ( $i , '<georss:point>' , '</georss:point>' ) ;
  	
  	$source_id = array_pop ( explode ( '/' , $url ) ) ;
  	$coor = str_replace ( ' ' , '|' , $coord ) ;
  	if ( $coor != '' ) $coor = "{{Location dec|$coor}}" ;
  	
  	if ( $api_output ) {
  		$full_page = file_get_contents ( $url ) ;
		$image_url = $full_page ;
		$image_url = array_pop ( explode ( 'mainphoto' , $image_url , 2 ) ) ;
		$image_url = array_shift ( explode ( '/>' , $image_url , 2 ) ) ;
		$image_url = array_pop ( explode ( 'src="' , $image_url , 2 ) ) ;
		$image_url = array_shift ( explode ( '"' , $image_url , 2 ) ) ;

		$ar = array ( 'n' => 'image' , 'a' => array (
			'source' => apitext ( $source ) ,
			'author' => apitext ( $author ) ,
			'name' => apitext ( $title ) ,
			'coordinates' => apitext ( $coord ) ,
			'date' => apitext ( $date ) ,
			'thumb_url' => apiurl ( $thumb ) ,
			'desc_url' => apiurl ( $url ) ,
			'file_url' => apiurl ( $image_url ) ,
			'text' => apitext ( $desc ) ,
			'freeness' => 'CC-BY-SA-2.0' ,
		) ) ;
		$apiret['*'][] = $ar ;
	}
  	
  	$ret .= '<tr>' ;
  	$ret .= "<td class='ll_good'>
  	<a target='_blank' href=\"$url\"><img border=0 src=\"$thumb\"/></a>
  	</td><td valign='top' class='ll_good' width='50%'>
  	<b>$title</b><br/>
  	<i><small>$desc</small></i><br/>
  	Taken by <a href='$source' target='_blank'>$author</a> on $date at $coord.<br/>
  	Released under <a href='//creativecommons.org/licenses/by-sa/2.0/' target='_blank'>CC-BY-SA 2.0</a>." ;
  	if ( $tusc_user != '' && $tusc_password != '' ) $ret .= get_geograph_button ( $url ) ;
  	$ret .= "
  	</td><td valign='top' class='ll_good' width='50%'>
  	<textarea style='font-size:80%;width:100%' cols='50' rows='6'>
{{Information
|Description={{de|$desc}}
|Source=From [$url $base_type]
|Date=$date
|Author=[$source $author]
|Permission=Creative Commons Attribution Share-alike license 2.0
}}
$coor

{{geograph|$source_id|$author}}
</textarea>
  	</td>" ;
  	$ret .= '</tr>' ;
  	$cnt++ ;
  	if ( $cnt >= $params[$base_max] ) break ;
  }

	if ( $api_output ) return $apiret ;
  return results_section ( $ret , "<a href='$base_url' target='_blank'>$base_type</a>" , 3 ) ;
}



#_____________________________________________________________________________________________________________
# agenciabrasil.gov.br

function find_images_agenciabrasil ( $article ) {
  global $params , $imagetypes , $api_output ;
  $apiret = array ( 'n' => 'set' , 'a' => array ( 'type' => 'agenciabrasil' ) , '*' => array() ) ;
  $ret = '' ;
  $cnt = 0 ;
  cleanup_article_name ( $article ) ;

  $base_query_url = 'http://www.agenciabrasil.gov.br/imagens/search?portal_type=ATPhoto&SearchableText=' ;
  $query_url = $base_query_url ;
  $query_url .= urlencode ( $article ) ;
  $cont = file_get_contents ( $query_url ) ;
  
  $key1 = ' nowrap valign="top">' ;
  $key2 = 'http://www.agenciabrasil.gov.br/media/' ;
  $key3 = 'http://stream.agenciabrasil.gov.br/media/imagens' ;
  $items = explode ( $key1 , $cont ) ;
  array_shift ( $items ) ; // Pre-table
  foreach ( $items AS $i ) {
  	$thumb = array_shift ( explode ( '</tr>' , $i ) ) ;
  	$thumb = array_pop ( explode ( '<img' , $thumb ) ) ;
  	$thumb = get_text_between ( $thumb , '"' , '"' ) ;
  	$url = $key2 . get_text_between ( $i , $key2 , '"' ) ;
  	$i2 = explode ( ' class="lista1"' , $i ) ;
  	$i2 = array_pop ( $i2 ) ;
  	$desc = get_text_between ( $i2 , '/view">' , '</a>' ) ;

  	$i2 = explode ( ' class="lista2"' , $i ) ;
  	$i2 = array_pop ( $i2 ) ;
  	$date = get_text_between ( $i2 , '<i>' , '</i>' ) ;

  	$author = 'Brasilian government' ;
  	
  	if ( $api_output ) {
  		$file_url = str_replace ( 'http://www.' , 'http://stream.' , $url ) ;
  		$file_url = str_replace ( '/view' , '' , $file_url ) ;
  		$name = explode ( '/' , $url ) ;
  		array_pop ( $name ) ;
  		$name = array_pop ( $name ) ;
  		$apiret['*'][] = array ( 'n' => 'image' , 'a' => array (
  			'name' => apitext ( $name ) ,
  			'desc_url' => apiurl ( $url ) ,
  			'thumb_url' => apiurl ( $thumb ) ,
  			'file_url' => apiurl ( $file_url ) ,
  			'text' => apitext ( $desc ) ,
  			'freeness' => 'CC-BY-2.5' ,
  		) ) ;
  	}

  	$ret .= '<tr>' ;
  	$ret .= "<td class='ll_good'>
  	<a target='_blank' href=\"$url\"><img border=0 src=\"$thumb\"/></a>
  	</td><td valign='top' class='ll_good' width='50%'>
  	<i><small>$desc</small></i><br/>
  	Taken by <a href='//www.agenciabrasil.gov.br' target='_blank'>$author</a> on $date.<br/>
  	Released under <a href='//creativecommons.org/licenses/by/2.5/' target='_blank'>CC-BY 2.5</a>.
  	</td><td valign='top' class='ll_good' width='50%'>
  	<textarea style='font-size:80%;width:100%' cols='50' rows='5'>
{{Information
|Description=$desc
|Source=From [$url agenciabrasil.gov.br]
|Date=$date
|Author=$author
|Permission=Creative Commons Attribution license 2.5 (Brasil)
}}</textarea>
  	</td>" ;
  	$ret .= '</tr>' ;
  	$cnt++ ;
  	if ( $cnt >= $params['ab_max'] ) break ;
  }

	if ( $api_output ) return $apiret ;
  return results_section ( $ret , "<a href='//www.agenciabrasil.gov.br' target='_blank'>agenciabrasil.gov.br</a>" , 3 ) ;
}



#_____________________________________________________________________________________________________________
# THUMBNAILROW

function get_thumbnail_row ( $images ) {
  global $language , $project , $thumbsize , $api_output ;
  $images_intl = array () ;
  foreach ( $images AS $k => $i ) {
  	$images_intl[$k] = array_pop ( explode ( ':' , $i , 2 ) ) ;
  }
  $local_images = db_local_images ( $language , $images_intl ) ;
  $apiret = array() ;
  $ret = '<tr>' ;
  $divsize = $thumbsize + 5 ;
  $divsize = $divsize . "px" ;
  $count = 0 ;
  foreach ( $images AS $i ) {
  	$i_short = array_pop ( explode ( ':' , $i , 2 ) ) ;
  	$is_local = in_array ( get_db_safe ( $i_short ) , $local_images ) ;
  	$lang = $is_local ? $language : 'commons' ;
  	$proj = $is_local ? $project : 'wikimedia' ;
  	
  	$data = db_get_image_data ( $i_short , $lang , $proj ) ;
  	if ( !isset ( $data->img_width ) or $data->img_width <= 0 ) $nw = $thumbsize ;
  	else if ( $data->img_width > $data->img_height ) $nw = $thumbsize ;
  	else $nw = floor ( $data->img_width * $thumbsize / $data->img_height ) ;
  	
  	$templates = db_get_used_templates ( $lang , $i_short , 6 ) ;
    $type = get_image_type ( $lang , $i_short , $templates ) ;
    $class = 'unknown' ;
    if ( $lang == 'commons' ) $class = 'commons' ;
    if ( $type == 'good' or $type == 'bad' ) $class = $type ;

  	
  	$thumb_url = get_thumbnail_url ( $lang , $i_short , $nw , $proj  ) ;
  	$image_url = get_wikipedia_url ( $lang , "Image:$i_short" , '' , $proj ) ;
  	
  	if ( $api_output ) {
  		$apiret[] = array ( 
  			'n' => 'image' ,
  			'a' => array (
  				'name' => htmlspecialchars($i_short) , 
  				'location' => "$lang.$project" ,
  				'freeness' => $type ,
      			'file_url' => apiurl ( get_image_url ( $lang , $i_short , $project ) ) ,
  				'desc_url' => apiurl($image_url) , 
  				'thumb_url' => apiurl($thumb_url) ,
  			) ,
  		) ;
	}
  	
  	
  	$ret .= "<td width='$divsize' height='$divsize' align='center' class='ll_$class'>" ;
  	$ret .= "<a target='_blank' href=\"$image_url\"><img border=0 src=\"$thumb_url\" /></a><br/><b>$i_short</b></td>\n" ;
  	$count++ ;
  	if ( $count % 6 == 0 and $count < count ( $images ) ) $ret .= "</tr><tr>" ;
  }
  $ret .= '</tr>' ;
  if ( $api_output ) return array ( 'n' => 'set' , '*' => $apiret , 'a' => array ( 'type' => 'already_in_article' ) ) ;
  return results_section ( $ret , "Images already in article" , count ( $images ) ) ;
}


#______________________________________________________
# Find categories to skip

function is_article_in_skip_category ( $article ) {
	global $skip_categories , $language , $project , $wq ;
	
	# Load categories
	if ( count ( $skip_categories ) == 0 ) {
		$url = get_wikipedia_url ( "meta" , "FIST/Ignored_categories" , "raw" , "wikipedia" ) ; ;
		$url = str_replace ( '%2F' , '/' , $url ) ;
		$text = file_get_contents ( $url ) ;
		$lines = explode ( "\n" , $text ) ;
		$in_section = false ;
		foreach ( $lines AS $l ) {
			if ( substr ( $l , 0 , 1 ) == '=' ) { # Heading
				if ( $in_section ) break ; # New section, end this
				$l = strtolower ( trim ( str_replace ( '=' , '' , $l ) ) ) ;
				$in_section = $l == "$language.$project" ;
			} else {
				if ( !$in_section ) continue ;
				$l = str_replace ( '_' , ' ' , $l ) ;
				$l = trim ( substr ( $l , 1 ) ) ;
				if ( $l == '' ) continue ;
				$skip_categories[] = $l ;
			}
		}
	}
	
	# Fetch article categories
	$cats = $wq->get_categories ( $article ) ;
	foreach ( $cats AS $c ) {
		$c = trim ( str_replace ( '_' , ' ' , $c ) ) ;
		if ( in_array ( $c , $skip_categories ) ) return true ;
	}
	
	return false ;
}


#______________________________________________________
# Main routine for finding images

function db_is_redirect ( $title , $language , $project , $ns = 0 ) {
	global $db ;
	make_db_safe ( $title ) ;
	
	$sql = "SELECT page_id,page_is_redirect FROM page WHERE page_namespace=\"{$ns}\" AND page_title=\"{$title}\"" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$o = $result->fetch_object();
	if ( !isset ( $o->page_is_redirect ) ) return false ; # Just in case...
	return $o->page_is_redirect ;
}

function find_images ( $article ) {
	global $sources , $params , $language , $project , $wq , $note ;
	global $language_links , $wq , $redirect_from , $api_output ;
	global $geograph_sites ;
	global $_REQUEST ;
	
	# Check and follow redirect
	if ( db_is_redirect ( $article , $language , $project , 0 ) ) {
		$redirect_from = $article ;
		$article = $wq->get_redirect_target ( $article ) ;
	} else $redirect_from = '' ;
	
	# Check for "skip categories"
	if ( isset ( $params['skip_articles_in_categories'] ) ) {
		if ( is_article_in_skip_category ( $article ) ) return ;
	}
	
	# Prep
	$note = '' ;
	$out = array () ;
	$article = str_replace ( '_' , ' ' , $article ) ;
	$uarticle = str_replace ( ' ' , '_' , $article ) ;
	$skip_no_candidates = isset ( $params['skip_no_candidate'] ) ;
//	print "!!$skip_no_candidates!!<br/>" ;
	
	# Check article
	$existing_images = db_get_image_list ( $article , $language , $project ) ;

	if ( isset($_REQUEST['count_only_jpg']) ) {
		$a = array() ;
		foreach ( $existing_images AS $f ) {
			if ( preg_match ( '/\.jpe{0,1}g$/i' , $f ) ) $a[] = $f ;
		}
		$existing_images = $a ;
	}
	
//	$existing_images = $wq->get_images_on_page ( $article , 1 ) ;
	$noi = count ( $existing_images , 1 ) ; # !!!!
	if ( $params['forarticles'] == 'all' ) $max = $noi+1 ;
	else if ( $params['forarticles'] == 'noimage' ) $max = 1 ;
	else $max = $params['lessthan_images'] ;
	

	
	$images_message = $noi > 1 ? 'images' : 'image' ;
	if ( $noi == 0 ) $images_message = "no images" ;
	else $images_message = "$noi $images_message" ;
	if ( $noi >= $max and $skip_no_candidates ) return ; # Don't show
  
  
  	if ( $api_output ) {
  	} else {
//	  	if ( $noi >= $max ) $out[] = "<font color='green'><i>Not searched (already has $images_message).</i></font>" ;
	  	if ( $noi >= $max ) return ;
	  	$images_message = "<br/>(has $images_message)" ;
	}
	

	# Run through the searches
	if ( count ( $out ) == 0 ) {
		unset ( $out_wp );
		unset ( $out_co );
		
    	if ( isset ( $sources['languagelinks'] ) ) $out_wp = find_images_languagelinks ( $article ) ;
    	else unset ( $language_links ) ;

	    if ( isset ( $sources['commons'] ) ) $out_co = find_images_commons ( $article ) ;

	    if ( isset ( $sources['wikidata'] ) ) $out_co = find_images_wikidata ( $article ) ;
	    
	    if ( $api_output ) {
		    if ( isset ( $out_co ) ) $out[] = $out_co ;
		    if ( isset ( $out_wp ) ) $out[] = $out_wp ;
		 } else {
		    if ( isset ( $out_co ) ) {
		    	if ( is_array ( $out_co ) ) $out[] = implode ( '' , $out_co ) ;
		    	else $out[] = $out_co ;
		    }
		    if ( isset ( $out_wp ) ) {
		    	if ( is_array ( $out_wp ) ) $out[] = implode ( '' , $out_wp ) ;
		    	else $out[] = $out_wp ;
		    }
		 }
	    
	    
	    if ( isset ( $sources['wts'] ) ) $out[] = find_images_wikitravel ( $article ) ;
	    
    	if ( isset ( $params['jpeg'] ) ) { # The rest only have jpegs, no need to search them if that's not wanted
			if ( isset ( $sources['flickr'] ) ) $out[] = find_images_flickr ( $article ) ;
			if ( isset ( $sources['freemages'] ) ) $out[] = find_images_freemages ( $article ) ;
			if ( isset ( $sources['ipernity'] ) ) $out[] = find_images_ipernity ( $article ) ;
			if ( isset ( $sources['picasa'] ) ) $out[] = find_images_picasa ( $article ) ;
			if ( isset ( $sources['gimp'] ) ) $out[] = find_images_gimp ( $article ) ;
			if ( isset ( $sources['everystockphoto'] ) ) $out[] = find_images_everystockphoto ( $article ) ;
			
			foreach ( $geograph_sites AS $site => $server ) {
				$k = '' ;
				if ( $site != 'uk' ) $k = "_$site" ;
				if ( !isset ( $sources['geograph'.$k] ) ) continue ;
				$out[] = find_images_geograph ( $article , 'http://'.$server , $server , 'geograph_max'.$k ) ;
			}
//			if ( isset ( $sources['geograph'] ) ) $out[] = find_images_geograph_uk ( $article ) ;
//			if ( isset ( $sources['geograph_de'] ) ) $out[] = find_images_geograph_de ( $article ) ;
			if ( isset ( $sources['agenciabrasil'] ) ) $out[] = find_images_agenciabrasil ( $article ) ;
		}
	}
	
	if ( !$api_output ) {
		foreach ( $out AS $k => $v ) {
			if ( $v == '' ) unset ( $out[$k] ) ;
		}
	}
	
	if ( isset ( $params['show_existing_images'] ) and $noi > 0 ) {
//	print "TEST : " . count ( $out ) . " : $skip_no_candidates ; $noi<br/>" ;
		if ( count ( $out ) == 0 and $skip_no_candidates ) return ; # No results for article
		$out[] = get_thumbnail_row ( $existing_images ) ;
	}

	# Nothing found, and skip articles when nothing found?
	if ( count ( $out ) == 0 and $skip_no_candidates ) return ; # No results for article
	
	# Output
	if ( !$api_output ) {
		$out = implode ( '' , $out ) ;
		if ( $out == '' ) {
			if ( $skip_no_candidates ) return ;
			$out = "<font color='red'><i>No images found!</i></font>" ;
		}
		if ( $note != '' ) $out .= "<br/>$note." ;
		$out = str_replace ( "</table><br/>" , "</table>" , $out ) ;
	}
	
	$v_url = get_wikipedia_url ( $language , $article , '' , $project ) ;
	$e_url = get_wikipedia_url ( $language , $article , 'edit' , $project ) ;
	
	if ( $api_output ) {
		return array (
			'n' => 'article' ,
			'*' => $out ,
			'a' => array ( 'title' => htmlspecialchars ( $article ) , 'view_url' => apiurl($v_url) , 'edit_url' => apiurl($e_url) , 'has_images' => $noi )
		) ;
	} else {
		print "<tr>
		<th class='main_th' valign='top' width='150px'>
		<a target='_blank' href=\"$v_url\">$article</a><br/>
		[<a target='_blank' href=\"$e_url\">edit</a>]$images_message</th>
		<td style='border-bottom:2px solid black' valign='top'>
		$out
		</td></tr>\n" ;
	}
}

function get_search_url() {
  global $language , $project , $data , $datatype , $sources , $params ;
  global $_REQUEST ;
  $url = '/fist/fist.php?doit=1' ;
  $url .= '&language=' . urlencode ( $language ) ;
  $url .= '&project=' . urlencode ( $project ) ;
  $url .= '&data=' . urlencode ( $data ) ;
  $url .= '&datatype=' . urlencode ( $datatype ) ;
  
  foreach ( $params AS $k => $v )
    $url .= '&params' . urlencode ( "[$k]" ) . '=' . urlencode ( $v ) ;
  foreach ( $sources AS $k => $v )
    $url .= '&sources' . urlencode ( "[$k]" ) . '=' . urlencode ( $v ) ;

	if ( isset($_REQUEST['count_only_jpg']) ) $url .= '&count_only_jpg=1' ;

  return $url ;
}

function apiurl ( $url ) {
	return htmlentities ( $url ) ;
}

function apitext ( $t ) {
	return htmlspecialchars ( $t ) ;
}

function api2xml ( $d ) {
	if ( is_array ( $d ) ) {
		if ( count ( $d ) == 0 ) return '' ;
		$ret = '<' . $d['n'] ;
		
		if ( isset ( $d['a'] ) ) {
			foreach ( $d['a'] AS $k => $v ) {
				$ret .= ' ' . $k . '="' . $v . '"' ;
			}
		}
		
		if ( isset ( $d['*'] ) ) {
			$ret .= '>' ;
			if ( is_array ( $d['*'] ) ) {
				foreach ( $d['*'] AS $sub ) $ret .= api2xml ( $sub ) ;
			} else $ret .= $d['*'] ;
			$ret .= '</' . $d['n'] . '>' ;
		} else $ret .= ' />' ;
		return $ret ;
	} else return $d ;
}

function get_xml_attr ( $xml , $attr , $default = '' ) {
	if ( isset ( $xml['a'][$attr] ) ) return $xml['a'][$attr] ;
	return $default ;
}

/*
	location="commons.wikimedia" 
	desc_url="http://commons.wikimedia.org/w/index.php?title=Image%3AAbd_alfatah_ismaeel.jpg" 
	thumb_url="http://upload.wikimedia.org/wikimedia/commons/thumb/8/89/Abd_alfatah_ismaeel.jpg/54px-Abd_alfatah_ismaeel.jpg" 
*/

function api2wiki ( $d ) {
	$freeness_color = array (
		'good' => '#CCFFCC' ,
		'bad' => '#FFCCCC' ,
		'unknown' => '#FFFFCC' ,
		'commons' => '#CCCCFF' ,
		'Creative Commons' => '#DDCCDD' ,
		'PD' => '#CCFFCC' ,
		'wikitravel' => '#FFFFCC' ,
		'CC-BY-SA-2.0' => '#CCFFCC' ,
		'CC-BY-2.5' => '#CCFFCC'
	) ;
	
	$url = $d['a']['run_url'] ;
	$url2 = str_replace ( '?doit=1' , '?' , $url ) ;
	$ret = "== FIST ==\n" ;
	$ret .= "FIST [$url rerun] / [$url2 seed settings]\n" ;
	$ret .= "{| border=1\n" ;
	$ret .= "|-\n!Article!!Image location!!Image name and properties!!Image links\n" ;
	foreach ( $d['*'] AS $article ) {
		if ( !isset ( $article['*'] ) ) continue ;

		$rows = 0 ;
		foreach ( $article['*'] AS $set ) {
			if ( !isset ( $set['*'] ) ) continue ;
			foreach ( $set['*'] AS $image ) {
				$rows++ ;
			}
		}
		if ( $rows == 0 ) $rows = 1 ;

		$ret .= "|-\n!valign='top' rowspan=$rows|[[" . $article['a']['title'] . "]]<br/><small>has " . $article['a']['has_images'] . " image(s)</small>\n" ;
		$first_set = true ;
		foreach ( $article['*'] AS $set ) {
			if ( !isset ( $set['*'] ) ) continue ;
			$rows2 = count ( $set['*'] ) ;
			if ( $rows2 == 0 ) $rows2 == 1 ;
			$type = $set['a']['type'] ;
			$type = ucfirst ( str_replace ( '_' , ' ' , $type ) ) ;
			if ( $first_set ) $first_set = false ;
			else $ret .= "|-\n" ;
			$ret .= "!valign='top' rowspan=$rows2|$type\n" ;
			$first_image = true ;
			foreach ( $set['*'] AS $image ) {
				$loc = get_xml_attr ( $image , 'location' ) ;
				$freeness = get_xml_attr ( $image , 'freeness' , '' ) ;
				if ( $freeness == '' and $type == 'Flickr' ) $freeness = 'Creative Commons' ;
				if ( !isset ( $freeness_color[$freeness] ) ) $freenexx = 'unknown' ;
				$fc = ' bgcolor="' . $freeness_color[$freeness] . '" ' ;
				
				if ( $first_image ) $first_image = false ;
				else $ret .= "|-\n" ;
				
				$img_name = get_xml_attr ( $image , 'name' , '???' ) ;
				$ret .= "|valign='top' $fc|$img_name" ;
				$ret .= "<br/><br/>\n" . ucfirst ( $loc ) ;
				$ret .= "<br/>\n" . get_xml_attr ( $image , 'width' , '?' ) . "&times;" . get_xml_attr ( $image , 'height' , '?' ) . " (" . get_xml_attr ( $image , 'bytes' , '?' ) . " bytes)" ;
				$ret .= "\n|$fc|[" . get_xml_attr ( $image , 'file_url' ) . " File]" ;
				$ret .= "<br/>\n[" . get_xml_attr ( $image , 'desc_url' ) . " Description]" ;
				
				if ( $loc == 'commons.wikimedia' ) {
					$ret .= "<br/>\n[[File:" . get_xml_attr ( $image , 'name' ) . "|thumb|none|]]" ;
				} else {
					$ret .= "<br/>\n[" . get_xml_attr ( $image , 'thumb_url' ) . " Thumb]" ;
				}
				
				if ( $loc == 'flickr' ) {
					$nn = urlencode ( $article['a']['title'] . ' (' . get_xml_attr ( $image , 'id' ) . ').jpg' ) ;
					$fu = urlencode ( get_xml_attr ( $image , 'desc_url' ) ) ;
					$ret .= "<br/>\n[http://tools.wmflabs.org/flickr2commons/?flickr_url=$fu&new_name=$nn Transfer to Commons]" ;
				} else if ( $type == 'Language links' and $loc != 'commons.wikimedia' ) {
					$a = explode ( '.' , $loc ) ;
					$lang = array_shift ( $a ) ;
					$proj = array_shift ( $a ) ;
					$url = "//tools.wmflabs.org/commonshelper/?language=$lang&project=$proj&image=" . urlencode ( $img_name ) ;
					$ret .= "<br/>\n[$url Transfer to Commons]" ;
				}
				
				$ret .= "\n" ;
			}
		}
	}
	$ret .= "|}\n" ;
	return $ret ;
}

#____________________________________________________________________________________________________________

# Read request
$language = get_request ( 'language' , 'en' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;
if ( $language == 'species' ) $project = 'wikipedia' ;
$data = trim ( get_request ( 'data' , '' ) ) ;
$datatype = get_request ( 'datatype' , 'categories' ) ;
$sources = get_request ( 'sources' , array() ) ;
$params = get_request ( 'params' , array() ) ;

$tusc_user = get_request ( 'tusc_user' , '' ) ;
$tusc_password = get_request ( 'tusc_password' , '' ) ;

$doit = isset ( $_REQUEST['doit'] ) ;
$test = isset ( $_REQUEST['test'] ) ;
$skip_categories = array () ;

$image_ns_name = 'File' ;
if ( $language == 'de' ) $image_ns_name = 'Datei' ;

$wq = new WikiQuery ( $language , $project ) ;

# Init for new form
if ( !$doit and count ( $sources ) == 0 ) {
  $sources = array ( 
    'flickr' => 1,
    'languagelinks' => 1,
    'commons' => 1,
    'wikidata' => 1,
  ) ;
  $params = array (
    'catdepth' => 0,
    'flickr_max' => 5,
    'll_max' => 5,
    'gimp_max' => 5,
    'freemages_max' => 5,
    'commons_max' => 5,
    'wts_max' => 5,
    'esp_max' => 5,
    'ab_max' => 5,
    'picasa_max' => 5,
    'random' => 50,
    'lessthan_images' => 3,
    'default_thumbnail_size' => '',
    'min_width' => 80,
    'min_height' => 80,
    'include_flickr_id' => 1,
	'esp_skip_flickr' => 1,
    'jpeg' => 1,
    'gif' => 1,
#    'tiff' => 1,
    'png' => 1,
    'svg' => 1,
    'ogg' => 1,
  ) ;

	foreach ( $geograph_sites AS $site => $server ) {
		$k = $site == 'uk' ? '' : "_$site" ;
	    $params['geograph_max'.$k] = 5 ;
	}

}



# Some implicated rules
if ( !isset($params['default_thumbnail_size']) ) $params['default_thumbnail_size'] = 200 ;
if ( !isset($params['wts_max']) ) $params['wts_max'] = 5 ;
if ( !isset($params['picasa_max']) ) $params['picasa_max'] = 5 ;
if ( !isset($params['freemages_max']) ) $params['freemages_max'] = 5 ;
if ( !isset($params['geograph_max']) ) $params['geograph_max'] = 5 ;
if ( !isset($params['geograph_max_de']) ) $params['geograph_max_de'] = 5 ;
if ( !isset($params['geograph_max_channel-islands']) ) $params['geograph_max_channel-islands'] = 5 ;
if ( !isset($params['forarticles']) ) $params['forarticles'] = 'noimage' ;
if ( !isset($params['esp_max']) ) $params['esp_max'] = 5 ;
if ( !isset($params['ab_max']) ) $params['ab_max'] = 5 ;
if ( !isset($params['startat']) ) $params['startat'] = '' ;
if ( !isset($params['output_format']) ) $params['output_format'] = 'out_html' ;
if ( $datatype == 'replaceimages' and $params['forarticles'] == 'noimage' ) $params['forarticles'] = 'all' ;
$api_output = $params['output_format'] != 'out_html' ;

$api_data = array (
	'n' => 'result' ,
	'a' => array () ,
	'*' => array ()
) ;
$use_api = false ;
$thumbsize = 75 ;
$note = '' ;
$flickr_key = get_flickr_key() ;
if ( $doit ) $db = openDB ( $language , $project ) ;

if ( $doit and $api_output ) {
	$search_url = apiurl ( get_search_url() ) ;
	$api_data['a']['run_url'] = $search_url ;
} else {
	print_form () ;
}

$is_on_toolserver = false ;

if ( $doit ) {
	$namespaces = $wq->get_namespaces() ;

  $imagetypes = array () ;
  if ( isset ( $params['jpeg'] ) ) { $imagetypes['jpg'] = 1 ; $imagetypes['jpeg'] = 1 ; }
  if ( isset ( $params['tiff'] ) ) { $imagetypes['tif'] = 1 ; $imagetypes['tiff'] = 1 ; }
  if ( isset ( $params['png'] ) ) $imagetypes['png'] = 1 ;
  if ( isset ( $params['gif'] ) ) $imagetypes['gif'] = 1 ;
  if ( isset ( $params['svg'] ) ) $imagetypes['svg'] = 1 ;
  if ( isset ( $params['ogg'] ) ) $imagetypes['ogg'] = 1 ;

  $check_exists = false ; # TODO : make this a parameter

	if ( $api_output ) {
		get_article_list () ;
		foreach ( $articles AS $article ) {
			$api_data['*'][] = find_images ( $article ) ;
		}
	} else {
		print "Copy the URL of <a href=\"" . get_search_url() . "\">this link</a> to get a pre-filled search for the values you have entered above.<br/>" ;
		print "Retrieving article list ... " ; myflush() ;
		get_article_list () ;
		print count ( $articles ) . " articles found. Searching for images will take time, please be patient...<br/>" ;
		myflush() ;
		
		print "<table style='border:1px solid black' cellpadding='2px' cellspacing='0px' width='100%'>" ;
		foreach ( $articles AS $article ) {
			find_images ( $article ) ;
		}
		
		print "</table>" ;
		print "<hr/>All done!" ;

		$mt2 = microtime(true); print ' (' . ($mt2-$mt) . 'sec)' ;
	}
}


if ( $doit and $api_output ) {
	if ( $params['output_format'] == 'out_json' ) {
		header('Content-type: application/x-json; charset=utf-8');
		$callback = get_request ( 'callback' , '' ) ;
		if ( $callback != '' ) print $callback . "(" ;
		print json_encode ( $api_data ) ;
		if ( $callback != '' ) print ");" ;
	} else if ( $params['output_format'] == 'out_wiki' ) {
		header('Content-type: text/plain; charset=utf-8');
		print api2wiki ( $api_data ) ;
	} else { // XML; default API output
		header('Content-type: text/xml; charset=utf-8');
		print "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" ;
		print api2xml ( $api_data ) ;
	}
} else {
	print "</div></body></html>" ;
}
myflush() ;

if ( $doit ) {
  require_once ( "php/ToolforgeCommon.php" ) ;
  $tfc = new ToolforgeCommon('fist') ;
  $tfc->logToolUse('','run') ;
}

?>
