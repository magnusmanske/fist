var use_sparql_editor = true ;
var sparql_editor ;
var tt = {} ;
var userinfo = {} ;

$.fn.is_on_screen = function(){
    var win = $(window);
    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();
 
    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();
 
    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
};




var fist = {

	items_per_page : 50 ,
	thumbsize : 180 ,
	max_ll : 5 ,
	widar_url : 'query.php?' ,
	icons : {} ,
	params : {} ,
	multi_count : {} ,
	multi_count_limit : 5 ,
	testing : false ,
	use_petscan_as_api : true ,
	widar_user : '' ,
	
	image_properties : {
		'P18' : 'image' ,
		'P109' : 'signature' ,
		'P41' : 'flag' ,
		'P94' : 'coat&nbsp;of&nbsp;arms' ,
		'P181' : 'range&nbsp;map' ,
		'P242' : 'map' ,
		'P1442' : 'grave&nbsp;picture' ,
		'P1766' : 'place&nbsp;name&nbsp;sign' ,
		'P1801' : 'Commemorative&nbsp;plaque&nbsp;image' ,
		'P1846' : 'distribution map' ,
		'P3451' : 'nighttime view' ,
		'P154' : 'logo' ,
		'P158' : 'seal image' ,
		'P2716' : 'collage image' ,
		'P2713' : 'sectional view' ,
		'P2910' : 'icon' ,
		'P948' : 'wikivoyage banner' ,
		'P4004' : 'shield image' ,
		'P3383' : 'film poster' ,
		'P4640' : 'photosphere image' ,
		'P4291' : 'panorama view' ,
		'P5252' : 'winter view' ,
		'P5775' : 'image of interior' ,
		'P5555' : 'schematic' ,
		'P5962' : 'sail emblem',
		'P4004' : 'shield-only image',
		'P6802' : 'related image'
	} ,
	
	multimedia_properties : {
		'P51' : 'audio' ,
		'P989' : 'spoken text' ,
		'P990' : 'voice recording' ,
		'P443' : 'pronunciation audio' ,
		'P10' : 'video' ,
		'P4896' : '3D model'
	} ,

	init : function ( callback ) {
		var self = this ;
		self.wd = new WikiData ;
		self.params = self.getUrlVars() ;
		if ( undefined !== callback ) callback() ;

/*		$.get ( './icons.txt?'+Math.random() , function ( d ) { // random() to avoid caching
			$.each ( d.split("\n") , function ( dummy , row ) {
				if ( row == '' ) return ;
				var t = row.split("\t") ;
				if ( undefined === self.icons[t[0]] ) self.icons[t[0]] = {} ;
				self.icons[t[0]][t[1]] = t[2] ;
			} ) ;
			if ( undefined !== callback ) callback() ;
		} ) ;*/
	} ,
	
	updatePermalink : function () {
		var self = this ;
		var url = [] ;
		$.each ( ['wdq','category','depth','language','project','page_with_links','popular_images','pagepile','psid','wiki_candidates','sparql'] , function ( k , v ) {
			if ( self[v] != '' ) url.push ( v+"="+(self[v]+'').replace(/\n/g,' ').replace(/\[/g,'%5b').replace(/\]/g,'%5d') ) ;
		} ) ;
		if ( $('#no_images_only').is(':checked') ) url.push('no_images_only=1') ;
		if ( !$('#search_language_links').is(':checked') ) url.push('no_language_links=1') ;
		if ( $('#search_commons').is(':checked') ) url.push('search_commons=1') ;
		if ( $('#search_by_coordinates').is(':checked') ) url.push('search_by_coordinates=1') ;
		if ( $('#search_by_commons_category').is(':checked') ) url.push('search_by_commons_category=1') ;
		if ( $('#jpeg_only').is(':checked') ) url.push('jpeg_only=1') ;
		if ( $('#keep_svg').is(':checked') ) url.push('keep_svg=1') ;
		if ( $('#page_image_only').is(':checked') ) url.push('page_image_only=1') ;
		if ( $('#remove_used').is(':checked') ) url.push('remove_used=1') ;
		if ( $('#remove_multiple').is(':checked') ) url.push('remove_multiple=1') ;
		if ( tt.language != 'en' ) url.push('interface_language='+tt.language) ;
		url.push('prefilled=1') ;
		
		url = '?' + url.join('&') ;
		$('#permalink').attr ( { href:url } ) . show() ;
	} ,

	runTool : function () {
		var self = this ;
		if ( self.use_petscan_as_api ) self.runToolPetScan() ;
		else self.runToolLocal() ;
	} ,

	updateInternalValues : function () {
		var self = this ;
		self.wdq = $('#wdq').val() ;
		if ( use_sparql_editor ) self.sparql = sparql_editor.getValue() ;
		else self.sparql = $('#sparql').val() ;
		self.category = $('#category').val() ;
		self.depth = 1*$('#depth').val() ;
		self.language = $('#language').val() ;
		self.project = $('#project').val() ;
		self.itemlist = $('#itemlist').val() ;
		self.popular_images = $('#popular_images').val() ;
		self.pagepile = $('#pagepile').val() ;
		self.psid = $('#psid').val() ;
		self.wiki_candidates = $('#wiki_candidates').val() ;
		self.page_with_links = $('#page_with_links').val() ;
		self.q_ll = {} ;
		self.updatePermalink() ;
	} ,

	logEvent : function ( method ) {
		$.getJSON ( 'https://tools.wmflabs.org/magnustools/logger.php?tool=wdfist&method='+method+'&callback=?' , function(j){} ) ;
	} ,

	runToolPetScan : function () {
		var self = this ;
		self.updateInternalValues() ;
		
		var params = { wdf_main:1 , doit:1 , format:'json' } ;
		if ( self.wdq != '' ) {
			return self.runToolLocal() ; // Fallback
		} else if ( self.sparql != '' ) {
			params.sparql = self.sparql ;
		} else if ( self.page_with_links != '' ) {
			return self.runToolLocal() ; // Fallback
		} else if ( self.category != '' ) {
			params.categories = $.trim(self.category.replace(/[\r\n]+$/,'')) ;
			params.depth = self.depth ;
			params.language = self.language ;
			params.project = self.project ;
		} else if ( self.itemlist != '' ) {
			params.manual_list = self.itemlist ;
			params.manual_list_wiki = 'wikidatawiki' ;
		} else if ( self.popular_images != '' ) {
			return self.runToolLocal() ; // Fallback
		} else if ( self.pagepile != '' ) {
			params.pagepile = self.pagepile ;
		} else if ( self.psid != '' ) {
			params.psid = self.psid ;
		} else if ( self.wiki_candidates != '' ) {
			return self.runToolLocal() ; // Fallback
		} else if ( fist.params['prefilled'] ) {
			alert ( tt.t('nothing2work_with') ) ;
			return ;
		} else {
			return ;
		}
		
		if ( $('#no_images_only').is(':checked') ) params.wdf_only_items_without_p18 = 1 ;
		if ( $('#search_language_links').is(':checked') ) params.wdf_langlinks = 1 ;
		if ( $('#search_commons').is(':checked') ) params.wdf_search_commons = 1 ;
		if ( $('#search_by_coordinates').is(':checked') ) params.wdf_coords = 1 ;
		if ( $('#search_by_commons_category').is(':checked') ) params.wdf_commons_cats = 1 ;
		if ( $('#page_image_only').is(':checked') ) params.wdf_only_page_images = 1 ;
		if ( $('#jpeg_only').is(':checked') ) params.wdf_only_jpeg = 1 ;
		if ( $('#keep_svg').is(':checked') ) params.wdf_allow_svg = 1 ;
		if ( $('#remove_used').is(':checked') ) params.wdf_only_files_not_on_wd = 1 ;
		if ( $('#remove_multiple').is(':checked') ) params.wdf_max_five_results = 1 ;
		
		
		$('#loading1').html('<span tt="loading_candidates"></span>').removeClass('label-success').addClass('label-info') ;
		tt.updateInterface ( $('#loading1') ) ;
		$('#loading').show() ;
		self.logEvent('run');
		$.getJSON ( 'https://petscan.wmflabs.org/?callback=?' , params , function ( d ) {
			if ( typeof d.error != 'undefined' ) {
				alert(d.error);
				return;
			}
			$('#loading1').html('<span tt="candidates_loaded"></span>').removeClass('label-info').addClass('label-success') ;
			tt.updateInterface ( $('#loading1') ) ;
			if ( d.status != "OK" ) {
				alert ( "E0:" + d.status ) ;
				return ;
			}
			if ( typeof d.data == 'undefined' ) {
				$('#results').html("<span tt='no_files_found'></span>").show() ;
				tt.updateInterface ( $('#results') ) ;
			}
			self.data = d ;
			self.q_ll = d.q_ll ;
			
			self.multi_count = {} ;
			$.each ( self.data.data , function ( q , imagelist ) {
				$.each ( imagelist , function ( image , count ) {
					if ( typeof self.multi_count[image] == 'undefined' ) self.multi_count[image] = 0 ;
					self.multi_count[image]++ ;
				} ) ;
			} ) ;
			$.each ( self.multi_count , function ( image , cnt ) {
				if ( cnt < self.multi_count_limit ) delete self.multi_count[image] ;
			} ) ;

			self.showData ( 0 ) ;

		} )   .fail(function(xhr, textStatus, err) {
				console.log("readyState: " + xhr.readyState);
			    console.log("responseText: "+ xhr.responseText);
			    console.log("status: " + xhr.status);
			    console.log("text status: " + textStatus);
			    console.log("error: " + err);
    			alert( tt.t('query_failed') );
		});
	} ,

	runToolLocal : function () {
		var self = this ;
		self.updateInternalValues() ;
		
		var params = {} ;
		if ( self.wdq != '' ) {
			params = { action : 'wdq' , q : self.wdq } ;
		} else if ( self.sparql != '' ) {
			params = { action : 'sparql' , sparql : self.sparql } ;
		} else if ( self.page_with_links != '' ) {
			params = {
				action:'page_with_links',
				page_with_links:self.page_with_links,
				lang:self.language,
				project:self.project
			} ;
		} else if ( self.category != '' ) {
			params = {
				action:'from_cat',
				category:$.trim(self.category.replace(/[\r\n]+$/,'')),
				depth:self.depth,
				lang:self.language,
				project:self.project
			} ;
		} else if ( self.itemlist != '' ) {
			params = {
				action:'itemlist',
				itemlist:self.itemlist
			} ;
		} else if ( self.popular_images != '' ) {
			params = {
				action:'popular_images',
				number:self.popular_images
			} ;
		} else if ( self.pagepile != '' ) {
			params = {
				action:'pagepile',
				pagepile:self.pagepile
			} ;
		} else if ( self.psid != '' ) {
			params = {
				action:'psid',
				psid:self.psid
			} ;
		} else if ( self.wiki_candidates != '' ) {
			params = {
				action:'wiki_candidates',
				wiki_candidates:self.wiki_candidates
			} ;
		} else if ( fist.params['prefilled'] ) {
			alert ( tt.t('nothing2work_with') ) ;
			return ;
		} else {
			return ;
		}
		
		if ( $('#no_images_only').is(':checked') ) params.no_images_only = 1 ;
		if ( !$('#search_language_links').is(':checked') ) params.no_language_links = 1 ;
		if ( $('#search_commons').is(':checked') ) params.search_commons = 1 ;
		if ( $('#search_by_coordinates').is(':checked') ) params.search_by_coordinates = 1 ;
		if ( $('#search_by_commons_category').is(':checked') ) params.search_by_commons_category = 1 ;
		if ( $('#page_image_only').is(':checked') ) params.page_image_only = 1 ;
		if ( $('#jpeg_only').is(':checked') ) params.jpeg_only = 1 ;
		if ( $('#keep_svg').is(':checked') ) params.keep_svg = 1 ;
		if ( $('#remove_used').is(':checked') ) params.remove_used = 1 ;
		if ( $('#remove_multiple').is(':checked') ) params.remove_multiple = 1 ;
		
		
		$('#loading1').html('<span tt="loading_candidates"></span>').removeClass('label-success').addClass('label-info') ;
		tt.updateInterface ( $('#loading1') ) ;
		$('#loading').show() ;
		self.logEvent('run');
		$.post ( 'query.php' , params , function ( d ) {
			$('#loading1').html('<span tt="candidates_loaded"></span>').removeClass('label-info').addClass('label-success') ;
			tt.updateInterface ( $('#loading1') ) ;
			if ( d.status != "OK" ) {
				alert ( "E0:" + d.status ) ;
				return ;
			}
			if ( typeof d.data == 'undefined' ) {
				$('#results').html("<span tt='no_files_found'></span>").show() ;
				tt.updateInterface ( $('#results') ) ;
			}
			self.data = d ;
			self.q_ll = d.q_ll ;
			
			self.multi_count = {} ;
			$.each ( self.data.data , function ( q , imagelist ) {
				$.each ( imagelist , function ( image , count ) {
					if ( typeof self.multi_count[image] == 'undefined' ) self.multi_count[image] = 0 ;
					self.multi_count[image]++ ;
				} ) ;
			} ) ;
			$.each ( self.multi_count , function ( image , cnt ) {
				if ( cnt < self.multi_count_limit ) delete self.multi_count[image] ;
			} ) ;

			self.showData ( 0 ) ;

		} , 'json' )   .fail(function() {
			alert( tt.t('query_failed') );
		});
	} ,
	
	fillRandom : function () {
		$('#loading1').html('<span tt="loading_random"></span>').removeClass('label-success').addClass('label-info') ;
		tt.updateInterface ( $('#loading1') ) ;
		$('#loading').show() ;
		$.post ( 'query.php' , {action:'random'} , function ( d ) {
			$('#loading').hide() ;
			$('#itemlist').val ( d.data.join('\n') ) ;
			$('#b_run').click() ;
		} ) ;
	} ,
	
	getNavbox : function ( is_top ) {
		var self = this ;
		var d = self.data ;
		var max = 0 ;
		$.each ( d.data , function(){max++} ) ;
		if ( max <= self.items_per_page ) return '' ;
		var h = "<div style='text-align:center;border-"+(is_top?'bottom':'top')+":1px solid #DDD;margin-top:10px;margin-bottom:10px;max-height:250px;overflow:auto;'>" ;
		if ( is_top ) h += "<a name='top'></a>" ;
		for ( var p = 0 ; p <= max ; p += self.items_per_page ) {
			if ( p > 0 ) h += " | " ;
			h += (p==self.offset) ? '<b>' : '<a href="#" onclick="fist.showData('+p+');return false">' ;
			h += (p+1) + "&ndash;" + (p+self.items_per_page>max?max:p+self.items_per_page) ;
			h += (p==self.offset) ? '</b>' : '</a>' ;
		}
		h += "</div>" ;
		return h ;
	} ,
	
	showData : function ( np ) {
		var self = this ;
		self.offset = np ;
		var d = self.data ;
		var mobile = isMobile() ;
		var html = "" ;
		html += self.getNavbox(true) ;
		html += "<table class='table-condensed table-striped'><tbody>" ;
		var qs = [] ;
		self.q2i = {} ;
		var cur = -1 ;

		$.each ( d.data , function ( q , images ) {
			q = q.replace(/\D/g,'') ;
			cur++ ;
			if ( cur >= self.offset + self.items_per_page ) return false ;
			if ( cur < self.offset ) return ;
			var total = 0 ;
			$.each ( images , function ( image , count ) { total+=count } ) ;
			if ( total == 0 ) return ; // If all files are ignored
			q = 'Q'+q ;
			qs.push ( q ) ;
			var h = '' ;
			h += "<tr class='itemrow'>" ;
			h += "<td class='itemcell' id='item_"+q+"' id='" + q + "'>" ;
			h += "<div><a target='_blank' class='item_link' href='//www.wikidata.org/wiki/" + q + "'>" + q + "</a>&nbsp;" ;
			h += "<a tt_title='reasonator' target='_blank' href='http://tools.wmflabs.org/reasonator/?&q=" + q + "'><img src='//upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Reasonator_logo_proposal.png/16px-Reasonator_logo_proposal.png' border='0'></a>" ;
			if ( mobile ) {
				h += "<br/><a href='#' class='ignore_files'><span class='label label-warning' tt='block_files4item'></span></a>" ;
			} else {
				h += "&nbsp;<a href='#' class='ignore_files' tt_title='block_forver'><span class='badge badge-pill badge-warning'>&times;</span></a>" ;
			}


			// Display direct links to Wikipedia pages
			var qnum = q.replace(/\D/g,'') ;
			if ( typeof self.q_ll != 'undefined' && typeof self.q_ll[qnum] != 'undefined' ) {
				var ll = [] ;
				$.each ( self.q_ll[qnum].split(/\|/) , function ( k0 , v0 ) {
					var m = decodeURIComponent(v0).match(/^(.+?)wiki:(.+)$/) ;
					if ( m == null ) return ;
					var lang = m[1] ;
					if ( lang == 'commons' ) return ;
					var url = "https://"+lang+".wikipedia.org/wiki/"+escattr(encodeURIComponent(m[2])) ;
					ll.push ( "<a target='_blank' href='"+url+"'>"+lang+"</a>" ) ;
				} ) ;
				if ( ll.length > self.max_ll ) {
					h += "<div style='display:inline-block;margin-left:5px;font-size:8pt;'> >" + self.max_ll + " </div>" ;
				} else if ( ll.length > 0 ) {
					ll.sort() ;
					h += "<div style='display:inline-block;margin-left:5px;font-size:8pt;'>" ;
					h += ll.join('|') ;
					h += "</div>" ;
				}
			}

			h += "</div>" ;
			
			h += "<div class='wikidata_images'></div>" ; // TODO add existing ones
			h += "</td>" ;
			h += "<td style='min-width:780px'>" ;
			h += "<ul class='thumbrow' style='max-height:900px;overflow:auto'>" ;
			self.q2i[q] = [] ;
			var imagecount = 0 ;
			$.each ( images , function ( image , count ) {
				if ( count == 0 ) return ;
				if ( typeof self.multi_count[image] != 'undefined' ) return ; // Occurs too often in result set
				self.q2i[q].push ( image ) ;
				h += self.getWikiThumb ( q , self.q2i[q].length-1 ) ;
				imagecount++ ;
			} ) ;
			if ( imagecount == 0 ) return ; // No images to show
			h += "</ul></td>" ;
			h += "</tr>" ;
			html += h ;
		} ) ;
		html += "</tbody></table>" ;
		html += self.getNavbox(false) ;

		html += "<hr/>" ;
		html += '<div class="row" style="text-align:center;margin-bottom:20px"><div><button class="btn btn-outline-primary" onclick="window.location.hash=\'#results\';fist.runTool();return false" tt="run_again"></button></div></div>' ; // $(\'#results table\').remove();
		
		$('#results').html(html).show() ;
		tt.updateInterface ( $('#results') ) ;

		if ( mobile ) {
			$('div.mythumb').css ( { 'margin-right':'130px' } ) ;
			$('div.thumbtools').show() ;
		}
		
		
		$('a.ignore_files').click ( function () {
			var a = $(this) ;
			var tr = $(a.parents('tr.itemrow').get(0)) ;
			var ul = $(tr.find('ul.thumbrow')) ;
			var q ;
			var files = [] ;
			$.each ( ul.find('li div.mythumb') , function ( k , v ) {
				var o = $(v) ;
				q = o.attr('q') ;
				files.push ( o.attr('file') ) ;
			} ) ;
			if ( typeof q == 'undefined' ) return false ; // No files to process
			self.ignoreFiles ( q , files , tr ) ;
			return false ;
		} ) ;
		
		
		$('#loading2').html('<span tt="loading_labels"></span>').removeClass('label-success').addClass('label-info') ;
		tt.updateInterface ( $('#loading2') ) ;
		self.wd.getItemBatch ( qs , function () {
			cnt = 0 ;
			$.each ( qs , function ( dummy , q ) {
				cnt++ ;
				$('#item_'+q+' a.item_link').html ( self.wd.items[q].getLabel() ) ;
				if ( self.wd.items[q].hasClaims('P225') ) { // Taxon name
					var tn = self.wd.items[q].getStringsForProperty('P225') ;
					if ( tn[0] != self.wd.items[q].getLabel() ) {
						var commons_search_url = "https://commons.wikimedia.org/w/index.php?search=%22" + encodeURIComponent(tn[0]) + "%22&title=Special:Search&profile=advanced&fulltext=1&ns6=1" ;
						$('#item_'+q+' a.item_link').parent().append ( '<div style="font-size:8pt"><i><a href="'+commons_search_url+'" target="_blank">'+tn[0]+'</a></i></div>' ) ;
					}
				}
				self.addExistingImages ( q ) ;
			} ) ;
			
			if ( 1 ) { // Remove existing duplicates
				$("div.wikidata_images div.mythumb").each ( function(){$("li div.mythumb[file='"+escattr($(this).attr("file"))+"']").parent().remove()});
				$('tr.itemrow').each ( function () {
					var row = $(this) ;
					if ( row.find('ul.thumbrow li').length == 0 ) row.hide() ;
				} ) ;
				$(window).scroll();
			}
			
			$('#loading2').html('<span tt="labels_loaded"></span>').removeClass('label-info').addClass('label-success') ;
			tt.updateInterface ( $('#loading2') ) ;
			$(window).scroll();
			if ( cnt == 0 ) {
				$('#results').html ( "<i tt='no_files_found'></i>" ) ;
				tt.updateInterface ( $('#results') ) ;
			}
		} ) ;

		if ( np > 0 ) window.location.hash = '#top' ;
		$(window).scroll();
		$('ul.thumbrow li').hover ( function () {
			$($(this).find('div.thumbtools')).show() ;
		} , function () {
			if ( isMobile() ) return ;
			$($(this).find('div.thumbtools')).hide() ;
		} ) ;
	} ,
	
	ignoreFiles : function ( q , files , tr ) {
		var self = this ;
		if ( self.widar_user == '' ) {
			$.get ( self.widar_url+'action=get_rights&botmode=1' , function ( d ) {
				self.widar_user = d.result.query.userinfo.name ;
				self.ignoreFiles ( q , files , tr ) ;
			} , 'json' ) ;
			return ;
		}
//		console.log ( q , files , self.widar_user ) ;
		$.post ( 'query.php' , {
			action:'ignore_files',
			q:q,
			files:files.join('|') ,
			user:self.widar_user
		} , function ( d ) {
//			console.log ( "Files ignored." , d.status ) ;
		} ) ;
		tr.remove() ;
		$(window).scroll();
	} ,
	
	addExistingImages : function ( q ) {
		var self = this ;
		var i = self.wd.items[q] ;
		var candidates = $.extend ( {} , self.image_properties , self.multimedia_properties ) ;
		$.each ( candidates , function ( prop , text ) {
			var files = i.getMultimediaFilesForProperty ( prop ) ;
			$.each ( files , function ( dummy , file ) {
				var h = self.getWikiThumb ( 'dummy' , 'dummy' , { exists:true , file:file , title:text } ) ;
				h = "<div style='display:inline'>" + h + "</div>" ;
				$('#item_'+q+' div.wikidata_images').append ( h ) ;
				tt.updateInterface( $('#item_'+q+' div.wikidata_images') ) ;
			} ) ;
		} ) ;
	} ,

	getWikiThumb : function ( q , i , o ) {
		var self = this ;
		if ( o === undefined ) o = {exists:false} ;
		var file = o.file || self.q2i[q][i] ;
		var pk = 'commons.wikimedia' ;
		if ( !o.exists && undefined !== self.icons[file] ) return '' ; // ICON
		var h = '' ;
		if ( !o.exists ) h += "<li>" ;

		h += "<div class='mythumb' q='"+q+"' i='"+i+"' file='"+escattr(file.replace(/ /g,'_'))+"'>" ;

		if ( !o.exists ) {
			h += "<div class='thumbtools' style='width:120px'>" ;
			h += "<button style='margin-bottom:10px' class='btn btn-sm btn-outline-success' onclick='fist.addToWikidata(this,\"P18\");return false' tt='add_as_image'></button>" ;
			h += "<button style='margin-bottom:10px' class='btn btn-sm btn-outline-info' onclick='fist.addImageAs(this);return false' tt='add_as'></button>" ;
			h += "<button style='margin-bottom:10px' class='btn btn-sm btn-outline-danger' onclick='fist.isAnIcon(this);return false' tt='block_image'></button>" ;
			h += "<button class='btn btn-sm btn-outline-warning' onclick='fist.cropImage(this);return false' tt='crop_notice'></button>" ;
			h += "<div style='margin-top:0.5rem'><button class='btn btn-sm btn-outline-secondary' onclick='fist.p180_normal(this);return false' tt='depicts_normal' style='padding:1px'></button>" ;
			h += "<button class='btn btn-sm btn-outline-secondary' onclick='fist.p180_prominent(this);return false' tt='depicts_prominent' style='padding:1px;margin-left:2px;'></button></div>" ;
			h += "</div>" ;
		}

		h += "<span class='thumb_inner'>" ;
		h += "<a target='_blank' href='//commons.wikimedia.org/wiki/File:" + escattr(encodeURIComponent(file)) + "'>" ;
		h += "<img border=0 class='pre_thumb' " ;
		if ( o.title !== undefined ) h += " title='" + escattr(o.title) + "'" ;
		h += "/>" ;
		h += "</a></span></div>" ;
		h += "<br/><div class='thumblabel'>" ;
		h += "<a target='_blank' href='//commons.wikimedia.org/wiki/File:" + escattr(encodeURIComponent(file)) + "' title='" + escattr(file.replace(/_/g,' ')) + "'>" ;
		h += file.replace(/_/g,' ').substr(0,30) ;
		h += "</a></div>" ;


		if ( !o.exists ) h += "</li>" ;
		return h ;
	} ,
	
	cropImage : function ( me ) {
		var self = this ;
		me = $($($(me).parents('div.mythumb')).get(0)) ;
		var q = me.attr('q') ;
		var i = me.attr('i') ;
		var file = self.q2i[q][i] ;

		self.widarRunning ( true , tt.t('crop_note') ) ;
		file = file.replace(/\s/g,'_') ;
		
		var note = "\n\n{{Crop for Wikidata|"+q+'}}' ;
		
//		alert ( note ) ; return ;

		$.post ( 'query.php' , {
			action:'crop_file',
			file:file
		} , function ( d ) {
//			console.log ( "Files ignored." , d.status ) ;
		} ) ;

		function addCropNote () {
			$.get ( self.widar_url , {
				botmode:1,
				tool_hashtag:'wdfist',
				action:'append',
				language:'commons',
				project:'wikimedia',
				page:'File:'+file,
				text:note
			} , function ( d ) {
				self.widarRunning ( false ) ;
				if ( d.error.match ( /invalid token/i ) || d.error.match ( /logged in/i ) ) { // Stupid broken OAuth
					addCropNote() ; // Again
				} else if ( d.error != 'OK' ) alert ( "ERROR : "+d.error ) ;
				else {
					$($(me.parents('li')).get(0)).remove() ;
					self.icons[file] = 1 ;
					$('div.mythumb[file="'+escattr(file)+'"]').each ( function () {
						$($($(this).parents('li')).get(0)).remove() ;
					} ) ;
/*					$('tr.itemrow').each ( function () {
						var row = $(this) ;
						if ( row.find('ul.thumbrow li').length == 0 ) row.hide() ;
					} ) ;*/
					$(window).scroll();
					self.logEvent('crop notice');
				}
			} , 'json' ) ;
		}

		addCropNote() ;
	} ,

	isAnIcon : function ( me ) {
		var self = this ;
		me = $($($(me).parents('div.mythumb')).get(0)) ;
		var q = me.attr('q') ;
		var i = me.attr('i') ;
		var file = self.q2i[q][i] ;
		
		var cs = tt.t('permanent_warning').replace('$1',file) ;
		if ( !confirm(cs) ) return ;

		self.widarRunning ( true , tt.t('blocking_image') ) ;
		file = file.replace(/\s/g,'_') ;
		
		function addIcon () {
			$.get ( self.widar_url , {
				botmode:1,
				tool_hashtag:'wdfist',
				action:'add_row',
				page:'User:Magnus_Manske/FIST_icons',
				row:"* "+file
			} , function ( d ) {
				self.widarRunning ( false ) ;
				if ( d.error.match ( /invalid token/i ) || d.error.match ( /logged in/i ) ) { // Stupid broken OAuth
					addIcon() ; // Again
				} else if ( d.error != 'OK' ) alert ( "ERROR : "+d.error ) ;
				else {
					$($(me.parents('li')).get(0)).remove() ;
					self.icons[file] = 1 ;
					$('div.mythumb[file="'+escattr(file)+'"]').each ( function () {
						$($($(this).parents('li')).get(0)).remove() ;
					} ) ;
					$('tr.itemrow').each ( function () {
						var row = $(this) ;
						if ( row.find('ul.thumbrow li').length == 0 ) row.hide() ;
					} ) ;
					$(window).scroll();
					self.logEvent('mark as icon');
				}
			} , 'json' ) ;
		}
		
		addIcon() ;
	} ,

	addImageAs : function ( me ) {
		var self = fist ;
		
		var h = '<div class="modal hide fade" id="image_as_dialog"><div class="modal-dialog" role="document"><div class="modal-content">' ;
		h += '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' ;
		h += '<h3 tt="add_image_as"></h3></div>' ;
		h += '<div class="modal-body" style="max-height:550px;overflow-y:auto">' ;

		h += "<table class='table table-condensed'><tbody><tr>" ;
		h += "<td style='vertical-align:top'><h3 tt='add_as'></h3>" ;
		$.each ( self.image_properties , function ( prop , text ) {
			var id = 'iad_button_' + prop ;
			h += "<p><button class='btn btn-secondary' id='"+id+"'>"+ucFirst(text)+"</button></p>" ;
		} ) ;
		h += "</td>" ;

		h += "<td style='vertical-align:top'><h3 tt='multimedia'></h3>" ;
		$.each ( self.multimedia_properties , function ( prop , text ) {
			var id = 'iad_button_' + prop ;
			h += "<p><button class='btn btn-secondary' id='"+id+"'>"+ucFirst(text)+"</button></p>" ;
		} ) ;
		h += "</td></tr></tbody></table>" ;

		h += '</div></div></div></div>' ;
		
		var candidates = $.extend ( {} , self.image_properties , self.multimedia_properties ) ;
		$('#image_as_dialog').remove() ;
		$('body').append ( h ) ;
		$.each ( candidates , function ( prop , text ) {
			var id = 'iad_button_' + prop ;
			$('#'+id).click ( function () {
				$('#image_as_dialog').modal ( 'hide' ) ;
				self.addToWikidata ( me , prop ) ;
			} ) ;
		} ) ;

		$('#image_as_dialog').modal ( { show:true , backdrop:true , keyboard:true } ) ;
		tt.updateInterface ( $('#image_as_dialog') ) ;
		
	} ,

	p180_normal : function ( element ) {
		this.add_sdc ( element , 'P180' , 'normal' ) ;
	} ,

	p180_prominent : function ( element ) {
		this.add_sdc ( element , 'P180' , 'preferred' ) ;
	} ,

	add_sdc : function ( element , property , rank ) {
		let self = this ;
		element = $($($(element).parents('div.mythumb')).get(0)) ;
		let q = element.attr('q') ;
		let i = element.attr('i') ;
		let filename = 'File:' + self.q2i[q][i] ;
		console.log ( q , i , filename , property , rank ) ;

		flickr2commons.get_sdc_id_from_name ( filename , function ( sdc_id ) {
			if ( typeof sdc_id == 'undefined' ) {
				//self.error = "Cannot get file ID" ;
				return ;
			}
			// console.log ( "MID: "+sdc_id)
			flickr2commons.check_statement ( sdc_id , property , q , function ( has_statement ) {
				// console.log("Checked: ",has_statement);
				if ( has_statement ) {} //self.depicts_is_set = true ;
				else flickr2commons.set_sdc_prop_q_rank ( sdc_id , property , q , rank , function ( d ) {
					// console.log("set_sdc_prop_q_rank:",d);
					if ( d.error == 'OK' ) {
						//self.depicts_is_set = true ;
						self.logEvent('structured data on commons');
					} else {
						alert ( d.error ) ;
					}
				} ) ;
			} )
		} )
	} ,

	addToWikidata : function ( me , prop ) {
		var self = this ;
		me = $($($(me).parents('div.mythumb')).get(0)) ;
		var q = me.attr('q') ;
		var i = me.attr('i') ;
		var file = self.q2i[q][i].replace(/_/g,' ') ;

		self.widarAddString ( q , prop , file , function (x) {
			if ( !x ) return ;
			
			$('#item_'+q+' div.wikidata_images').append ( $(me.find('span.thumb_inner')).html() ) ;
			$($(me.parents('li')).get(0)).remove() ;
			$(window).scroll();
			self.logEvent('add to wikidata');
		} ) ;
	} ,
	
	widarRunning : function ( state , label ) {
		if ( state ) {
			if ( label === undefined ) label = "Adding image..." ; // TODO i18n
			var oauth_wait_dialog = "<div id='oauth_wait_dialog' style='display:none;width: 200px; position: fixed; top: 200px; background-color: #99C7FF; color: white; text-align: center; left: 50%; margin-left: -100px; padding: 5px; border:10px solid #DDD'>"+label+"</div>" ;
			$('#oauth_wait_dialog').remove() ;
			$('body').append ( oauth_wait_dialog ) ;
			$('#oauth_wait_dialog').fadeIn(200) ;
		} else {
			$('#oauth_wait_dialog').fadeOut(200) ;
		}
	} ,

	widarAddString : function ( q , prop , s , callback ) {
		var self = this ;
		self.widarRunning ( true ) ;

		$.post ( 'query.php' , {
			action:'set_file',
			file:s
		} , function ( d ) {
//			console.log ( "Files ignored." , d.status ) ;
		} ) ;

		$.get ( self.widar_url , {
			action:'set_string',
			tool_hashtag:'wdfist',
			id:q,
			prop:prop,
			text:s,
			botmode:1
		} , function ( d ) {
			self.widarRunning ( false ) ;
			if ( d.error == 'OK' ) {
				if ( callback ) callback ( true ) ;
			} else if ( d.error.match ( /invalid token/i ) || d.error.match ( /logged in/i ) ) { // Stupid broken OAuth
				self.widarAddString ( q , prop , s , callback ) ;
			} else {
				alert ( "E1:" + d.error ) ;
				if ( callback ) callback ( false ) ;
			}
		} , 'json' ) .fail(function( jqxhr, textStatus, error ) {
			alert ( "E2:" + error ) ;
			if ( callback ) callback ( false ) ;
		} ) ;
	} ,

	
	loadThumb : function ( img ) {
		if ( img.css('display') === 'none' ) return ; // Hacking around Chrome bug; should be: if ( !img.is(':visible') ) return ;
		if ( !img.is_on_screen() ) return ;
		var self = fist ;
		img.removeClass ( 'pre_thumb' ) ;
		var mt = $(img.parents('div.mythumb').get(0)) ;
		var q = mt.attr('q') ;
		var i = mt.attr('i') ;
		var file = mt.attr('file') ;
		if ( file === undefined ) file = self.q2i[q][i] ;
		
		$.getJSON ( '//commons.wikimedia.org/w/api.php?callback=?' , {
			action:'query',
			titles:'File:'+file,
			prop:'imageinfo',
			iiprop:'url',
			iiurlwidth:self.thumbsize,
			iiurlheight:self.thumbsize,
			redirects:'',
			format:'json'
		} , function ( d ) {
			$.each ( (d.query.pages||[]) , function ( id , image ) {
				if ( id < 0 ) return ;
				if ( undefined === image.imageinfo ) return ;
				var ii = image.imageinfo[0] ;

				var turl = ii.thumburl ;
				var top = Math.floor((self.thumbsize-ii.thumbheight)/2) + "px" ;
				img.attr('src',turl).css('margin-top',top) ;
				
				return false ;
			} ) ;
		} ) ;
	} ,

	getUrlVars : function () {
		var vars = {} ;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1) ;
//		var hash = window.location.href.slice(window.location.href.indexOf('#') + 1) ;
//		if ( hash == window.location.href ) hash = '' ;
//		if ( hash.length > 0 ) hashes = hash ;
//		else 
		hashes = hashes.replace ( /#.*$/ , '' ) ;
		hashes = hashes.split('&');
		$.each ( hashes , function ( i , j ) {
			var hash = j.split('=');
			hash[1] += '' ;
			vars[hash[0]] = decodeURIComponent(hash[1]).replace(/_/g,' ');
		} ) ;
		return vars;
	} ,

	fin : ''
} ;

// Load thumbnails on demand
$(window).scroll(function(){
	$('img.pre_thumb').each ( function () {
		fist.loadThumb ( $(this) ) ;
	} ) ;
} ) ;


$(document).ready ( function () {

	if ( isMobile() ) {
		$('#main_content').removeClass('container').addClass('container-fluid') ;
	}
	
	loadMenuBarAndContent ( { toolname : 'WDFIST' , meta : 'WDFIST' , content : 'form.html' , run : function () {

		$('label').css({'font-weight':'normal'}) ;

		$('#toolname').attr ( { tt:'toolname' } ) ;
	
		tt = new ToolTranslation ( {
			tool:'wdfist' ,
			highlight_missing:true,
			onUpdateInterface : function () {
				fist.updatePermalink() ;
				if ( typeof userinfo.name != 'undefined' ) {
					$('#widar_note').html('<span tt="welcome" tt1="'+userinfo.name+'"></span>') ;
					tt.updateInterface ( $('#widar_note') ) ;
				}
			} ,
			callback : function () {

				if ( use_sparql_editor ) { // Initialize SPARQL editor
					sparql_editor = new wikibase.queryService.ui.editor.Editor();
					sparql_editor.fromTextArea( $( '.editor' )[0] );
				}
			
				tt.addILdropdown ( $('#tooltranslate_wrapper') ) ;

				$.get ( 'query.php?action=get_rights&botmode=1' , function ( d ) {
					if ( typeof d.result == 'undefined' ) return ;
					if ( typeof d.result.query == 'undefined' ) return ;
					if ( typeof d.result.query.userinfo == 'undefined' ) return ;
					if ( typeof d.result.query.userinfo.name == 'undefined' ) return ;
					userinfo = d.result.query.userinfo ;
					$('#widar_note').html('<span tt="welcome" tt1="'+d.result.query.userinfo.name+'"></span>') ;
					tt.updateInterface ( $('#widar_note') ) ;
				} , 'json' ) ;

				$('div.span12').removeClass('span12').addClass('col-xs-12') ;

				$('#pagepile_wrapper').show() ;
				wikiDataCache.ensureSiteInfo ( [ { lang:'commons' , project:'wikimedia' } ] , function () {

//					$('#toolname').html ( "<b>W</b>iki<b>D</b>ata <b>F</b>ree <b>I</b>mage <b>S</b>earch <b>T</b>ool" ) ;
					$('#wdq').focus() ;
			
					fist.init ( function () {
			
						if ( typeof fist.params['testing'] != 'undefined' ) fist.testing = true ;
			
						$.each ( ['wdq','language','project','category','depth','itemlist','popular_images','pagepile','psid','wiki_candidates','sparql','page_with_links'] , function ( k , v ) {
							if ( undefined !== fist.params[v] ) {
								if ( fist.params[v] == '{{'+v+'}}' ) return ; // {{wqd}} / {{sparql}}
								$('#'+v).val ( fist.params[v] ) ;
							}
						} ) ;
						$('#wdq').val ( $('#wdq').val().replace(/\+/g,' ') ) ;
						
						
						var sparql = $('#sparql').val().replace(/^\s*\#.*$/gm,' ') ;
						if ( use_sparql_editor ) sparql_editor.setValue ( sparql ) ;
						else $('#sparql').val ( sparql ) ;
				
						if ( fist.params['prefilled'] ) {
							if ( !fist.params['no_images_only'] ) $('#no_images_only').attr('checked', false);
						}
						if ( fist.params['no_language_links'] ) $('#search_language_links').prop('checked', false);
						if ( fist.params['search_commons'] ) $('#search_commons').prop('checked', true);
						if ( fist.params['search_by_coordinates'] ) $('#search_by_coordinates').prop('checked', true);
						if ( fist.params['search_by_commons_category'] ) $('#search_by_commons_category').prop('checked', true);
						if ( fist.params['jpeg_only'] ) $('#jpeg_only').prop('checked', true);
						if ( fist.params['keep_svg'] ) $('#keep_svg').prop('checked', true);
						if ( fist.params['page_image_only'] ) $('#page_image_only').prop('checked', true);
						if ( fist.params['remove_used'] ) $('#remove_used').prop('checked', true);
						if ( fist.params['remove_multiple'] ) $('#remove_multiple').prop('checked', true);
				
			
						if ( $('#wdq').val() != '' || $('#category').val() != '' || $('#category').val() != 'page_with_links' ) {
							flickr2commons.oauth_uploader_base = '/oauth_uploader.php' ;
							flickr2commons.oauth_uploader_api = '/oauth_uploader.php?botmode=1&callback=?' ;
							fist.runTool() ;
							return ;
						}

					} ) ;
			
				} ) ;

			}
		} ) ;

	} } ) ;
} ) ;
