<?PHP

/*
Wikidata items with Commons category but no image,
skipping obvious bogus (years etc.)

THIS IS ANCIENT AND BROKEN

Should use:
SELECT ?q {
  VALUES ?bad_instance { wd:Q577 wd:Q4167836 wd:Q13406463 wd:Q4167836 }
  ?q wdt:P373 [] .
  MINUS { ?q wdt:P18 [] }
  MINUS { ?q wdt:P31 wd:Q577 }
  MINUS { ?q wdt:P31 wd:Q4167836 }
  MINUS { ?q wdt:P31 wd:Q13406463 }
  MINUS { ?q wdt:P31 wd:Q4167836 }
  }


*/
exit(0) ;

ini_set('memory_limit','500M');
//error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
//ini_set('display_errors', 'On');
@set_time_limit ( 15*60 ) ; # Time limit 45min

include_once ( "php/common.php" ) ;

$batch_size = 50 ;

// 

$d = json_decode ( file_get_contents ( $wdq_internal_url."?q=" . urlencode("claim[373] and noclaim[18] and noclaim[31:4167836] and noclaim[31:13406463]") ) ) ;
$total = count ( $d->items ) ;
shuffle ( $d->items ) ;
$items = array_slice ( $d->items , 0 , $batch_size ) ;


function myescape ( $s ) {
	return htmlspecialchars ( $s , ENT_QUOTES ) ;
}

print get_common_header ( '' , 'Wikidata items without images but with Commons categories' ) ;


$db = openDB ( 'wikidata' , 'wikidata' ) ;

$label = array() ;
$sql = "select * from wb_terms WHERE term_entity_type='item' and term_type='label' and term_language IN ('en','de','fr','it','es','nl') and term_entity_id in (" . implode(',',$items) . ")" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	if ( !isset($label[$o->term_entity_id]) or $o->term_language == 'en' ) {
		$label[$o->term_entity_id] = $o->term_text ;
	}
}

$db = openDB ( 'commons' , 'wikimedia' ) ;

$cnt = 0 ;
print "<div class='lead'>Here are $batch_size of $total items that have no image on Wikidata, but a Commons category.</div>" ;
print "<div><i>Note:</i> This tool requires <a href='http://tools.wmflabs.org/widar/index.php?action=authorize' target='_blank'>WiDar authorisation</a>.</div>" ;
print "<div><a href='?'>Load another random $batch_size items</a></div>" ;
print "<table class='table-striped table-condensed'><tbody>" ;
foreach ( $items AS $q ) {

	$q2 = 'Q' . $q ;
	$d = json_decode ( file_get_contents ( "https://www.wikidata.org/wiki/Special:EntityData/Q".$q.".json" ) ) ;
	$cat = $d->entities->$q2->claims->P373[0]->mainsnak->datavalue->value ;
	if ( !isset ( $cat ) ) continue ;
	
	$images = array() ;
	$sql = 'select distinct page_title from page,categorylinks where page_namespace=6 AND page_id=cl_from AND cl_to="' . get_db_safe($cat) . '" order by rand() limit 20' ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$images[] = 'File:' . $o->page_title ;
	}

	print "<tr>" ;
	print "<td valign='top'><a href='//www.wikidata.org/wiki/Q$q' target='_blank'>Q$q</a>" ;
	print "&nbsp;<a target='_blank' title='Reasonator' href='/reasonator/?q=Q$q'><img src='//upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Reasonator_logo_proposal.png/16px-Reasonator_logo_proposal.png' border='0'></a>" ;
	if ( isset($label[$q]) ) print "<br/><small>".$label[$q]."</small>" ;
	print "</td>" ;
	
	print "<td>" ;
	print "<a target='_blank' href='//commons.wikipedia.org/wiki/Category:" . myescape($cat) . "'>" . $cat . "</a><br/>" ;
	$ii = json_decode ( file_get_contents ( 'http://commons.wikimedia.org/w/api.php?action=query&titles='.implode("|",$images).'&prop=imageinfo&format=json&iiprop=url&iiurlwidth=120&iiurlheight=120' ) ) ;
	foreach ( $ii->query->pages AS $i ) {
		$l = floor((120-$i->imageinfo[0]->thumbwidth)/2) . "px" ;
		$t = floor((120-$i->imageinfo[0]->thumbheight)/2) . "px" ;
		$thumb = $i->imageinfo[0]->thumburl ;
		$desc = $i->imageinfo[0]->descriptionurl ;
		$title = substr ( $i->title , 5 ) ;
		if ( $thumb == '' ) continue ;

		print "<div style='display:inline-block;padding:2px;text-align:center'>" ;
		print "<div><a target='_blank' title='" . myescape($title) . "' href='" . $desc . "'>" ;
		print "<img border=0 src='" . $thumb . "' style='padding-left:$l;padding-right:$l;padding-top:$t;padding-bottom:$t'/>" ;
		print "</a></div>" ;
		print "<div class='links'>" ;
		print "Add as" ;
		print " <a href='#' onclick='add_image(this,\"$q\",\"".myescape($title)."\",\"18\");return false'>image</a>" ;
		print "/<br/><a href='#' onclick='add_image(this,\"$q\",\"".myescape($title)."\",\"242\");return false'>map</a>" ;
		print "/<a href='#' onclick='add_image(this,\"$q\",\"".myescape($title)."\",\"154\");return false'>logo</a>" ;
		print "/<a href='#' onclick='add_image(this,\"$q\",\"".myescape($title)."\",\"94\");return false'>CoA</a>" ;
		print "</div></div>" ;
	}
	print "</td>" ;
	
/*	print "<td>" ;
	foreach ( $d AS $k => $v ) {
		$i = $v->imageinfo[0] ;
		$l = floor((120-$i->thumbwidth)/2) . "px" ;
		$t = floor((120-$i->thumbheight)/2) . "px" ;
		print "<div style='display:inline-block;padding:2px;text-align:center'>" ;
		print "<div><a target='_blank' title='" . myescape($v->title) . "' href='" . $i->descriptionurl . "'>" ;
		print "<img border=0 src='" . $i->thumburl . "' style='padding-left:$l;padding-right:$l;padding-top:$t;padding-bottom:$t'/>" ;
		print "</a></div><div class='links'>" ;
		print "<a href='#' onclick='add_image(this,\"$q\",\"".myescape($v->title)."\",\"18\");return false'>Image</a> / " ;
		print "<a href='#' onclick='add_image(this,\"$q\",\"".myescape($v->title)."\",\"181\");return false'>range map</a>" ;
		print "</div></div>" ;
	}
//	print "<a href='https://secure.flickr.com/photos/" . $d->owner . "/" . $d->id . "' target='_blank'><img src='" . $d->url_t . "' border='0' /></a></td>" ;
//	print "<td>" . $d->title . "<br/>" ;
//	print "<a href='http://tools.wmflabs.org/flickr2commons/?photoid=" . $d->id . "' target='_blank'>Flickr2Commons</a>" ;
	print "</td>" ;*/
	print "</tr>" ;
	myflush();
	$cnt++ ;
}
print "</tbody></table>" ;

print "<div><a href='?'>Load another random $batch_size items</a></div>" ;

print '<script>

function add_image ( o , q , s , prop ) {
	s = s.replace(/[_\+]/g," ").replace(/^File:/,"") ;
	prop = prop.replace(/\D/g,"") ;
	q = q.replace(/\D/g,"") ;
	$.getJSON ( "/widar/index.php?callback=?" , {
		action:"set_string",
		id:"Q"+q,
		prop:"P"+prop,
		text:s,
		botmode:1
	} , function ( d ) {
		if ( d.error == "OK" ) {
			$($(o).parents("div.links").get(0)).html("<b>Added!</b><br/>&nbsp;") ;
		} else alert ( d.error ) ;
	} ) .fail(function( jqxhr, textStatus, error ) {
		alert ( error ) ;
	} ) ;
}
</script>' ;

print get_common_footer() ;

?>